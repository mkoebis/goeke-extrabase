﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ExtraBase.Shared.DTO;

namespace ExtraBase.Shared.Interface
{
    [ServiceContract]
    public interface IExtraBaseService
    {
        #region Canongruppe        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newCanongruppe", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddCanongruppe(CanongruppeDTO canongruppe);
        [OperationContract]
        [WebGet(UriTemplate = "Canongruppe?id={canongruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        CanongruppeDTO GetCanongruppeById(int canongruppeId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateCanongruppe?id={canongruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateCanongruppe(CanongruppeDTO canongruppe, int canongruppeId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteCanongruppe?id={canongruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteCanongruppe(int canongruppeId);
        [OperationContract]
        [WebGet(UriTemplate = "Canongruppen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<CanongruppeDTO> GetCanongruppen();
        #endregion

        #region Hersteller     
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newHersteller", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddHersteller(HerstellerDTO hersteller);        
        [OperationContract]
        [WebGet(UriTemplate = "Hersteller?id={herstellerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        HerstellerDTO GetHerstellerById(int herstellerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateHersteller?id={herstellerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateHerstellerById(HerstellerDTO hersteller, int herstellerId);        
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteHersteller?id={herstellerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteHerstellerById(int herstellerId);
        [OperationContract]
        [WebGet(UriTemplate = "Hersteller", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<HerstellerDTO> GetHersteller();
        #endregion

        #region Serviceart        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newServiceart", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddServiceArt(ServiceArtDTO serviceart);
        [OperationContract]
        [WebGet(UriTemplate = "Serviceart?id={serviceartId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ServiceArtDTO GetServiceArtById(int serviceartId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateServiceart?id={serviceartId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateServiceArtById(ServiceArtDTO serviceart, int serviceartId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteServiceart?id={serviceartId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteServiceArtById(int serviceartId);
        [OperationContract]
        [WebGet(UriTemplate = "Servicearten", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ServiceArtDTO> GetServiceArten();
        #endregion

        #region ServiceberichtPosition
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newServiceberichtPosition", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddServiceberichtPosition(ServiceberichtPositionDTO ServiceberichtPosition);
        [OperationContract]
        [WebGet(UriTemplate = "ServiceberichtPosition?id={serviceberichtPositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ServiceberichtPositionDTO GetServiceberichtPositionById(int ServiceberichtPositionId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateServiceberichtPosition?id={serviceberichtPositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateServiceberichtPositionById(ServiceberichtPositionDTO ServiceberichtPosition, int ServiceberichtPositionId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteServiceberichtPosition?id={serviceberichtPositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteServiceberichtPositionById(int ServiceberichtPositionId);
        [OperationContract]
        [WebGet(UriTemplate = "ServiceberichtPositionen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ServiceberichtPositionDTO> GetServiceberichtPositionen();
        #endregion

        #region Maschinenstammm
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newMaschinenstamm", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0430Maschinenstamm(T0430MaschinenstammDTO T0430Maschinenstamm);
        [OperationContract]
        [WebGet(UriTemplate = "Maschinenstamm?id={T0430maschinenstammId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0430MaschinenstammDTO GetT0430MaschinenstammById(int T0430MaschinenstammId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateMaschinenstamm?id={T0430maschinenstammId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0430MaschinenstammById(T0430MaschinenstammDTO T0430Maschinenstamm, int T0430MaschinenstammId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteMaschinenstamm?id={T0430maschinenstammId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0430MaschinenstammById(int T0430MaschinenstammId);
        [OperationContract]
        [WebGet(UriTemplate = "Maschinenstamm", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0430MaschinenstammDTO> GetT0430Maschinenstamm();
        #endregion

        #region Servicebericht
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newServicebericht", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddServicebericht(ServiceberichtDTO serviceBericht);
        [OperationContract]
        [WebGet(UriTemplate = "Servicebericht?id={serviceberichtId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ServiceberichtDTO GetServiceberichtById(int serviceberichtId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateServicebericht?id={serviceberichtId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateServiceberichtById(ServiceberichtDTO serviceBericht, int serviceberichtId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteServicebericht?id={serviceberichtId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteServiceberichtById(int serviceberichtId);
        [OperationContract]
        [WebGet(UriTemplate = "Serviceberichte", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ServiceberichtDTO> GetServiceberichte();
        #endregion

        #region Techniker
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newTechniker", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0143Techniker(T0143TechnikerDTO T0143Techniker);
        [OperationContract]
        [WebGet(UriTemplate = "Techniker?id={T0143technikerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0143TechnikerDTO GetT0143TechnikerById(int T0143TechnikerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateTechniker?id={T0143technikerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0143TechnikerById(T0143TechnikerDTO T0143Techniker, int T0143TechnikerId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteTechniker?id={T0143technikerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0143TechnikerById(int T0143TechnikerId);
        [OperationContract]
        [WebGet(UriTemplate = "Techniker", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0143TechnikerDTO> GetT0143Techniker();
        #endregion

        #region MwSt
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newMwst", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0102MwSt(T0102MwStDTO T0102MwSt);
        [OperationContract]
        [WebGet(UriTemplate = "Mwst?id={T0102mwstId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0102MwStDTO GetT0102MwStById(int T0102MwStId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateMwst?id={T0102mwstId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0102MwStById(T0102MwStDTO T0102MwSt, int T0102MwStId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteMwst?id={T0102mwstId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0102MwStById(int T0102MwStId);
        [OperationContract]
        [WebGet(UriTemplate = "Mwst", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0102MwStDTO> GetT0102MwSt();
        #endregion

        #region Vertrag
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newVertrag", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0602Vertrag(T0602VertragDTO T0602Vertrag);
        [OperationContract]
        [WebGet(UriTemplate = "Vertrag?id={T0602vertragId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0602VertragDTO GetT0602VertragById(int T0602VertragId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateVertrag?id={t0602vertragId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0602VertragById(T0602VertragDTO T0602Vertrag, int T0602VertragId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteVertrag?id={T0602vertragId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0602VertragById(int T0602VertragId);
        [OperationContract]
        [WebGet(UriTemplate = "Vertraege", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0602VertragDTO> GetT0602Vertraege();
        #endregion

        #region Bank
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newBank", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0103Bank(T0103BankDTO T0103Bank);
        [OperationContract]
        [WebGet(UriTemplate = "Bank?id={T0103bankId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0103BankDTO GetT0103BankById(int T0103BankId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateBank?id={T0103bankId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0103BankById(T0103BankDTO T0103Bank, int T0103BankId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteBank?id={T0103bankId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0103BankById(int T0103BankId);
        [OperationContract]
        [WebGet(UriTemplate = "Banken", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0103BankDTO> GetT0103Banken();
        #endregion

        #region Anschrift
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newAnschrift", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0108Anschrift(T0108AnschriftDTO T0108Anschrift);
        [OperationContract]
        [WebGet(UriTemplate = "Anschrift?id={T0108anschriftId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0108AnschriftDTO GetT0108AnschriftById(int T0108AnschriftId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateAnschrift?id={t0108anschriftId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0108AnschriftById(T0108AnschriftDTO T0108Anschrift, int T0108AnschriftId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteAnschrift?id={T0108anschriftId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0108AnschriftById(int T0108AnschriftId);
        [OperationContract]
        [WebGet(UriTemplate = "Anschriften", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0108AnschriftDTO> GetT0108Anschriften();
        #endregion

        #region Lagerort
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newLagerort", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0302Lagerort(T0302LagerortDTO T0302Lagerort);
        [OperationContract]
        [WebGet(UriTemplate = "Lagerort?id={T0302lagerortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0302LagerortDTO GetT0302LagerortById(int T0302LagerortId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateLagerort?id={T0302lagerortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0302LagerortById(T0302LagerortDTO T0302Lagerort, int T0302LagerortId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteLagerort?id={T0302lagerortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0302LagerortById(int T0302LagerortId);
        [OperationContract]
        [WebGet(UriTemplate = "Lagerorte", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0302LagerortDTO> GetT0302Lagerorte();
        #endregion

        #region Kostenstelle
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newKostenstelle", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0141Kostenstelle(T0141KostenstelleDTO T0141Kostenstelle);
        [OperationContract]
        [WebGet(UriTemplate = "Kostenstelle?id={T0141kostenstelleId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0141KostenstelleDTO GetT0141KostenstelleById(int T0141KostenstelleId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateKostenstelle?id={T0141kostenstelleId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0141KostenstelleById(T0141KostenstelleDTO T0141Kostenstelle, int T0141KostenstelleId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteKostenstelle?id={T0141kostenstelleId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0141KostenstelleById(int T0141KostenstelleId);
        [OperationContract]
        [WebGet(UriTemplate = "Kostenstellen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0141KostenstelleDTO> GetT0141Kostenstellen();
        #endregion

        #region Lager
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newLager", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0301Lager(T0301LagerDTO T0301Lager);
        [OperationContract]
        [WebGet(UriTemplate = "Lager?id={T0301lagerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0301LagerDTO GetT0301LagerById(int T0301LagerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateLager?id={T0301lagerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0301LagerById(T0301LagerDTO T0301Lager, int T0301LagerId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteLager?id={T0301lagerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0301LagerById(int T0301LagerId);
        [OperationContract]
        [WebGet(UriTemplate = "Lager", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0301LagerDTO> GetT0301Lager();
        #endregion

        #region Angebot
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newAngebot", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0401Angebot(T0401AngebotDTO T0401Angebot);
        [OperationContract]
        [WebGet(UriTemplate = "Angebot?id={T0401angebotId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0401AngebotDTO GetT0401AngebotById(int T0401AngebotId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateAngebot?id={T0401angebotId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0401AngebotById(T0401AngebotDTO T0401Angebot, int T0401AngebotId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteAngebot?id={T0401angebotId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0401AngebotById(int T0401AngebotId);
        [OperationContract]
        [WebGet(UriTemplate = "Angebote", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0401AngebotDTO> GetT0401Angebote();
        #endregion

        #region Vertragstyp
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newVertragstyp", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0601Vertragstyp(T0601VertragstypDTO T0601Vertragstyp);
        [OperationContract]
        [WebGet(UriTemplate = "Vertragstyp?id={T0601vertragstypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0601VertragstypDTO GetT0601VertragstypById(int T0601VertragstypId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateVertragstyp?id={T0601vertragstypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0601VertragstypById(T0601VertragstypDTO T0601Vertragstyp, int T0601VertragstypId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteVertragstyp?id={T0601vertragstypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0601VertragstypById(int T0601VertragstypId);
        [OperationContract]
        [WebGet(UriTemplate = "Vertragstypen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0601VertragstypDTO> GetT0601Vertragstypen();
        #endregion

        #region Abrechnungskopf
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newAbrechnungskopf", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0620Abrechnunskopf(T0620AbrechnungskopfDTO T0620Abrechnungskopf);
        [OperationContract]
        [WebGet(UriTemplate = "Abrechnungskopf?id={T0620abrechnungskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0620AbrechnungskopfDTO GetT0620AbrechnungskopfById(int T0620AbrechnungskopfId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateAbrechnungskopf?id={T0620abrechnungskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0620AbrechnungskopfById(T0620AbrechnungskopfDTO T0620Abrechnungskopf, int T0620AbrechnungskopfId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteAbrechnungskopf?id={T0620abrechnungskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0620AbrechnungskopfById(int T0620AbrechnungskopfId);
        [OperationContract]
        [WebGet(UriTemplate = "Abrechnungskoepfe", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0620AbrechnungskopfDTO> GetT0620Abrechnungskoepfe();
        #endregion

        #region Gruppe
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newGruppe", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0706Gruppe(T0706GruppeDTO T0706Gruppe);
        [OperationContract]
        [WebGet(UriTemplate = "Gruppe?id={T0706gruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0706GruppeDTO GetT0706GruppeById(int T0706GruppeId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateGruppe?id={T0706gruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0706GruppeById(T0706GruppeDTO T0706Gruppe, int T0706GruppeId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteGruppe?id={T0706gruppeId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0706GruppeById(int T0706GruppeId);
        [OperationContract]
        [WebGet(UriTemplate = "Gruppen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0706GruppeDTO> GetT0706Gruppen();
        #endregion

        #region Benutzer
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newBenutzer", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0707Benutzer(T0707BenutzerDTO T0707Benutzer);
        [OperationContract]
        [WebGet(UriTemplate = "Benutzer?id={t0707benutzerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0707BenutzerDTO GetT0707BenutzerById(int T0707BenutzerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateBenutzer?id={T0707benutzerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0707BenutzerById(T0707BenutzerDTO T0707Benutzer, int T0707BenutzerId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteBenutzer?id={T0707benutzerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0707BenutzerById(int T0707BenutzerId);
        [OperationContract]
        [WebGet(UriTemplate = "Benutzer", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0707BenutzerDTO> GetT0707Benutzer();
        #endregion

        #region Prozesskopf
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newProzesskopf", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0802Prozesskopf(T0802ProzesskopfDTO T0802Prozesskopf);
        [OperationContract]
        [WebGet(UriTemplate = "Prozesskopf?id={T0802prozesskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0802ProzesskopfDTO GetT0802ProzesskopfById(int T0802ProzesskopfId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateProzesskopf?id={T0802prozesskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0802ProzesskopfById(T0802ProzesskopfDTO T0802Prozesskopf, int T0802ProzesskopfId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteProzesskopf?id={T0802prozesskopfId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0802ProzesskopfById(int T0802ProzesskopfId);
        [OperationContract]
        [WebGet(UriTemplate = "Prozesskoepfe", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0802ProzesskopfDTO> GetT0802Prozesskoepfe();
        #endregion

        #region TechnikerEinsatz
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newTechnikerEinsatz", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0810TechnikerEinsatz(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerEinsatz?id={T0810technikerEinsatzId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0810TechnikerEinsatzDTO GetT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateTechnikerEinsatz?id={T0810technikerEinsatzId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0810TechnikerEinsatzById(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz, int T0810TechnikerEinsatzId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteTechnikerEinsatz?id={T0810technikerEinsatzId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerEinsaetze", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0810TechnikerEinsatzDTO> GetT0810TechnikerEinsaetze();
        #endregion

        #region TechnikerStatus
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newTechnikerStatus", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0813TechnikerStatus(T0813TechnikerStatusDTO T0813TechnikerStatus);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerStatus?id={T0813technikerStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0813TechnikerStatusDTO GetT0813TechnikerStatusById(int T0813TechnikerStatusId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateTechnikerStatus?id={T0813technikerStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0813TechnikerStatusById(T0813TechnikerStatusDTO T0813TechnikerStatus, int T0813TechnikerStatusId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteTechnikerStatus?id={T0813technikerStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0813TechnikerStatusById(int T0813TechnikerStatusId);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerStatus", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0813TechnikerStatusDTO> GetT0813TechnikerStatus();
        #endregion

        #region Fehlercodes
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newFehlercode", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT1601Fehlercodes(T1601FehlercodesDTO T1601Fehlercodes);
        [OperationContract]
        [WebGet(UriTemplate = "Fehlercodes?id={T1601fehlercodesId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T1601FehlercodesDTO GetT1601FehlercodesById(int T1601FehlercodesId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateFehlercode?id={T1601fehlercodesId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT1601FehlercodesById(T1601FehlercodesDTO T1601Fehlercodes, int T1601FehlercodesId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteFehlercode?id={T1601fehlercodesId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT1601FehlercodesById(int T1601FehlercodesId);
        [OperationContract]
        [WebGet(UriTemplate = "Fehlercodes", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T1601FehlercodesDTO> GetT1601Fehlercodes();
        #endregion

        #region TechnikerEinsatzStatus
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newTechnikerEinsatzStatus", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddTechnikerEinsatzStatus(TechnikerEinsatzStatusDTO technikerEinsatzStatus);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerEinsatzStatus?id={technikerEinsatzStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        TechnikerEinsatzStatusDTO GetTechnikerEinsatzStatusById(int technikerEinsatzStatusId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateTechnikerEinsatzStatus?id={technikerEinsatzStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateTechnikerEinsatzStatusById(TechnikerEinsatzStatusDTO technikerEinsatzStatus, int technikerEinsatzStatusId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteTechnikerEinsatzStatus?id={technikerEinsatzStatusId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteTechnikerEinsatzStatusById(int technikerEinsatzStatusId);
        [OperationContract]
        [WebGet(UriTemplate = "TechnikerEinsatzStatus", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<TechnikerEinsatzStatusDTO> GetTechnikerEinsatzStatus();
        #endregion

        #region Lagerortbestand
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newLagerortbestand", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0504Lagerortbestand(T0504LagerortbestandDTO T0504Lagerortbestand);        
        [OperationContract]
        [WebGet(UriTemplate = "Lagerortbestand?id={T0504lagerortbestandId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0504LagerortbestandDTO GetT0504LagerortbestandById(int T0504LagerortbestandId);        
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateLagerortbestand?id={T0504lagerortbestandId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0504LagerortbestandById(T0504LagerortbestandDTO T0504Lagerortbestand, int T0504LagerortbestandId);        
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteLagerortbestand?id={T0504lagerortbestandId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0504LagerortbestandById(int T0504LagerortbestandId);
        [OperationContract]
        [WebGet(UriTemplate = "Lagerortbestaende", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0504LagerortbestandDTO> GetT0504Lagerortbestaende();
        #endregion

        #region Zaehlwerk
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newZaehlwerk", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0432Zaehlwerk(T0432ZaehlwerkDTO T0432Zaehlwerk);
        [OperationContract]
        [WebGet(UriTemplate = "Zaehlwerk?id={T0432zaehlwerkId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0432ZaehlwerkDTO GetT0432ZaehlwerkById(int T0432ZaehlwerkId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateZaehlwerk?id={T0432zaehlwerkId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0432ZaehlwerkById(T0432ZaehlwerkDTO T0432Zaehlwerk, int T0432ZaehlwerkId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteZaehlwerk?id={T0432zaehlwerkId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0432ZaehlwerkById(int T0432ZaehlwerkId);
        [OperationContract]
        [WebGet(UriTemplate = "Zaehlwerke", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0432ZaehlwerkDTO> GetT0432Zaehlwerke();
        #endregion

        #region ZaehlwerkTyp
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newZaehlwerkTyp", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0431ZaehlwerkTyp(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp);
        [OperationContract]
        [WebGet(UriTemplate = "ZaehlwerkTyp?id={T0431zaehlwerkTypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0431ZaehlwerkTypDTO GetT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateZaehlwerkTyp?id={T0431zaehlwerkTypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0431ZaehlwerkTypById(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp, int T0431ZaehlwerkTypId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteZaehlwerkTyp?id={T0431ZaehlwerkTypId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId);
        [OperationContract]
        [WebGet(UriTemplate = "Zaehlwerktypen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0431ZaehlwerkTypDTO> GetT0431Zaehlwerktypen();
        #endregion

        #region Standort
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newStandort", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0434Standort(T0434StandortDTO T0434Standort);
        [OperationContract]
        [WebGet(UriTemplate = "Standort?id={T0434standortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0434StandortDTO GetT0434StandortById(int T0434StandortId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateStandort?id={T0434standortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0434StandortById(T0434StandortDTO T0434Standort, int T0434StandortId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteStandort?id={T0434standortId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0434StandortById(int T0434StandortId);
        [OperationContract]
        [WebGet(UriTemplate = "Standorte", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0434StandortDTO> GetT0434Standorte();
        #endregion

        #region Ansprechpartner
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newAnsprechpartner", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0120Ansprechpartner(T0120AnsprechpartnerDTO T0120Ansprechpartner);
        [OperationContract]
        [WebGet(UriTemplate = "Ansprechpartner?id={T0120ansprechpartnerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0120AnsprechpartnerDTO GetT0120AnsprechpartnerById(int T0120AnsprechpartnerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateAnsprechpartner?id={T0120ansprechpartnerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0120AnsprechpartnerById(T0120AnsprechpartnerDTO T0120Ansprechpartner, int T0120AnsprechpartnerId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteAnsprechpartner?id={T0120ansprechpartnerId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0120AnsprechpartnerById(int T0120AnsprechpartnerId);
        [OperationContract]
        [WebGet(UriTemplate = "Ansprechpartner", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0120AnsprechpartnerDTO> GetT0120Ansprechpartner();
        #endregion

        #region Prozessposition
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newProzessposition", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0803Prozessposition(T0803ProzesspositionDTO T0803Prozessposition);
        [OperationContract]
        [WebGet(UriTemplate = "Prozessposition?id={T0803prozesspositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0803ProzesspositionDTO GetT0803ProzesspositionById(int T0803ProzesspositionId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateProzessposition?id={T0803prozesspositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0803ProzesspositionById(T0803ProzesspositionDTO T0803Prozessposition, int T0803ProzesspositionId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteProzessposition?id={T0803prozesspositionId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0803ProzesspositionById(int T0803ProzesspositionId);
        [OperationContract]
        [WebGet(UriTemplate = "Prozesspositionen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0803ProzesspositionDTO> GetT0803Prozesspositionen();
        #endregion

        #region Person
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newPerson", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0110Person(T0110PersonDTO T0110Person);
        [OperationContract]
        [WebGet(UriTemplate = "Person?id={T0110PersonId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0110PersonDTO GetT0110PersonById(int T0110PersonId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updatePerson?id={T0110PersonId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0110PersonById(T0110PersonDTO T0110Person, int T0110PersonId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deletePerson?id={T0110PersonId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0110PersonById(int T0110PersonId);
        [OperationContract]
        [WebGet(UriTemplate = "Personen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0110PersonDTO> GetT0110Personen();
        #endregion

        #region Bankverbindung
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newBankverbindung", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0123Bankverbindung(T0123BankverbindungDTO T0123Bankverbindung);
        [OperationContract]
        [WebGet(UriTemplate = "Bankverbindung?id={T0123BankverbindungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        T0123BankverbindungDTO GetT0123BankverbindungById(int T0123BankverbindungId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateBankverbindung?id={T0123BankverbindungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0123BankverbindungById(T0123BankverbindungDTO T0123Bankverbindung, int T0123BankverbindungId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteBankverbindung?id={T0123BankverbindungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0123BankverbindungById(int T0123BankverbindungId);
        [OperationContract]
        [WebGet(UriTemplate = "Bankverbindungen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0123BankverbindungDTO> GetT0123Bankverbindungen();
        #endregion

        #region VDE
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newVDE", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddVDE(VDEDTO VDE);
        [OperationContract]
        [WebGet(UriTemplate = "VDE?id={VDEId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        VDEDTO GetVDEById(int VDEId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateVDE?id={VDEId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateVDEById(VDEDTO VDE, int VDEId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteVDE?id={VDEId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteVDEById(int VDEId);
        [OperationContract]
        [WebGet(UriTemplate = "VDEs", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<VDEDTO> GetVDEs();
        #endregion

        #region SNRBewegung
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newSNRBewegung", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0511SNRBewegung(T0511SNRBewegungDTO T0511SNRBewegung);
        [OperationContract]
        [WebGet(UriTemplate = "SNRBewegung?id={T0511SNRBewegungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]           
        T0511SNRBewegungDTO GetT0511SNRBewegungById(int T0511SNRBewegungId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateSNRBewegung?id={T0511SNRBewegungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0511SNRBewegungById(T0511SNRBewegungDTO T0511SNRBewegung, int T0511SNRBewegungId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteSNRBewegung?id={T0511SNRBewegungId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0511SNRBewegungById(int T0511SNRBewegungId);
        [OperationContract]
        [WebGet(UriTemplate = "SNRBewegungen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0511SNRBewegungDTO> GetT0511SNRBewegungen();
        #endregion

        #region Vertragsmaschine
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newVertragsmaschine", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0606Vertragsmaschine(T0606VertragsmaschineDTO T0606Vertragsmaschine);
        [OperationContract]
        [WebGet(UriTemplate = "Vertragsmaschine?id={T0606VertragsmaschineId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]           
        T0606VertragsmaschineDTO GetT0606VertragsmaschineById(int T0606VertragsmaschineId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateVertragsmaschine?id={T0606VertragsmaschineId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0606VertragsmaschineById(T0606VertragsmaschineDTO T0606Vertragsmaschine, int T0606VertragsmaschineId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteVertragsmaschine?id={T0606VertragsmaschineId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0606VertragsmaschineById(int T0606VertragsmaschineId);
        [OperationContract]
        [WebGet(UriTemplate = "Vertragsmaschinen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0606VertragsmaschineDTO> GetT0606Vertragsmaschinen();
        #endregion
        
        #region GPKonditionenWgrp
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newGPKonditionenWgrp", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0117GPKonditionenWgrp(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp);
        [OperationContract]
        [WebGet(UriTemplate = "GPKonditionenWgrp?id={T0117GPKonditionenWgrpId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]           
        T0117GPKonditionenWgrpDTO GetT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateGPKonditionenWgrp?id={T0117GPKonditionenWgrpId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp, int T0117GPKonditionenWgrpId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteGPKonditionenWrgp?id={T0117GPKonditionenWgrpId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId);
        [OperationContract]
        [WebGet(UriTemplate = "GPKonditionenWrgp", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0117GPKonditionenWgrpDTO> GetT0117GPKonditionenWgrp();
        #endregion

        #region GPKonditionenArtikel
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "newGPKonditionenArtikel", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool AddT0118GPKonditionenArtikel(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel);
        [OperationContract]
        [WebGet(UriTemplate = "GPKonditionenArtikel?id={T0118GPKonditionenArtikelId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]           
        T0118GPKonditionenArtikelDTO GetT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateGPKonditionenArtikel?id={T0118GPKonditionenArtikelId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool UpdateT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel, int T0118GPKonditionenArtikelId);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteGPKonditionenArtikel?id={T0118GPKonditionenArtikelId}", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId);
        [OperationContract]
        [WebGet(UriTemplate = "GPKonditionenArtikel", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<T0118GPKonditionenArtikelDTO> GetT0118GPKonditionenArtikel();
        #endregion

        [OperationContract]
        [WebGet(UriTemplate = "Connection", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool TestConnection();

        [OperationContract]
        [WebGet(UriTemplate = "Time", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        DateTime GetCurrentDateTime();            
    }
}