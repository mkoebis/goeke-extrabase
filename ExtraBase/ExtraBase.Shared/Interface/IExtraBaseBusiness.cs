﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExtraBase.Shared.DTO;

namespace ExtraBase.Shared.Interface
{
    public interface IExtraBaseBusiness
    {
        #region Canongruppe        
        bool AddCanongruppe(CanongruppeDTO canongruppe);
        CanongruppeDTO GetCanongruppeById(int canongruppeId);
        bool UpdateCanongruppeById(CanongruppeDTO canongruppe, int canongruppeId);
        bool DeleteCanongruppeById(int canongruppeId);
        List<CanongruppeDTO> GetCanongruppen();
        #endregion

        #region Hersteller     
        bool AddHersteller(HerstellerDTO hersteller);
        HerstellerDTO GetHerstellerById(int herstellerId);
        bool UpdateHerstellerById(HerstellerDTO hersteller, int herstellerId);
        bool DeleteHerstellerById(int herstellerId);
        List<HerstellerDTO> GetHersteller();
        #endregion

        #region Serviceart        
        bool AddServiceArt(ServiceArtDTO serviceart);
        ServiceArtDTO GetServiceArtById(int serviceartId);
        bool UpdateServiceArtById(ServiceArtDTO serviceart, int serviceartId);
        bool DeleteServiceArtById(int serviceartId);
        List<ServiceArtDTO> GetServiceArten();
        #endregion

        #region Maschinenstammm        
        bool AddT0430Maschinenstamm(T0430MaschinenstammDTO T0430Maschinenstamm);
        T0430MaschinenstammDTO GetT0430MaschinenstammById(int T0430MaschinenstammId);
        bool UpdateMaschinenstammById(T0430MaschinenstammDTO T0430Maschinenstamm, int T0430MaschinenstammId);
        bool DeleteT0430MaschinenstammById(int T0430MaschinenstammId);
        List<T0430MaschinenstammDTO> GetT0430Maschinenstamm();
        #endregion

        #region Servicebericht
        bool AddServicebericht(ServiceberichtDTO serviceBericht);
        ServiceberichtDTO GetServiceberichtById(int serviceberichtId);
        bool UpdateServiceberichtById(ServiceberichtDTO serviceBericht, int serviceberichtId);
        bool DeleteServiceberichtById(int serviceberichtId);
        List<ServiceberichtDTO> GetServiceberichte();
        #endregion

        #region ServiceberichtPosition
        bool AddServiceberichtPosition(ServiceberichtPositionDTO ServiceberichtPosition);
        ServiceberichtPositionDTO GetServiceberichtPositionById(int ServiceberichtPositionId);
        bool UpdateServiceberichtPositionById(ServiceberichtPositionDTO ServiceberichtPosition, int ServiceberichtPositionId);
        bool DeleteServiceberichtPositionById(int ServiceberichtPositionId);
        List<ServiceberichtPositionDTO> GetServiceberichtPositionen();
        #endregion

        #region Techniker
        bool AddT0143Techniker(T0143TechnikerDTO T0143Techniker);
        T0143TechnikerDTO GetT0143TechnikerById(int T0143TechnikerId);
        bool UpdateT0143TechnikerById(T0143TechnikerDTO T0143Techniker, int T0143TechnikerId);
        bool DeleteT0143TechnikerById(int T0143TechnikerId);
        List<T0143TechnikerDTO> GetT0143Techniker();
        #endregion

        #region MwSt
        bool AddT0102Mwst(T0102MwStDTO T0102MwSt);
        T0102MwStDTO GetT0102MwStById(int T0102MwStId);
        bool UpdateT0102MwStById(T0102MwStDTO T0102MwSt, int T0102MwStId);
        bool DeleteT0102MwStById(int T0102MwStId);
        List<T0102MwStDTO> GetT0102MwSt();
        #endregion

        #region Vertrag
        bool AddT0602Vertrag(T0602VertragDTO T0602Vertrag);
        T0602VertragDTO GetT0602VertragById(int T0602VertragId);
        bool UpdateT0602VertragById(T0602VertragDTO T0602Vertrag, int T0602VertragId);
        bool DeleteT0602VertragById(int T0602VertragId);
        List<T0602VertragDTO> GetT0602Vertraege();
        #endregion

        #region Bank
        bool AddT0103Bank(T0103BankDTO T0103Bank);
        T0103BankDTO GetT0103BankById(int T0103BankId);
        bool UpdateT0103BankById(T0103BankDTO T0103Bank, int T0103BankId);
        bool DeleteT0103BankById(int T0103BankId);
        List<T0103BankDTO> GetT0103Banken();
        #endregion

        #region Anschrift
        bool AddT0108Anschrift(T0108AnschriftDTO T0108Anschrift);
        T0108AnschriftDTO GetT0108AnschriftById(int T0108AnschriftId);
        bool UpdateT0108AnschriftById(T0108AnschriftDTO T0108Anschrift, int T0108AnschriftId);
        bool DeleteT0108AnschriftById(int T0108AnschriftId);
        List<T0108AnschriftDTO> GetT0108Anschriften();
        #endregion

        #region Lagerort
        bool AddT0302Lagerort(T0302LagerortDTO T0302Lagerort);
        T0302LagerortDTO GetT0302LagerortById(int T0302LagerortId);
        bool UpdateT0302LagerortById(T0302LagerortDTO T0302Lagerort, int T0302LagerortId);
        bool DeleteT0302LagerortById(int T0302LagerortId);
        List<T0302LagerortDTO> GetT0302Lagerorte();
        #endregion

        #region Kostenstelle
        bool AddT0141Kostenstelle(T0141KostenstelleDTO T0141Kostenstelle);
        T0141KostenstelleDTO GetT0141KostenstelleById(int T0141KostenstelleId);
        bool UpdateT0141KostenstelleById(T0141KostenstelleDTO T0141Kostenstelle, int T0141KostenstelleId);
        bool DeleteT0141KostenstelleById(int T0141KostenstelleId);
        List<T0141KostenstelleDTO> GetT0141Kostenstellen();
        #endregion

        #region Lager
        bool AddT0301Lager(T0301LagerDTO T0301Lager);
        T0301LagerDTO GetT0301LagerById(int T0301LagerId);
        bool UpdateT0301LagerById(T0301LagerDTO T0301Lager, int T0301LagerId);
        bool DeleteT0301LagerById(int T0301LagerId);
        List<T0301LagerDTO> GetT0301Lager();
        #endregion

        #region Angebot
        bool AddT0401Angebot(T0401AngebotDTO T0401Angebot);
        T0401AngebotDTO GetT0401AngebotById(int T0401AngebotId);
        bool UpdateT0401AngebotById(T0401AngebotDTO T0401Angebot, int T0401AngebotId);
        bool DeleteT0401AngebotById(int T0401AngebotId);
        List<T0401AngebotDTO> GetT0401Angebote();
        #endregion

        #region Vertragstyp
        bool AddT0601Vertragstyp(T0601VertragstypDTO T0601Vertragstyp);
        T0601VertragstypDTO GetT0601VertragstypById(int T0601VertragstypId);
        bool UpdateT0601VertragstypById(T0601VertragstypDTO T0601Vertragstyp, int T0601VertragstypId);
        bool DeleteT0601VertragstypById(int T0601VertragstypId);
        List<T0601VertragstypDTO> GetT0601Vertragstypen();
        #endregion

        #region Abrechnungskopf
        bool AddT0620Abrechnunskopf(T0620AbrechnungskopfDTO T0620Abrechnungskopf);
        T0620AbrechnungskopfDTO GetT0620AbrechnungskopfById(int T0620AbrechnungskopfId);
        bool UpdateT0620AbrechnungskopfById(T0620AbrechnungskopfDTO T0620Abrechnungskopf, int T0620AbrechnungskopfId);
        bool DeleteT0620AbrechnungskopfById(int T0620AbrechnungskopfId);
        List<T0620AbrechnungskopfDTO> GetT0620Abrechnungskoepfe();
        #endregion

        #region Gruppe
        bool AddT0706Gruppe(T0706GruppeDTO T0706Gruppe);
        T0706GruppeDTO GetT0706GruppeById(int T0706GruppeId);
        bool UpdateT0706GruppeById(T0706GruppeDTO T0706Gruppe, int T0706GruppeId);
        bool DeleteT0706GruppeById(int T0706GruppeId);
        List<T0706GruppeDTO> GetT0706Gruppen();
        #endregion

        #region Benutzer
        bool AddT0707Benutzer(T0707BenutzerDTO T0707Benutzer);
        T0707BenutzerDTO GetT0707BenutzerById(int T0707BenutzerId);
        bool UpdateT0707BenutzerById(T0707BenutzerDTO T0707Benutzer, int T0707BenutzerId);
        bool DeleteT0707BenutzerById(int T0707BenutzerId);
        List<T0707BenutzerDTO> GetT0707Benutzer();
        #endregion

        #region Prozesskopf
        bool AddT0802Prozesskopf(T0802ProzesskopfDTO T0802Prozesskopf);
        T0802ProzesskopfDTO GetT0802ProzesskopfById(int T0802ProzesskopfId);
        bool UpdateT0802ProzesskopfById(T0802ProzesskopfDTO T0802Prozesskopf, int T0802ProzesskopfId);
        bool DeleteT0802ProzesskopfById(int T0802ProzesskopfId);
        List<T0802ProzesskopfDTO> GetT0802Prozesskoepfe();
        #endregion

        #region TechnikerEinsatz
        bool AddT0810TechnikerEinsatz(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz);
        T0810TechnikerEinsatzDTO GetT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId);
        bool UpdateT0810TechnikerEinsatzById(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz, int T0810TechnikerEinsatzId);
        bool DeleteT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId);
        List<T0810TechnikerEinsatzDTO> GetT0810TechnikerEinsaetze();
        #endregion

        #region TechnikerStatus
        bool AddT0813TechnikerStatus(T0813TechnikerStatusDTO T0813TechnikerStatus);
        T0813TechnikerStatusDTO GetT0813TechnikerStatusById(int T0813TechnikerStatusId);
        bool UpdateT0813TechnikerStatusById(T0813TechnikerStatusDTO T0813TechnikerStatus, int T0813TechnikerStatusId);
        bool DeleteT0813TechnikerStatusById(int T0813TechnikerStatusId);
        List<T0813TechnikerStatusDTO> GetT0813TechnikerStatus();
        #endregion

        #region Fehlercodes
        bool AddT1601Fehlercodes(T1601FehlercodesDTO T1601Fehlercodes);
        T1601FehlercodesDTO GetT1601FehlercodesById(int T1601FehlercodesId);
        bool UpdateT1601FehlercodesById(T1601FehlercodesDTO T1601Fehlercodes, int T1601FehlercodesId);
        bool DeleteT1601FehlercodesById(int T1601FehlercodesId);
        List<T1601FehlercodesDTO> GetT1601Fehlercodes();
        #endregion

        #region TechnikerEinsatzStatus
        bool AddTechnikerEinsatzStatus(TechnikerEinsatzStatusDTO technikerEinsatzStatus);
        TechnikerEinsatzStatusDTO GetTechnikerEinsatzStatusById(int technikerEinsatzStatusId);
        bool UpdateTechnikerEinsatzStatusById(TechnikerEinsatzStatusDTO technikerEinsatzStatus, int technikerEinsatzStatusId);
        bool DeleteTechnikerEinsatzStatusById(int technikerEinsatzStatusId);
        List<TechnikerEinsatzStatusDTO> GetTechnikerEinsatzStatus();
        #endregion

        #region Ansprechpartner
        bool AddT0120Ansprechpartner(T0120AnsprechpartnerDTO T0120Ansprechpartner);
        T0120AnsprechpartnerDTO GetT0120AnsprechpartnerById(int T0120AnsprechpartnerId);
        bool UpdateT0120AnsprechpartnerById(T0120AnsprechpartnerDTO T0120Ansprechpartner, int T0120AnsprechpartnerId);
        bool DeleteT0120AnsprechpartnerById(int T0120AnsprechpartnerId);
        List<T0120AnsprechpartnerDTO> GetT0120Ansprechpartner();
        #endregion

        #region Lagerortbestand
        bool AddT0504Lagerortbestand(T0504LagerortbestandDTO T0504Lagerortbestand);
        T0504LagerortbestandDTO GetT0504LagerortbestandById(int T0504LagerortbestandId);
        bool UpdateT0504LagerortbestandById(T0504LagerortbestandDTO T0504Lagerortbestand, int T0504LagerortbestandId);
        bool DeleteT0504LagerortbestandById(int T0504LagerortbestandId);
        List<T0504LagerortbestandDTO> GetT0504Lagerortbestaende();
        #endregion

        #region Zaehlwerk
        bool AddT0432Zaehlwerk(T0432ZaehlwerkDTO T0432Zaehlwerk);
        T0432ZaehlwerkDTO GetT0432ZaehlwerkById(int T0432ZaehlwerkId);
        bool UpdateT0432ZaehlwerkById(T0432ZaehlwerkDTO T0432Zaehlwerk, int T0432ZaehlwerkId);
        bool DeleteT0432ZaehlwerkById(int T0432ZaehlwerkId);
        List<T0432ZaehlwerkDTO> GetT0432Zaehlwerke();
        #endregion

        #region ZaehlwerkTyp
        bool AddT0431ZaehlwerkTyp(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp);
        T0431ZaehlwerkTypDTO GetT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId);
        bool UpdateT0431ZaehlwerkTypById(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp, int T0431ZaehlwerkTypId);
        bool DeleteT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId);
        List<T0431ZaehlwerkTypDTO> GetT0431Zaehlwerktypen();
        #endregion

        #region Standort
        bool AddT0434Standort(T0434StandortDTO T0434Standort);
        T0434StandortDTO GetT0434StandortById(int T0434StandortId);
        bool UpdateT0434StandortById(T0434StandortDTO T0434Standort, int T0434StandortId);
        bool DeleteT0434StandortById(int T0434StandortId);
        List<T0434StandortDTO> GetT0434Standorte();
        #endregion

        #region Prozessposition
        bool AddT0803Prozessposition(T0803ProzesspositionDTO T0803Prozessposition);
        T0803ProzesspositionDTO GetT0803ProzesspositionById(int T0803ProzesspositionId);
        bool UpdateT0803ProzesspositionById(T0803ProzesspositionDTO T0803Prozessposition, int T0803ProzesspositionId);
        bool DeleteT0803ProzesspositionById(int T0803ProzesspositionId);
        List<T0803ProzesspositionDTO> GetT0803Prozesspositionen();
        #endregion

        #region Person
        bool AddT0110Person(T0110PersonDTO T0110Person);
        T0110PersonDTO GetT0110PersonById(int T0110PersonId);
        bool UpdateT0110PersonById(T0110PersonDTO T0110Person, int T0110PersonId);
        bool DeleteT0110PersonById(int T0110PersonId);
        List<T0110PersonDTO> GetT0110Personen();
        #endregion

        #region Bankverbindung
        bool AddT0123Bankverbindung(T0123BankverbindungDTO T0123Bankverbindung);
        T0123BankverbindungDTO GetT0123BankverbindungById(int T0123BankverbindungId);
        bool UpdateT0123BankverbindungById(T0123BankverbindungDTO T0123Bankverbindung, int T0123BankverbindungId);
        bool DeleteT0123BankverbindungById(int T0123BankverbindungId);
        List<T0123BankverbindungDTO> GetT0123Bankverbindungen();
        #endregion

        #region VDE
        bool AddVDE(VDEDTO VDE);
        VDEDTO GetVDEById(int VDEId);
        bool UpdateVDEById(VDEDTO VDE, int VDEId);
        bool DeleteVDEById(int VDEId);
        List<VDEDTO> GetVDEs();
        #endregion

        #region SNRBewegung
        bool AddT0511SNRBewegung(T0511SNRBewegungDTO T0511SNRBewegung);
        T0511SNRBewegungDTO GetT0511SNRBewegungById(int T0511SNRBewegungId);
        bool UpdateT0511SNRBewegungById(T0511SNRBewegungDTO T0511SNRBewegung, int T0511SNRBewegungId);
        bool DeleteT0511SNRBewegungById(int T0511SNRBewegungId);
        List<T0511SNRBewegungDTO> GetT0511SNRBewegungen();
        #endregion

        #region Vertragsmaschine
        bool AddT0606Vertragsmaschine(T0606VertragsmaschineDTO T0606Vertragsmaschine);
        T0606VertragsmaschineDTO GetT0606VertragsmaschineById(int T0606VertragsmaschineId);
        bool UpdateT0606VertragsmaschineById(T0606VertragsmaschineDTO T0606Vertragsmaschine, int T0606VertragsmaschineId);
        bool DeleteT0606VertragsmaschineById(int T0606VertragsmaschineId);
        List<T0606VertragsmaschineDTO> GetT0606Vertragsmaschinen();
        #endregion

        #region GPKonditionenWgrp
        bool AddT0117GPKonditionenWgrp(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp);
        T0117GPKonditionenWgrpDTO GetT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId);
        bool UpdateT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp, int T0117GPKonditionenWgrpId);
        bool DeleteT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId);
        List<T0117GPKonditionenWgrpDTO> GetT0117GPKonditionenWgrp();
        #endregion

        #region GPKonditionenArtikel
        bool AddT0118GPKonditionenArtikel(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel);
        T0118GPKonditionenArtikelDTO GetT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId);
        bool UpdateT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel, int T0118GPKonditionenArtikelId);
        bool DeleteT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId);
        List<T0118GPKonditionenArtikelDTO> GetT0118GPKonditionenArtikel();
        #endregion

        DateTime GetCurrentDateTime();

        bool TestConnection();
    }
}

