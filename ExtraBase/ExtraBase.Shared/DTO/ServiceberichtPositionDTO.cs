using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class ServiceberichtPositionDTO
    {
        [DataMember()]
        public Int32 ServiceberichtPositionID { get; set; }

        [DataMember()]
        public Int32 Menge { get; set; }

        [DataMember()]
        public Int32 PositionsNummer { get; set; }

        [DataMember()]
        public Int32 HauptprozessID { get; set; }

        [DataMember()]
        public String BemerkungKunde { get; set; }

        [DataMember()]
        public String BemerkungIntern { get; set; }

        [DataMember()]
        public Decimal VerkaufsPreisEinzel { get; set; }

        [DataMember()]
        public Nullable<Decimal> Rabatt { get; set; }

        public ServiceberichtPositionDTO()
        {
        }

        public ServiceberichtPositionDTO(Int32 serviceberichtPositionID, Int32 menge, Int32 positionsNummer, Int32 hauptprozessID, String bemerkungKunde, String bemerkungIntern, Decimal verkaufsPreisEinzel, Nullable<Decimal> rabatt)
        {
			this.ServiceberichtPositionID = serviceberichtPositionID;
			this.Menge = menge;
			this.PositionsNummer = positionsNummer;
			this.HauptprozessID = hauptprozessID;
			this.BemerkungKunde = bemerkungKunde;
			this.BemerkungIntern = bemerkungIntern;
			this.VerkaufsPreisEinzel = verkaufsPreisEinzel;
			this.Rabatt = rabatt;
        }
    }
}
