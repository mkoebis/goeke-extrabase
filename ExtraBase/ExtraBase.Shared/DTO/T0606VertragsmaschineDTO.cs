using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0606VertragsmaschineDTO
    {
        [DataMember()]
        public Int32 TF0606VertragsmaschineID { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606Einbuchung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606Endabrechnung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606MaschinenPauschale { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606AnfangsZaehlerstand { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606EndZaehlerstand { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0606MaschineID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606Serviceanteil { get; set; }

        [DataMember()]
        public String TF0606AbrTextMaschPauschale { get; set; }

        [DataMember()]
        public String TF0606SBE { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606Erfassung { get; set; }

        [DataMember()]
        public String TF0606SBA { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606Aenderung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606ServiceanteilProz { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606PaBeEndtermin { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606SaBeEndtermin { get; set; }

        [DataMember()]
        public Decimal TF0606RefiAnpassung { get; set; }

        [DataMember()]
        public Decimal TF0606Abholauftrag { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606Zeitwert { get; set; }

        [DataMember()]
        public Decimal TF0606Eigentumsuebergang { get; set; }

        [DataMember()]
        public Decimal TF0606PaEndabgerechnet { get; set; }

        [DataMember()]
        public Decimal TF0606SeEndabgerechnet { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606Objektwert { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606SeZyklusstart { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606SeBerechnungsstart { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606SeZahlzyklus { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606PaZyklusstart { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0606PaBerechnungsstart { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606PaZahlzyklus { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606Poolmaschine { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0606Aktiv { get; set; }

        public T0606VertragsmaschineDTO()
        {
        }

        public T0606VertragsmaschineDTO(Int32 tF0606VertragsmaschineID, Nullable<DateTime> tF0606Einbuchung, Nullable<DateTime> tF0606Endabrechnung, Nullable<Decimal> tF0606MaschinenPauschale, Nullable<Decimal> tF0606AnfangsZaehlerstand, Nullable<Decimal> tF0606EndZaehlerstand, Nullable<Int32> tF0606MaschineID, Nullable<Decimal> tF0606Serviceanteil, String tF0606AbrTextMaschPauschale, String tF0606SBE, Nullable<DateTime> tF0606Erfassung, String tF0606SBA, Nullable<DateTime> tF0606Aenderung, Nullable<Decimal> tF0606ServiceanteilProz, Nullable<DateTime> tF0606PaBeEndtermin, Nullable<DateTime> tF0606SaBeEndtermin, Decimal tF0606RefiAnpassung, Decimal tF0606Abholauftrag, Nullable<Decimal> tF0606Zeitwert, Decimal tF0606Eigentumsuebergang, Decimal tF0606PaEndabgerechnet, Decimal tF0606SeEndabgerechnet, Nullable<Decimal> tF0606Objektwert, Nullable<DateTime> tF0606SeZyklusstart, Nullable<DateTime> tF0606SeBerechnungsstart, Nullable<Decimal> tF0606SeZahlzyklus, Nullable<DateTime> tF0606PaZyklusstart, Nullable<DateTime> tF0606PaBerechnungsstart, Nullable<Decimal> tF0606PaZahlzyklus, Nullable<Decimal> tF0606Poolmaschine, Nullable<Decimal> tF0606Aktiv)
        {
			this.TF0606VertragsmaschineID = tF0606VertragsmaschineID;
			this.TF0606Einbuchung = tF0606Einbuchung;
			this.TF0606Endabrechnung = tF0606Endabrechnung;
			this.TF0606MaschinenPauschale = tF0606MaschinenPauschale;
			this.TF0606AnfangsZaehlerstand = tF0606AnfangsZaehlerstand;
			this.TF0606EndZaehlerstand = tF0606EndZaehlerstand;
			this.TF0606MaschineID = tF0606MaschineID;
			this.TF0606Serviceanteil = tF0606Serviceanteil;
			this.TF0606AbrTextMaschPauschale = tF0606AbrTextMaschPauschale;
			this.TF0606SBE = tF0606SBE;
			this.TF0606Erfassung = tF0606Erfassung;
			this.TF0606SBA = tF0606SBA;
			this.TF0606Aenderung = tF0606Aenderung;
			this.TF0606ServiceanteilProz = tF0606ServiceanteilProz;
			this.TF0606PaBeEndtermin = tF0606PaBeEndtermin;
			this.TF0606SaBeEndtermin = tF0606SaBeEndtermin;
			this.TF0606RefiAnpassung = tF0606RefiAnpassung;
			this.TF0606Abholauftrag = tF0606Abholauftrag;
			this.TF0606Zeitwert = tF0606Zeitwert;
			this.TF0606Eigentumsuebergang = tF0606Eigentumsuebergang;
			this.TF0606PaEndabgerechnet = tF0606PaEndabgerechnet;
			this.TF0606SeEndabgerechnet = tF0606SeEndabgerechnet;
			this.TF0606Objektwert = tF0606Objektwert;
			this.TF0606SeZyklusstart = tF0606SeZyklusstart;
			this.TF0606SeBerechnungsstart = tF0606SeBerechnungsstart;
			this.TF0606SeZahlzyklus = tF0606SeZahlzyklus;
			this.TF0606PaZyklusstart = tF0606PaZyklusstart;
			this.TF0606PaBerechnungsstart = tF0606PaBerechnungsstart;
			this.TF0606PaZahlzyklus = tF0606PaZahlzyklus;
			this.TF0606Poolmaschine = tF0606Poolmaschine;
			this.TF0606Aktiv = tF0606Aktiv;
        }
    }
}
