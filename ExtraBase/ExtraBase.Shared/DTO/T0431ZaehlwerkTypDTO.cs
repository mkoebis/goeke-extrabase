using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0431ZaehlwerkTypDTO
    {
        [DataMember()]
        public Int32 TF0431ZaehlwerktypID { get; set; }

        [DataMember()]
        public String TF0431ZaehlwerktypBezeichnung { get; set; }

        [DataMember()]
        public Decimal TF0431Faktor { get; set; }

        [DataMember()]
        public Int32 TF0431Abrechnungseinheit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0431VerteilungFreikopien { get; set; }

        [DataMember()]
        public Int32 T0401Angebot_TF0401AngebotID { get; set; }

        [DataMember()]
        public List<Int32> T0432Zaehlwerk_TF0432ZaehlwerkID { get; set; }

        public T0431ZaehlwerkTypDTO()
        {
        }

        public T0431ZaehlwerkTypDTO(Int32 tF0431ZaehlwerktypID, String tF0431ZaehlwerktypBezeichnung, Decimal tF0431Faktor, Int32 tF0431Abrechnungseinheit, Nullable<Decimal> tF0431VerteilungFreikopien, Int32 t0401Angebot_TF0401AngebotID, List<Int32> t0432Zaehlwerk_TF0432ZaehlwerkID)
        {
			this.TF0431ZaehlwerktypID = tF0431ZaehlwerktypID;
			this.TF0431ZaehlwerktypBezeichnung = tF0431ZaehlwerktypBezeichnung;
			this.TF0431Faktor = tF0431Faktor;
			this.TF0431Abrechnungseinheit = tF0431Abrechnungseinheit;
			this.TF0431VerteilungFreikopien = tF0431VerteilungFreikopien;
			this.T0401Angebot_TF0401AngebotID = t0401Angebot_TF0401AngebotID;
			this.T0432Zaehlwerk_TF0432ZaehlwerkID = t0432Zaehlwerk_TF0432ZaehlwerkID;
        }
    }
}
