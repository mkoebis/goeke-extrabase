using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0120AnsprechpartnerDTO
    {
        [DataMember()]
        public Int32 TF0120AnsprechpartnerID { get; set; }

        [DataMember()]
        public Int32 TF0120PersonID { get; set; }

        [DataMember()]
        public String TF0120Telefon { get; set; }

        [DataMember()]
        public String TF0120Fax { get; set; }

        [DataMember()]
        public String TF0120Email { get; set; }

        [DataMember()]
        public String TF0120Mobil { get; set; }

        [DataMember()]
        public String TF0120Vorwahl { get; set; }

        [DataMember()]
        public String TF0120Abteilung { get; set; }

        [DataMember()]
        public String TF0120Funktion { get; set; }

        [DataMember()]
        public Decimal TF0120Mitarbeiter { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Int32 T0108Anschrift_TF0108AnschriftID { get; set; }

        public T0120AnsprechpartnerDTO()
        {
        }

        public T0120AnsprechpartnerDTO(Int32 tF0120AnsprechpartnerID, Int32 tF0120PersonID, String tF0120Telefon, String tF0120Fax, String tF0120Email, String tF0120Mobil, String tF0120Vorwahl, String tF0120Abteilung, String tF0120Funktion, Decimal tF0120Mitarbeiter, Guid rowguid, Int32 t0108Anschrift_TF0108AnschriftID)
        {
			this.TF0120AnsprechpartnerID = tF0120AnsprechpartnerID;
			this.TF0120PersonID = tF0120PersonID;
			this.TF0120Telefon = tF0120Telefon;
			this.TF0120Fax = tF0120Fax;
			this.TF0120Email = tF0120Email;
			this.TF0120Mobil = tF0120Mobil;
			this.TF0120Vorwahl = tF0120Vorwahl;
			this.TF0120Abteilung = tF0120Abteilung;
			this.TF0120Funktion = tF0120Funktion;
			this.TF0120Mitarbeiter = tF0120Mitarbeiter;
			this.rowguid = rowguid;
			this.T0108Anschrift_TF0108AnschriftID = t0108Anschrift_TF0108AnschriftID;
        }
    }
}
