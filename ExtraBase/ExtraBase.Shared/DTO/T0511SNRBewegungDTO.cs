using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0511SNRBewegungDTO
    {
        [DataMember()]
        public Int32 TF0511SNRBewegungID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0511LagerortBewID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0511Warenwert { get; set; }

        public T0511SNRBewegungDTO()
        {
        }

        public T0511SNRBewegungDTO(Int32 tF0511SNRBewegungID, Nullable<Int32> tF0511LagerortBewID, Nullable<Decimal> tF0511Warenwert)
        {
			this.TF0511SNRBewegungID = tF0511SNRBewegungID;
			this.TF0511LagerortBewID = tF0511LagerortBewID;
			this.TF0511Warenwert = tF0511Warenwert;
        }
    }
}
