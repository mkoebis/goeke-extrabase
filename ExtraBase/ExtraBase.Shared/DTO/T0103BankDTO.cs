using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0103BankDTO
    {
        [DataMember()]
        public Int32 TF0103BankID { get; set; }

        [DataMember()]
        public String TF0103BLZ { get; set; }

        [DataMember()]
        public String TF0103Bank { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Boolean> Hide { get; set; }

        public T0103BankDTO()
        {
        }

        public T0103BankDTO(Int32 tF0103BankID, String tF0103BLZ, String tF0103Bank, Guid rowguid, Nullable<Boolean> hide)
        {
			this.TF0103BankID = tF0103BankID;
			this.TF0103BLZ = tF0103BLZ;
			this.TF0103Bank = tF0103Bank;
			this.rowguid = rowguid;
			this.Hide = hide;
        }
    }
}
