using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class VDEDTO
    {
        [DataMember()]
        public Int32 VdeId { get; set; }

        [DataMember()]
        public DateTime Datum { get; set; }

        [DataMember()]
        public Int32 AuftragId { get; set; }

        [DataMember()]
        public Nullable<Boolean> PruefungNachReparatur { get; set; }

        [DataMember()]
        public Nullable<Boolean> Wiederholungspruefung { get; set; }

        [DataMember()]
        public Nullable<Int32> MaschinenId { get; set; }

        [DataMember()]
        public String MaschinenSeriennummer { get; set; }

        [DataMember()]
        public String MaschinenModell { get; set; }

        [DataMember()]
        public String MaschinenHersteller { get; set; }

        [DataMember()]
        public Nullable<Decimal> Nennspannung { get; set; }

        [DataMember()]
        public Nullable<Decimal> Nennstrom { get; set; }

        [DataMember()]
        public Nullable<Decimal> Nennleistung { get; set; }

        [DataMember()]
        public Nullable<Decimal> Frequenz { get; set; }

        [DataMember()]
        public String Schutzklasse { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsGehaeuseIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsAnschlussleitungIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsBemessungDerZugaenglichenSicherungIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsLeitungsfuehrungIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsZugentlastungsvorrichtungIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsKuehlluftoeffnungenIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsLesbarkeitIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsSicherungshalterIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsFunktionsfaehigkeitIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsZustandDerSchutzIo { get; set; }

        [DataMember()]
        public Nullable<Boolean> HasMechanischeGefaehrdung { get; set; }

        [DataMember()]
        public Nullable<Boolean> HasUnzulaessigeEingriffe { get; set; }

        [DataMember()]
        public Nullable<Boolean> HasAnzeichenVonGebrauch { get; set; }

        [DataMember()]
        public Nullable<Boolean> HasAnzeichenVonUeberlastung { get; set; }

        [DataMember()]
        public Nullable<Boolean> HasVerschmutzungUndKorosion { get; set; }

        [DataMember()]
        public Nullable<Decimal> Schutzleiterwiederstand { get; set; }

        [DataMember()]
        public Nullable<Decimal> Isolationswiderstand { get; set; }

        [DataMember()]
        public Nullable<Decimal> Ersatzableitstrom { get; set; }

        [DataMember()]
        public Nullable<Decimal> Schutzleiterstrom { get; set; }

        [DataMember()]
        public Nullable<Decimal> Beruehrungsstrom { get; set; }

        [DataMember()]
        public Nullable<Boolean> HinweiseKeineMaengel { get; set; }

        [DataMember()]
        public Nullable<Boolean> HinweiseMaengelReparieren { get; set; }

        [DataMember()]
        public Nullable<Boolean> HinweiseFestgestellteMaengel { get; set; }

        [DataMember()]
        public Nullable<Boolean> HinweiseNichtVerwenden { get; set; }

        [DataMember()]
        public Nullable<Boolean> Pruefplakette { get; set; }

        [DataMember()]
        public DateTime DatumTermin { get; set; }

        [DataMember()]
        public Int32 TechnikerNummer { get; set; }

        [DataMember()]
        public String MessgeraetTyp { get; set; }

        [DataMember()]
        public String MessgeraetFabrikat { get; set; }

        [DataMember()]
        public String SchutzleiterwiderstandZusatz { get; set; }

        [DataMember()]
        public String IsolationswiderstandZusatz { get; set; }

        public VDEDTO()
        {
        }

        public VDEDTO(Int32 vdeId, DateTime datum, Int32 auftragId, Nullable<Boolean> pruefungNachReparatur, Nullable<Boolean> wiederholungspruefung, Nullable<Int32> maschinenId, String maschinenSeriennummer, String maschinenModell, String maschinenHersteller, Nullable<Decimal> nennspannung, Nullable<Decimal> nennstrom, Nullable<Decimal> nennleistung, Nullable<Decimal> frequenz, String schutzklasse, Nullable<Boolean> isGehaeuseIo, Nullable<Boolean> isAnschlussleitungIo, Nullable<Boolean> isBemessungDerZugaenglichenSicherungIo, Nullable<Boolean> isLeitungsfuehrungIo, Nullable<Boolean> isZugentlastungsvorrichtungIo, Nullable<Boolean> isKuehlluftoeffnungenIo, Nullable<Boolean> isLesbarkeitIo, Nullable<Boolean> isSicherungshalterIo, Nullable<Boolean> isFunktionsfaehigkeitIo, Nullable<Boolean> isZustandDerSchutzIo, Nullable<Boolean> hasMechanischeGefaehrdung, Nullable<Boolean> hasUnzulaessigeEingriffe, Nullable<Boolean> hasAnzeichenVonGebrauch, Nullable<Boolean> hasAnzeichenVonUeberlastung, Nullable<Boolean> hasVerschmutzungUndKorosion, Nullable<Decimal> schutzleiterwiederstand, Nullable<Decimal> isolationswiderstand, Nullable<Decimal> ersatzableitstrom, Nullable<Decimal> schutzleiterstrom, Nullable<Decimal> beruehrungsstrom, Nullable<Boolean> hinweiseKeineMaengel, Nullable<Boolean> hinweiseMaengelReparieren, Nullable<Boolean> hinweiseFestgestellteMaengel, Nullable<Boolean> hinweiseNichtVerwenden, Nullable<Boolean> pruefplakette, DateTime datumTermin, Int32 technikerNummer, String messgeraetTyp, String messgeraetFabrikat, String schutzleiterwiderstandZusatz, String isolationswiderstandZusatz)
        {
			this.VdeId = vdeId;
			this.Datum = datum;
			this.AuftragId = auftragId;
			this.PruefungNachReparatur = pruefungNachReparatur;
			this.Wiederholungspruefung = wiederholungspruefung;
			this.MaschinenId = maschinenId;
			this.MaschinenSeriennummer = maschinenSeriennummer;
			this.MaschinenModell = maschinenModell;
			this.MaschinenHersteller = maschinenHersteller;
			this.Nennspannung = nennspannung;
			this.Nennstrom = nennstrom;
			this.Nennleistung = nennleistung;
			this.Frequenz = frequenz;
			this.Schutzklasse = schutzklasse;
			this.IsGehaeuseIo = isGehaeuseIo;
			this.IsAnschlussleitungIo = isAnschlussleitungIo;
			this.IsBemessungDerZugaenglichenSicherungIo = isBemessungDerZugaenglichenSicherungIo;
			this.IsLeitungsfuehrungIo = isLeitungsfuehrungIo;
			this.IsZugentlastungsvorrichtungIo = isZugentlastungsvorrichtungIo;
			this.IsKuehlluftoeffnungenIo = isKuehlluftoeffnungenIo;
			this.IsLesbarkeitIo = isLesbarkeitIo;
			this.IsSicherungshalterIo = isSicherungshalterIo;
			this.IsFunktionsfaehigkeitIo = isFunktionsfaehigkeitIo;
			this.IsZustandDerSchutzIo = isZustandDerSchutzIo;
			this.HasMechanischeGefaehrdung = hasMechanischeGefaehrdung;
			this.HasUnzulaessigeEingriffe = hasUnzulaessigeEingriffe;
			this.HasAnzeichenVonGebrauch = hasAnzeichenVonGebrauch;
			this.HasAnzeichenVonUeberlastung = hasAnzeichenVonUeberlastung;
			this.HasVerschmutzungUndKorosion = hasVerschmutzungUndKorosion;
			this.Schutzleiterwiederstand = schutzleiterwiederstand;
			this.Isolationswiderstand = isolationswiderstand;
			this.Ersatzableitstrom = ersatzableitstrom;
			this.Schutzleiterstrom = schutzleiterstrom;
			this.Beruehrungsstrom = beruehrungsstrom;
			this.HinweiseKeineMaengel = hinweiseKeineMaengel;
			this.HinweiseMaengelReparieren = hinweiseMaengelReparieren;
			this.HinweiseFestgestellteMaengel = hinweiseFestgestellteMaengel;
			this.HinweiseNichtVerwenden = hinweiseNichtVerwenden;
			this.Pruefplakette = pruefplakette;
			this.DatumTermin = datumTermin;
			this.TechnikerNummer = technikerNummer;
			this.MessgeraetTyp = messgeraetTyp;
			this.MessgeraetFabrikat = messgeraetFabrikat;
			this.SchutzleiterwiderstandZusatz = schutzleiterwiderstandZusatz;
			this.IsolationswiderstandZusatz = isolationswiderstandZusatz;
        }
    }
}
