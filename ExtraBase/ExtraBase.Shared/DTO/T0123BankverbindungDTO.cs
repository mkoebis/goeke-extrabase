using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0123BankverbindungDTO
    {
        [DataMember()]
        public Int32 TF0123BankverbindungID { get; set; }

        [DataMember()]
        public Int32 TF0123BankID { get; set; }

        [DataMember()]
        public String TF0123Kontonr { get; set; }

        [DataMember()]
        public String TF0123Kontoinhaber { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0123BankverbindungDTO()
        {
        }

        public T0123BankverbindungDTO(Int32 tF0123BankverbindungID, Int32 tF0123BankID, String tF0123Kontonr, String tF0123Kontoinhaber, Guid rowguid)
        {
			this.TF0123BankverbindungID = tF0123BankverbindungID;
			this.TF0123BankID = tF0123BankID;
			this.TF0123Kontonr = tF0123Kontonr;
			this.TF0123Kontoinhaber = tF0123Kontoinhaber;
			this.rowguid = rowguid;
        }
    }
}
