using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0813TechnikerStatusDTO
    {
        [DataMember()]
        public Int32 TF0813TechnikerStatusID { get; set; }

        [DataMember()]
        public String TF0813StatusBezeichnung { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public List<Int32> T0802Prozesskopf_TF0802ProzesskopfID { get; set; }

        public T0813TechnikerStatusDTO()
        {
        }

        public T0813TechnikerStatusDTO(Int32 tF0813TechnikerStatusID, String tF0813StatusBezeichnung, Guid rowguid, List<Int32> t0802Prozesskopf_TF0802ProzesskopfID)
        {
			this.TF0813TechnikerStatusID = tF0813TechnikerStatusID;
			this.TF0813StatusBezeichnung = tF0813StatusBezeichnung;
			this.rowguid = rowguid;
			this.T0802Prozesskopf_TF0802ProzesskopfID = t0802Prozesskopf_TF0802ProzesskopfID;
        }
    }
}
