using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0302LagerortDTO
    {
        [DataMember()]
        public Int32 TF0302LagerortID { get; set; }

        [DataMember()]
        public String TF0302Lagerortbezeichnung { get; set; }

        [DataMember()]
        public String TF0302Lagerortbeschreibung { get; set; }

        [DataMember()]
        public List<Int32> T0143Techniker_TF0143TechnikerID { get; set; }

        [DataMember()]
        public Int32 T0301Lager_TF0301LagerID { get; set; }

        [DataMember()]
        public List<Int32> T0504Lagerortbestand_TF0504LagerortbestandID { get; set; }

        public T0302LagerortDTO()
        {
        }

        public T0302LagerortDTO(Int32 tF0302LagerortID, String tF0302Lagerortbezeichnung, String tF0302Lagerortbeschreibung, List<Int32> t0143Techniker_TF0143TechnikerID, Int32 t0301Lager_TF0301LagerID, List<Int32> t0504Lagerortbestand_TF0504LagerortbestandID)
        {
			this.TF0302LagerortID = tF0302LagerortID;
			this.TF0302Lagerortbezeichnung = tF0302Lagerortbezeichnung;
			this.TF0302Lagerortbeschreibung = tF0302Lagerortbeschreibung;
			this.T0143Techniker_TF0143TechnikerID = t0143Techniker_TF0143TechnikerID;
			this.T0301Lager_TF0301LagerID = t0301Lager_TF0301LagerID;
			this.T0504Lagerortbestand_TF0504LagerortbestandID = t0504Lagerortbestand_TF0504LagerortbestandID;
        }
    }
}
