using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0143TechnikerDTO
    {
        [DataMember()]
        public Int32 TF0143TechnikerID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0143KostenstelleID { get; set; }

        [DataMember()]
        public String AdressIdCanon { get; set; }

        [DataMember()]
        public Int16 Vorschauberechtigung { get; set; }

        [DataMember()]
        public Boolean Aussendienst { get; set; }

        [DataMember()]
        public Nullable<Int32> ServerVersionen { get; set; }

        [DataMember()]
        public String Bemerkung { get; set; }

        [DataMember()]
        public Int32 T0108Anschrift_TF0108AnschriftID { get; set; }

        [DataMember()]
        public Int32 T0302Lagerort_TF0302LagerortID { get; set; }

        public T0143TechnikerDTO()
        {
        }

        public T0143TechnikerDTO(Int32 tF0143TechnikerID, Nullable<Int32> tF0143KostenstelleID, String adressIdCanon, Int16 vorschauberechtigung, Boolean aussendienst, Nullable<Int32> serverVersionen, String bemerkung, Int32 t0108Anschrift_TF0108AnschriftID, Int32 t0302Lagerort_TF0302LagerortID)
        {
			this.TF0143TechnikerID = tF0143TechnikerID;
			this.TF0143KostenstelleID = tF0143KostenstelleID;
			this.AdressIdCanon = adressIdCanon;
			this.Vorschauberechtigung = vorschauberechtigung;
			this.Aussendienst = aussendienst;
			this.ServerVersionen = serverVersionen;
			this.Bemerkung = bemerkung;
			this.T0108Anschrift_TF0108AnschriftID = t0108Anschrift_TF0108AnschriftID;
			this.T0302Lagerort_TF0302LagerortID = t0302Lagerort_TF0302LagerortID;
        }
    }
}
