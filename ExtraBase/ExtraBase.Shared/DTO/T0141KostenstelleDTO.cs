using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0141KostenstelleDTO
    {
        [DataMember()]
        public Int32 TF0141KostenstelleID { get; set; }

        [DataMember()]
        public String TF0141Nummer { get; set; }

        [DataMember()]
        public String TF0141Bezeichnung { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Boolean> Hide { get; set; }

        public T0141KostenstelleDTO()
        {
        }

        public T0141KostenstelleDTO(Int32 tF0141KostenstelleID, String tF0141Nummer, String tF0141Bezeichnung, Guid rowguid, Nullable<Boolean> hide)
        {
			this.TF0141KostenstelleID = tF0141KostenstelleID;
			this.TF0141Nummer = tF0141Nummer;
			this.TF0141Bezeichnung = tF0141Bezeichnung;
			this.rowguid = rowguid;
			this.Hide = hide;
        }
    }
}
