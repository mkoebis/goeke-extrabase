using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0430MaschinenstammDTO
    {
        [DataMember()]
        public Int32 TF0430MaschinenstammID { get; set; }

        [DataMember()]
        public String TF0430Baujahr { get; set; }

        [DataMember()]
        public String TF0430SerienNr { get; set; }

        [DataMember()]
        public Decimal TF0430Maschine { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Eigentuemer { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0430Garantie { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Garantieeinheit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0430Gewaehleistung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Gewaehleistungseinheit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0430Abrechnungswert { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Standort { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Lieferant { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430Besitzer { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430GesamtZahlerstand { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0430Altbestand { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0430Bestandswert { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430LagerortbestandID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0430ProzessPosID { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0430Kundengarantie { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0430Lieferantengarantie { get; set; }

        [DataMember()]
        public Boolean Hide { get; set; }

        [DataMember()]
        public String RefiVertragNummer { get; set; }

        [DataMember()]
        public String FirmwareMain { get; set; }

        [DataMember()]
        public String FirmwareEngine { get; set; }

        [DataMember()]
        public Nullable<Int32> SWZaehlerMax { get; set; }

        [DataMember()]
        public Nullable<Int32> ColorZaehlerMax { get; set; }

        [DataMember()]
        public Int32 T0401Angebot_TF0401AngebotID { get; set; }

        [DataMember()]
        public List<Int32> T0432Zaehlwerk_TF0432ZaehlwerkID { get; set; }

        public T0430MaschinenstammDTO()
        {
        }

        public T0430MaschinenstammDTO(Int32 tF0430MaschinenstammID, String tF0430Baujahr, String tF0430SerienNr, Decimal tF0430Maschine, Nullable<Int32> tF0430Eigentuemer, Nullable<Decimal> tF0430Garantie, Nullable<Int32> tF0430Garantieeinheit, Nullable<Decimal> tF0430Gewaehleistung, Nullable<Int32> tF0430Gewaehleistungseinheit, Nullable<Decimal> tF0430Abrechnungswert, Nullable<Int32> tF0430Standort, Nullable<Int32> tF0430Lieferant, Nullable<Int32> tF0430Besitzer, Nullable<Int32> tF0430GesamtZahlerstand, Nullable<Decimal> tF0430Altbestand, Nullable<Decimal> tF0430Bestandswert, Nullable<Int32> tF0430LagerortbestandID, Nullable<Int32> tF0430ProzessPosID, Nullable<DateTime> tF0430Kundengarantie, Nullable<DateTime> tF0430Lieferantengarantie, Boolean hide, String refiVertragNummer, String firmwareMain, String firmwareEngine, Nullable<Int32> sWZaehlerMax, Nullable<Int32> colorZaehlerMax, Int32 t0401Angebot_TF0401AngebotID, List<Int32> t0432Zaehlwerk_TF0432ZaehlwerkID)
        {
			this.TF0430MaschinenstammID = tF0430MaschinenstammID;
			this.TF0430Baujahr = tF0430Baujahr;
			this.TF0430SerienNr = tF0430SerienNr;
			this.TF0430Maschine = tF0430Maschine;
			this.TF0430Eigentuemer = tF0430Eigentuemer;
			this.TF0430Garantie = tF0430Garantie;
			this.TF0430Garantieeinheit = tF0430Garantieeinheit;
			this.TF0430Gewaehleistung = tF0430Gewaehleistung;
			this.TF0430Gewaehleistungseinheit = tF0430Gewaehleistungseinheit;
			this.TF0430Abrechnungswert = tF0430Abrechnungswert;
			this.TF0430Standort = tF0430Standort;
			this.TF0430Lieferant = tF0430Lieferant;
			this.TF0430Besitzer = tF0430Besitzer;
			this.TF0430GesamtZahlerstand = tF0430GesamtZahlerstand;
			this.TF0430Altbestand = tF0430Altbestand;
			this.TF0430Bestandswert = tF0430Bestandswert;
			this.TF0430LagerortbestandID = tF0430LagerortbestandID;
			this.TF0430ProzessPosID = tF0430ProzessPosID;
			this.TF0430Kundengarantie = tF0430Kundengarantie;
			this.TF0430Lieferantengarantie = tF0430Lieferantengarantie;
			this.Hide = hide;
			this.RefiVertragNummer = refiVertragNummer;
			this.FirmwareMain = firmwareMain;
			this.FirmwareEngine = firmwareEngine;
			this.SWZaehlerMax = sWZaehlerMax;
			this.ColorZaehlerMax = colorZaehlerMax;
			this.T0401Angebot_TF0401AngebotID = t0401Angebot_TF0401AngebotID;
			this.T0432Zaehlwerk_TF0432ZaehlwerkID = t0432Zaehlwerk_TF0432ZaehlwerkID;
        }
    }
}
