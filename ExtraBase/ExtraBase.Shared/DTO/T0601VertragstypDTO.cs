using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0601VertragstypDTO
    {
        [DataMember()]
        public Int32 TF0601VertragstypID { get; set; }

        [DataMember()]
        public String TF0601Vertragstypbezeichnung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601Laufzeit { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601Verlaengerung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601Kuendigungsfrist { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601Vertragspauschale { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601Serviceanteil { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601PauschaleZZ { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601PauschaleZA { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601KopienAbrZZ { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601KopienAbrZA { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601Abrechnungsoption { get; set; }

        [DataMember()]
        public String TF0601AbrPauschaleText { get; set; }

        [DataMember()]
        public String TF0601AbrKopienText { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601Freikopien { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601Mindestkopien { get; set; }

        [DataMember()]
        public Decimal TF0601Abrechnungspool { get; set; }

        [DataMember()]
        public String TF0601SBE { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0601Erfassung { get; set; }

        [DataMember()]
        public String TF0601SBA { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0601Aenderung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601ServiceanteilProz { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601Preiseinheit { get; set; }

        [DataMember()]
        public Decimal TF0601aktiviert { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0601VertragstypWaehrung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0601ServiceZZ { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Boolean> TF0601Hide { get; set; }

        [DataMember()]
        public List<Int32> T0602Vertrag_TF0602VertragID { get; set; }

        public T0601VertragstypDTO()
        {
        }

        public T0601VertragstypDTO(Int32 tF0601VertragstypID, String tF0601Vertragstypbezeichnung, Nullable<Int32> tF0601Laufzeit, Nullable<Int32> tF0601Verlaengerung, Nullable<Int32> tF0601Kuendigungsfrist, Nullable<Decimal> tF0601Vertragspauschale, Nullable<Decimal> tF0601Serviceanteil, Nullable<Decimal> tF0601PauschaleZZ, Nullable<Int32> tF0601PauschaleZA, Nullable<Decimal> tF0601KopienAbrZZ, Nullable<Int32> tF0601KopienAbrZA, Nullable<Int32> tF0601Abrechnungsoption, String tF0601AbrPauschaleText, String tF0601AbrKopienText, Nullable<Decimal> tF0601Freikopien, Nullable<Decimal> tF0601Mindestkopien, Decimal tF0601Abrechnungspool, String tF0601SBE, Nullable<DateTime> tF0601Erfassung, String tF0601SBA, Nullable<DateTime> tF0601Aenderung, Nullable<Decimal> tF0601ServiceanteilProz, Nullable<Int32> tF0601Preiseinheit, Decimal tF0601aktiviert, Nullable<Int32> tF0601VertragstypWaehrung, Nullable<Decimal> tF0601ServiceZZ, Guid rowguid, Nullable<Boolean> tF0601Hide, List<Int32> t0602Vertrag_TF0602VertragID)
        {
			this.TF0601VertragstypID = tF0601VertragstypID;
			this.TF0601Vertragstypbezeichnung = tF0601Vertragstypbezeichnung;
			this.TF0601Laufzeit = tF0601Laufzeit;
			this.TF0601Verlaengerung = tF0601Verlaengerung;
			this.TF0601Kuendigungsfrist = tF0601Kuendigungsfrist;
			this.TF0601Vertragspauschale = tF0601Vertragspauschale;
			this.TF0601Serviceanteil = tF0601Serviceanteil;
			this.TF0601PauschaleZZ = tF0601PauschaleZZ;
			this.TF0601PauschaleZA = tF0601PauschaleZA;
			this.TF0601KopienAbrZZ = tF0601KopienAbrZZ;
			this.TF0601KopienAbrZA = tF0601KopienAbrZA;
			this.TF0601Abrechnungsoption = tF0601Abrechnungsoption;
			this.TF0601AbrPauschaleText = tF0601AbrPauschaleText;
			this.TF0601AbrKopienText = tF0601AbrKopienText;
			this.TF0601Freikopien = tF0601Freikopien;
			this.TF0601Mindestkopien = tF0601Mindestkopien;
			this.TF0601Abrechnungspool = tF0601Abrechnungspool;
			this.TF0601SBE = tF0601SBE;
			this.TF0601Erfassung = tF0601Erfassung;
			this.TF0601SBA = tF0601SBA;
			this.TF0601Aenderung = tF0601Aenderung;
			this.TF0601ServiceanteilProz = tF0601ServiceanteilProz;
			this.TF0601Preiseinheit = tF0601Preiseinheit;
			this.TF0601aktiviert = tF0601aktiviert;
			this.TF0601VertragstypWaehrung = tF0601VertragstypWaehrung;
			this.TF0601ServiceZZ = tF0601ServiceZZ;
			this.rowguid = rowguid;
			this.TF0601Hide = tF0601Hide;
			this.T0602Vertrag_TF0602VertragID = t0602Vertrag_TF0602VertragID;
        }
    }
}
