using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class ServiceArtDTO
    {
        [DataMember()]
        public Int32 ServiceArtID { get; set; }

        [DataMember()]
        public String Bezeichnung { get; set; }

        [DataMember()]
        public List<Int32> T0802Prozesskopf_TF0802ProzesskopfID { get; set; }

        public ServiceArtDTO()
        {
        }

        public ServiceArtDTO(Int32 serviceArtID, String bezeichnung, List<Int32> t0802Prozesskopf_TF0802ProzesskopfID)
        {
			this.ServiceArtID = serviceArtID;
			this.Bezeichnung = bezeichnung;
			this.T0802Prozesskopf_TF0802ProzesskopfID = t0802Prozesskopf_TF0802ProzesskopfID;
        }
    }
}
