using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0620AbrechnungskopfDTO
    {
        [DataMember()]
        public Int32 TF0620ProzesskopfID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620VorgangsID { get; set; }

        [DataMember()]
        public String TF0620SBE { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0620Erfassung { get; set; }

        [DataMember()]
        public String TF0620SBA { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0620Aenderung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Prozesspartei1 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620AuftragsNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Zahlungsbedingung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Zahlart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Verkaeufer { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Waehrung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620Objektwert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620Rate { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620Serviceanteil { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Abrechnungsoption { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620APProzesspartei1 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620VertragID { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0620Prozessdatum { get; set; }

        [DataMember()]
        public String TF0620BLZ { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620BankID { get; set; }

        [DataMember()]
        public String TF0620Kontonr { get; set; }

        [DataMember()]
        public String TF0802Kontoinhaber { get; set; }

        [DataMember()]
        public String TF0620ZAText1 { get; set; }

        [DataMember()]
        public String TF0620ZBText1 { get; set; }

        [DataMember()]
        public String TF0620ZBText2 { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620FibuUebertragung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0620UebertragungAm { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertNetto { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertBrutto { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertMwSt { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620APProzesspartei2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620Prozesspartei2 { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620ServiceanteilProz { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0620FibuUebertrNr { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertNettoStdW { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertBruttoStdW { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0620GesamtwertMwStStdW { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0620AbrechnungskopfDTO()
        {
        }

        public T0620AbrechnungskopfDTO(Int32 tF0620ProzesskopfID, Nullable<Int32> tF0620VorgangsID, String tF0620SBE, Nullable<DateTime> tF0620Erfassung, String tF0620SBA, Nullable<DateTime> tF0620Aenderung, Nullable<Int32> tF0620Prozesspartei1, Nullable<Int32> tF0620AuftragsNr, Nullable<Int32> tF0620Zahlungsbedingung, Nullable<Int32> tF0620Zahlart, Nullable<Int32> tF0620Verkaeufer, Nullable<Int32> tF0620Waehrung, Nullable<Decimal> tF0620Objektwert, Nullable<Decimal> tF0620Rate, Nullable<Decimal> tF0620Serviceanteil, Nullable<Int32> tF0620Abrechnungsoption, Nullable<Int32> tF0620APProzesspartei1, Nullable<Int32> tF0620VertragID, Nullable<DateTime> tF0620Prozessdatum, String tF0620BLZ, Nullable<Int32> tF0620BankID, String tF0620Kontonr, String tF0802Kontoinhaber, String tF0620ZAText1, String tF0620ZBText1, String tF0620ZBText2, Nullable<Decimal> tF0620FibuUebertragung, Nullable<DateTime> tF0620UebertragungAm, Nullable<Decimal> tF0620GesamtwertNetto, Nullable<Decimal> tF0620GesamtwertBrutto, Nullable<Decimal> tF0620GesamtwertMwSt, Nullable<Int32> tF0620APProzesspartei2, Nullable<Int32> tF0620Prozesspartei2, Nullable<Decimal> tF0620ServiceanteilProz, Nullable<Int32> tF0620FibuUebertrNr, Nullable<Decimal> tF0620GesamtwertNettoStdW, Nullable<Decimal> tF0620GesamtwertBruttoStdW, Nullable<Decimal> tF0620GesamtwertMwStStdW, Guid rowguid)
        {
			this.TF0620ProzesskopfID = tF0620ProzesskopfID;
			this.TF0620VorgangsID = tF0620VorgangsID;
			this.TF0620SBE = tF0620SBE;
			this.TF0620Erfassung = tF0620Erfassung;
			this.TF0620SBA = tF0620SBA;
			this.TF0620Aenderung = tF0620Aenderung;
			this.TF0620Prozesspartei1 = tF0620Prozesspartei1;
			this.TF0620AuftragsNr = tF0620AuftragsNr;
			this.TF0620Zahlungsbedingung = tF0620Zahlungsbedingung;
			this.TF0620Zahlart = tF0620Zahlart;
			this.TF0620Verkaeufer = tF0620Verkaeufer;
			this.TF0620Waehrung = tF0620Waehrung;
			this.TF0620Objektwert = tF0620Objektwert;
			this.TF0620Rate = tF0620Rate;
			this.TF0620Serviceanteil = tF0620Serviceanteil;
			this.TF0620Abrechnungsoption = tF0620Abrechnungsoption;
			this.TF0620APProzesspartei1 = tF0620APProzesspartei1;
			this.TF0620VertragID = tF0620VertragID;
			this.TF0620Prozessdatum = tF0620Prozessdatum;
			this.TF0620BLZ = tF0620BLZ;
			this.TF0620BankID = tF0620BankID;
			this.TF0620Kontonr = tF0620Kontonr;
			this.TF0802Kontoinhaber = tF0802Kontoinhaber;
			this.TF0620ZAText1 = tF0620ZAText1;
			this.TF0620ZBText1 = tF0620ZBText1;
			this.TF0620ZBText2 = tF0620ZBText2;
			this.TF0620FibuUebertragung = tF0620FibuUebertragung;
			this.TF0620UebertragungAm = tF0620UebertragungAm;
			this.TF0620GesamtwertNetto = tF0620GesamtwertNetto;
			this.TF0620GesamtwertBrutto = tF0620GesamtwertBrutto;
			this.TF0620GesamtwertMwSt = tF0620GesamtwertMwSt;
			this.TF0620APProzesspartei2 = tF0620APProzesspartei2;
			this.TF0620Prozesspartei2 = tF0620Prozesspartei2;
			this.TF0620ServiceanteilProz = tF0620ServiceanteilProz;
			this.TF0620FibuUebertrNr = tF0620FibuUebertrNr;
			this.TF0620GesamtwertNettoStdW = tF0620GesamtwertNettoStdW;
			this.TF0620GesamtwertBruttoStdW = tF0620GesamtwertBruttoStdW;
			this.TF0620GesamtwertMwStStdW = tF0620GesamtwertMwStStdW;
			this.rowguid = rowguid;
        }
    }
}
