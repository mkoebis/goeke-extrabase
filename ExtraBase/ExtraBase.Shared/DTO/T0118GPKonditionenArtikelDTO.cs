using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0118GPKonditionenArtikelDTO
    {
        [DataMember()]
        public Int32 TF0118GPKonditionenArtikeliD { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0118GPKonditionenWgrpID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0118ArtikelID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0118Rabatt { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0118Preis { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0118WaehrungID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0118RabattAktivierung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0118PreisAktivierung { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0118GPKonditionenArtikelDTO()
        {
        }

        public T0118GPKonditionenArtikelDTO(Int32 tF0118GPKonditionenArtikeliD, Nullable<Int32> tF0118GPKonditionenWgrpID, Nullable<Int32> tF0118ArtikelID, Nullable<Decimal> tF0118Rabatt, Nullable<Decimal> tF0118Preis, Nullable<Int32> tF0118WaehrungID, Nullable<Decimal> tF0118RabattAktivierung, Nullable<Decimal> tF0118PreisAktivierung, Guid rowguid)
        {
			this.TF0118GPKonditionenArtikeliD = tF0118GPKonditionenArtikeliD;
			this.TF0118GPKonditionenWgrpID = tF0118GPKonditionenWgrpID;
			this.TF0118ArtikelID = tF0118ArtikelID;
			this.TF0118Rabatt = tF0118Rabatt;
			this.TF0118Preis = tF0118Preis;
			this.TF0118WaehrungID = tF0118WaehrungID;
			this.TF0118RabattAktivierung = tF0118RabattAktivierung;
			this.TF0118PreisAktivierung = tF0118PreisAktivierung;
			this.rowguid = rowguid;
        }
    }
}
