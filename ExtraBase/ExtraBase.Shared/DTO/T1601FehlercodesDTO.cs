using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T1601FehlercodesDTO
    {
        [DataMember()]
        public Int32 TF1601FehlercodesID { get; set; }

        [DataMember()]
        public String TF1601Code { get; set; }

        [DataMember()]
        public String TF1601Beschreibung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF1601Typ { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Boolean> Hide { get; set; }

        public T1601FehlercodesDTO()
        {
        }

        public T1601FehlercodesDTO(Int32 tF1601FehlercodesID, String tF1601Code, String tF1601Beschreibung, Nullable<Int32> tF1601Typ, Guid rowguid, Nullable<Boolean> hide)
        {
			this.TF1601FehlercodesID = tF1601FehlercodesID;
			this.TF1601Code = tF1601Code;
			this.TF1601Beschreibung = tF1601Beschreibung;
			this.TF1601Typ = tF1601Typ;
			this.rowguid = rowguid;
			this.Hide = hide;
        }
    }
}
