using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class ServiceberichtDTO
    {
        [DataMember()]
        public Int32 ServiceberichtID { get; set; }

        [DataMember()]
        public Int32 AuftragsID { get; set; }

        [DataMember()]
        public Int32 BearbeitungStatusID { get; set; }

        [DataMember()]
        public Int32 StatusID { get; set; }

        [DataMember()]
        public Int32 TechnikerID { get; set; }

        [DataMember()]
        public Int32 TechnikereinsatzID { get; set; }

        [DataMember()]
        public DateTime Berichtsdatum { get; set; }

        [DataMember()]
        public DateTime ArbeitszeitBeginn { get; set; }

        [DataMember()]
        public DateTime ArbeitszeitEnde { get; set; }

        [DataMember()]
        public String Bemerkung { get; set; }

        [DataMember()]
        public Boolean Handhabungsfehler { get; set; }

        [DataMember()]
        public Boolean Reinigungsmaterial { get; set; }

        [DataMember()]
        public Boolean VDEPruefung { get; set; }

        [DataMember()]
        public Int32 ZaehlerstandTotalSW { get; set; }

        [DataMember()]
        public Int32 ZaehlerstandTotalFarbe { get; set; }

        [DataMember()]
        public Nullable<Int32> AnsprechpartnerID { get; set; }

        [DataMember()]
        public Nullable<Int32> RechnungsnehmerID { get; set; }

        [DataMember()]
        public Nullable<Int32> MaschineID { get; set; }

        [DataMember()]
        public Nullable<Int32> VertragID { get; set; }

        [DataMember()]
        public String AnsprechpartnerNachname { get; set; }

        [DataMember()]
        public String AnsprechpartnerVorname { get; set; }

        [DataMember()]
        public String AnsprechpartnerVorwahl { get; set; }

        [DataMember()]
        public String AnsprechpartnerTelefon { get; set; }

        [DataMember()]
        public String AnsprechpartnerEMail { get; set; }

        [DataMember()]
        public String StandortBezeichnung { get; set; }

        [DataMember()]
        public String MaschineSeriennummer { get; set; }

        [DataMember()]
        public String MaschineBezeichnung { get; set; }

        [DataMember()]
        public String StandortBeschreibung { get; set; }

        [DataMember()]
        public String AnsprechpartnerAnrede { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumAbgeschlossen { get; set; }

        [DataMember()]
        public Nullable<Int32> StandortID { get; set; }

        [DataMember()]
        public String Memo { get; set; }

        [DataMember()]
        public Byte[] PDF { get; set; }

        [DataMember()]
        public String AnsprechpartnerFax { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsDruckfaehig { get; set; }

        [DataMember()]
        public Nullable<Boolean> Kostenvoranschlag { get; set; }

        public ServiceberichtDTO()
        {
        }

        public ServiceberichtDTO(Int32 serviceberichtID, Int32 auftragsID, Int32 bearbeitungStatusID, Int32 statusID, Int32 technikerID, Int32 technikereinsatzID, DateTime berichtsdatum, DateTime arbeitszeitBeginn, DateTime arbeitszeitEnde, String bemerkung, Boolean handhabungsfehler, Boolean reinigungsmaterial, Boolean vDEPruefung, Int32 zaehlerstandTotalSW, Int32 zaehlerstandTotalFarbe, Nullable<Int32> ansprechpartnerID, Nullable<Int32> rechnungsnehmerID, Nullable<Int32> maschineID, Nullable<Int32> vertragID, String ansprechpartnerNachname, String ansprechpartnerVorname, String ansprechpartnerVorwahl, String ansprechpartnerTelefon, String ansprechpartnerEMail, String standortBezeichnung, String maschineSeriennummer, String maschineBezeichnung, String standortBeschreibung, String ansprechpartnerAnrede, Nullable<DateTime> datumAbgeschlossen, Nullable<Int32> standortID, String memo, Byte[] pDF, String ansprechpartnerFax, Nullable<Boolean> isDruckfaehig, Nullable<Boolean> kostenvoranschlag)
        {
			this.ServiceberichtID = serviceberichtID;
			this.AuftragsID = auftragsID;
			this.BearbeitungStatusID = bearbeitungStatusID;
			this.StatusID = statusID;
			this.TechnikerID = technikerID;
			this.TechnikereinsatzID = technikereinsatzID;
			this.Berichtsdatum = berichtsdatum;
			this.ArbeitszeitBeginn = arbeitszeitBeginn;
			this.ArbeitszeitEnde = arbeitszeitEnde;
			this.Bemerkung = bemerkung;
			this.Handhabungsfehler = handhabungsfehler;
			this.Reinigungsmaterial = reinigungsmaterial;
			this.VDEPruefung = vDEPruefung;
			this.ZaehlerstandTotalSW = zaehlerstandTotalSW;
			this.ZaehlerstandTotalFarbe = zaehlerstandTotalFarbe;
			this.AnsprechpartnerID = ansprechpartnerID;
			this.RechnungsnehmerID = rechnungsnehmerID;
			this.MaschineID = maschineID;
			this.VertragID = vertragID;
			this.AnsprechpartnerNachname = ansprechpartnerNachname;
			this.AnsprechpartnerVorname = ansprechpartnerVorname;
			this.AnsprechpartnerVorwahl = ansprechpartnerVorwahl;
			this.AnsprechpartnerTelefon = ansprechpartnerTelefon;
			this.AnsprechpartnerEMail = ansprechpartnerEMail;
			this.StandortBezeichnung = standortBezeichnung;
			this.MaschineSeriennummer = maschineSeriennummer;
			this.MaschineBezeichnung = maschineBezeichnung;
			this.StandortBeschreibung = standortBeschreibung;
			this.AnsprechpartnerAnrede = ansprechpartnerAnrede;
			this.DatumAbgeschlossen = datumAbgeschlossen;
			this.StandortID = standortID;
			this.Memo = memo;
			this.PDF = pDF;
			this.AnsprechpartnerFax = ansprechpartnerFax;
			this.IsDruckfaehig = isDruckfaehig;
			this.Kostenvoranschlag = kostenvoranschlag;
        }
    }
}
