using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0707BenutzerDTO
    {
        [DataMember()]
        public Int32 TF0707BenutzerID { get; set; }

        [DataMember()]
        public String TF0707Benutzername { get; set; }

        [DataMember()]
        public String TF0707Kennwort { get; set; }

        [DataMember()]
        public Boolean Hide { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumLastLogin { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumLastPWChange { get; set; }

        [DataMember()]
        public String VersionIntraBase { get; set; }

        [DataMember()]
        public String LoginRechner { get; set; }

        [DataMember()]
        public Nullable<Int32> Filiale { get; set; }

        [DataMember()]
        public String VersionTradus { get; set; }

        [DataMember()]
        public String VersionExtraBase { get; set; }

        [DataMember()]
        public String SystemInfo { get; set; }

        [DataMember()]
        public Nullable<Int32> KostenstelleId { get; set; }

        [DataMember()]
        public String Kuerzel { get; set; }

        [DataMember()]
        public String Telefon { get; set; }

        [DataMember()]
        public String Email { get; set; }

        [DataMember()]
        public Int32 T0108Anschrift_TF0108AnschriftID { get; set; }

        [DataMember()]
        public Int32 T0706Gruppe_TF0706GruppeID { get; set; }

        public T0707BenutzerDTO()
        {
        }

        public T0707BenutzerDTO(Int32 tF0707BenutzerID, String tF0707Benutzername, String tF0707Kennwort, Boolean hide, Nullable<DateTime> datumLastLogin, Nullable<DateTime> datumLastPWChange, String versionIntraBase, String loginRechner, Nullable<Int32> filiale, String versionTradus, String versionExtraBase, String systemInfo, Nullable<Int32> kostenstelleId, String kuerzel, String telefon, String email, Int32 t0108Anschrift_TF0108AnschriftID, Int32 t0706Gruppe_TF0706GruppeID)
        {
			this.TF0707BenutzerID = tF0707BenutzerID;
			this.TF0707Benutzername = tF0707Benutzername;
			this.TF0707Kennwort = tF0707Kennwort;
			this.Hide = hide;
			this.DatumLastLogin = datumLastLogin;
			this.DatumLastPWChange = datumLastPWChange;
			this.VersionIntraBase = versionIntraBase;
			this.LoginRechner = loginRechner;
			this.Filiale = filiale;
			this.VersionTradus = versionTradus;
			this.VersionExtraBase = versionExtraBase;
			this.SystemInfo = systemInfo;
			this.KostenstelleId = kostenstelleId;
			this.Kuerzel = kuerzel;
			this.Telefon = telefon;
			this.Email = email;
			this.T0108Anschrift_TF0108AnschriftID = t0108Anschrift_TF0108AnschriftID;
			this.T0706Gruppe_TF0706GruppeID = t0706Gruppe_TF0706GruppeID;
        }
    }
}
