using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0301LagerDTO
    {
        [DataMember()]
        public Int32 TF0301LagerID { get; set; }

        [DataMember()]
        public String TF0301Lagerbezeichnung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0301AnschriftID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0301AnsprechpartnerID { get; set; }

        [DataMember()]
        public Decimal TF0301Vertriebslager { get; set; }

        [DataMember()]
        public Boolean TF0301An_Auslieferungslager { get; set; }

        [DataMember()]
        public Int32 TF0301Lagerart { get; set; }

        [DataMember()]
        public String TF0301SachbearbeiterErfassung { get; set; }

        [DataMember()]
        public String TF0301Erfassungsdatum { get; set; }

        [DataMember()]
        public String TF0301SachbearbeiterAenderung { get; set; }

        [DataMember()]
        public String TF0301Aenderungsdatum { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public List<Int32> T0302Lagerort_TF0302LagerortID { get; set; }

        public T0301LagerDTO()
        {
        }

        public T0301LagerDTO(Int32 tF0301LagerID, String tF0301Lagerbezeichnung, Nullable<Int32> tF0301AnschriftID, Nullable<Int32> tF0301AnsprechpartnerID, Decimal tF0301Vertriebslager, Boolean tF0301An_Auslieferungslager, Int32 tF0301Lagerart, String tF0301SachbearbeiterErfassung, String tF0301Erfassungsdatum, String tF0301SachbearbeiterAenderung, String tF0301Aenderungsdatum, Guid rowguid, List<Int32> t0302Lagerort_TF0302LagerortID)
        {
			this.TF0301LagerID = tF0301LagerID;
			this.TF0301Lagerbezeichnung = tF0301Lagerbezeichnung;
			this.TF0301AnschriftID = tF0301AnschriftID;
			this.TF0301AnsprechpartnerID = tF0301AnsprechpartnerID;
			this.TF0301Vertriebslager = tF0301Vertriebslager;
			this.TF0301An_Auslieferungslager = tF0301An_Auslieferungslager;
			this.TF0301Lagerart = tF0301Lagerart;
			this.TF0301SachbearbeiterErfassung = tF0301SachbearbeiterErfassung;
			this.TF0301Erfassungsdatum = tF0301Erfassungsdatum;
			this.TF0301SachbearbeiterAenderung = tF0301SachbearbeiterAenderung;
			this.TF0301Aenderungsdatum = tF0301Aenderungsdatum;
			this.rowguid = rowguid;
			this.T0302Lagerort_TF0302LagerortID = t0302Lagerort_TF0302LagerortID;
        }
    }
}
