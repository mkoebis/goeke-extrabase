using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0706GruppeDTO
    {
        [DataMember()]
        public Int32 TF0706GruppeID { get; set; }

        [DataMember()]
        public String TF0706Gruppenname { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public List<Int32> T0707Benutzer_TF0707BenutzerID { get; set; }

        public T0706GruppeDTO()
        {
        }

        public T0706GruppeDTO(Int32 tF0706GruppeID, String tF0706Gruppenname, Guid rowguid, List<Int32> t0707Benutzer_TF0707BenutzerID)
        {
			this.TF0706GruppeID = tF0706GruppeID;
			this.TF0706Gruppenname = tF0706Gruppenname;
			this.rowguid = rowguid;
			this.T0707Benutzer_TF0707BenutzerID = t0707Benutzer_TF0707BenutzerID;
        }
    }
}
