using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0108AnschriftDTO
    {
        [DataMember()]
        public Int32 TF0108AnschriftID { get; set; }

        [DataMember()]
        public String TF0108Kundennummer { get; set; }

        [DataMember()]
        public DateTime TF0108Aenderungsdatum { get; set; }

        [DataMember()]
        public String TF0108PLZ { get; set; }

        [DataMember()]
        public String TF0108Ort { get; set; }

        [DataMember()]
        public String TF0108Homepage { get; set; }

        [DataMember()]
        public String TF0108Email { get; set; }

        [DataMember()]
        public Int32 TF0108LieferartID { get; set; }

        [DataMember()]
        public Int32 TF0108ZahlartID { get; set; }

        [DataMember()]
        public Int32 TF0108ZahlungsbedingungID { get; set; }

        [DataMember()]
        public DateTime TF0108Erfassungsdatum { get; set; }

        [DataMember()]
        public String TF0108Strasse_Hnr { get; set; }

        [DataMember()]
        public Decimal TF0108Hauptanschrift { get; set; }

        [DataMember()]
        public String TF0108PLZ_Postfach { get; set; }

        [DataMember()]
        public String TF0108Postfach { get; set; }

        [DataMember()]
        public Int32 TF0108GPStandardwaehrung { get; set; }

        [DataMember()]
        public Int32 TF0108LandID { get; set; }

        [DataMember()]
        public String TF0108Telefon { get; set; }

        [DataMember()]
        public String TF0108Vorwahl { get; set; }

        [DataMember()]
        public String TF0108Fax { get; set; }

        [DataMember()]
        public String TF0108Aenderungspersonal { get; set; }

        [DataMember()]
        public String TF0108Aenderungsgrund { get; set; }

        [DataMember()]
        public String TF0108Matchcode { get; set; }

        [DataMember()]
        public Decimal TF0108Eigenkennzeichen { get; set; }

        [DataMember()]
        public String TF0108Anschriftsinhaber1 { get; set; }

        [DataMember()]
        public String TF0108Anschriftsinhaber2 { get; set; }

        [DataMember()]
        public String TF0108Anschriftsinhaber3 { get; set; }

        [DataMember()]
        public Int32 TF0108Standardrechnungsnehmer { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0108Geburtsdatum_Gruendung { get; set; }

        [DataMember()]
        public Decimal TF0108Privat_Geschaeftskunde { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108Standardvertriebler { get; set; }

        [DataMember()]
        public Decimal TF0108OPAnschrift { get; set; }

        [DataMember()]
        public String TF0108Anrede { get; set; }

        [DataMember()]
        public String TF0108Briefanrede { get; set; }

        [DataMember()]
        public String TF0108Titel { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108BrancheID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108ZoneID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108VerkaufsgebietID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0108km { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0108Werbung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108PersonIDAlt { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Int16 TF0108KundenKategorie { get; set; }

        [DataMember()]
        public String TF0108Mobil { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108Standardvertriebler2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0108Standardvertriebler3 { get; set; }

        [DataMember()]
        public Boolean TF0108Hide { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Events { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Informationen { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_IT { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Schulungen { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_VBM { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Angebote { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Messen { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_Kundenzeitung { get; set; }

        [DataMember()]
        public Boolean TF0108VIPKunde { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_BVB { get; set; }

        [DataMember()]
        public Boolean TF0108VfM_SkiParty { get; set; }

        [DataMember()]
        public String ServiceEmail { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumExportDebitor { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumExportKreditor { get; set; }

        [DataMember()]
        public String SachbearbeiterErfassung { get; set; }

        [DataMember()]
        public String Region { get; set; }

        [DataMember()]
        public Nullable<Boolean> KeineEdvWerbung { get; set; }

        [DataMember()]
        public String VorwahlMobil { get; set; }

        [DataMember()]
        public String VorwahlFax { get; set; }

        [DataMember()]
        public Nullable<Boolean> SuppressCanonUmfrage { get; set; }

        [DataMember()]
        public Nullable<Boolean> AnfahrtArtikel { get; set; }

        [DataMember()]
        public Nullable<Int32> Status { get; set; }

        [DataMember()]
        public Nullable<Boolean> SuppressServiceMailAp { get; set; }

        [DataMember()]
        public List<Int32> T0120Ansprechpartner_TF0120AnsprechpartnerID { get; set; }

        [DataMember()]
        public List<Int32> T0143Techniker_TF0143TechnikerID { get; set; }

        [DataMember()]
        public List<Int32> T0707Benutzer_TF0707BenutzerID { get; set; }

        public T0108AnschriftDTO()
        {
        }

        public T0108AnschriftDTO(Int32 tF0108AnschriftID, String tF0108Kundennummer, DateTime tF0108Aenderungsdatum, String tF0108PLZ, String tF0108Ort, String tF0108Homepage, String tF0108Email, Int32 tF0108LieferartID, Int32 tF0108ZahlartID, Int32 tF0108ZahlungsbedingungID, DateTime tF0108Erfassungsdatum, String tF0108Strasse_Hnr, Decimal tF0108Hauptanschrift, String tF0108PLZ_Postfach, String tF0108Postfach, Int32 tF0108GPStandardwaehrung, Int32 tF0108LandID, String tF0108Telefon, String tF0108Vorwahl, String tF0108Fax, String tF0108Aenderungspersonal, String tF0108Aenderungsgrund, String tF0108Matchcode, Decimal tF0108Eigenkennzeichen, String tF0108Anschriftsinhaber1, String tF0108Anschriftsinhaber2, String tF0108Anschriftsinhaber3, Int32 tF0108Standardrechnungsnehmer, Nullable<DateTime> tF0108Geburtsdatum_Gruendung, Decimal tF0108Privat_Geschaeftskunde, Nullable<Int32> tF0108Standardvertriebler, Decimal tF0108OPAnschrift, String tF0108Anrede, String tF0108Briefanrede, String tF0108Titel, Nullable<Int32> tF0108BrancheID, Nullable<Int32> tF0108ZoneID, Nullable<Int32> tF0108VerkaufsgebietID, Nullable<Decimal> tF0108km, Nullable<Decimal> tF0108Werbung, Nullable<Int32> tF0108PersonIDAlt, Guid rowguid, Int16 tF0108KundenKategorie, String tF0108Mobil, Nullable<Int32> tF0108Standardvertriebler2, Nullable<Int32> tF0108Standardvertriebler3, Boolean tF0108Hide, Boolean tF0108VfM_Events, Boolean tF0108VfM_Informationen, Boolean tF0108VfM_IT, Boolean tF0108VfM_Schulungen, Boolean tF0108VfM_VBM, Boolean tF0108VfM_Angebote, Boolean tF0108VfM_Messen, Boolean tF0108VfM_Kundenzeitung, Boolean tF0108VIPKunde, Boolean tF0108VfM_BVB, Boolean tF0108VfM_SkiParty, String serviceEmail, Nullable<DateTime> datumExportDebitor, Nullable<DateTime> datumExportKreditor, String sachbearbeiterErfassung, String region, Nullable<Boolean> keineEdvWerbung, String vorwahlMobil, String vorwahlFax, Nullable<Boolean> suppressCanonUmfrage, Nullable<Boolean> anfahrtArtikel, Nullable<Int32> status, Nullable<Boolean> suppressServiceMailAp, List<Int32> t0120Ansprechpartner_TF0120AnsprechpartnerID, List<Int32> t0143Techniker_TF0143TechnikerID, List<Int32> t0707Benutzer_TF0707BenutzerID)
        {
			this.TF0108AnschriftID = tF0108AnschriftID;
			this.TF0108Kundennummer = tF0108Kundennummer;
			this.TF0108Aenderungsdatum = tF0108Aenderungsdatum;
			this.TF0108PLZ = tF0108PLZ;
			this.TF0108Ort = tF0108Ort;
			this.TF0108Homepage = tF0108Homepage;
			this.TF0108Email = tF0108Email;
			this.TF0108LieferartID = tF0108LieferartID;
			this.TF0108ZahlartID = tF0108ZahlartID;
			this.TF0108ZahlungsbedingungID = tF0108ZahlungsbedingungID;
			this.TF0108Erfassungsdatum = tF0108Erfassungsdatum;
			this.TF0108Strasse_Hnr = tF0108Strasse_Hnr;
			this.TF0108Hauptanschrift = tF0108Hauptanschrift;
			this.TF0108PLZ_Postfach = tF0108PLZ_Postfach;
			this.TF0108Postfach = tF0108Postfach;
			this.TF0108GPStandardwaehrung = tF0108GPStandardwaehrung;
			this.TF0108LandID = tF0108LandID;
			this.TF0108Telefon = tF0108Telefon;
			this.TF0108Vorwahl = tF0108Vorwahl;
			this.TF0108Fax = tF0108Fax;
			this.TF0108Aenderungspersonal = tF0108Aenderungspersonal;
			this.TF0108Aenderungsgrund = tF0108Aenderungsgrund;
			this.TF0108Matchcode = tF0108Matchcode;
			this.TF0108Eigenkennzeichen = tF0108Eigenkennzeichen;
			this.TF0108Anschriftsinhaber1 = tF0108Anschriftsinhaber1;
			this.TF0108Anschriftsinhaber2 = tF0108Anschriftsinhaber2;
			this.TF0108Anschriftsinhaber3 = tF0108Anschriftsinhaber3;
			this.TF0108Standardrechnungsnehmer = tF0108Standardrechnungsnehmer;
			this.TF0108Geburtsdatum_Gruendung = tF0108Geburtsdatum_Gruendung;
			this.TF0108Privat_Geschaeftskunde = tF0108Privat_Geschaeftskunde;
			this.TF0108Standardvertriebler = tF0108Standardvertriebler;
			this.TF0108OPAnschrift = tF0108OPAnschrift;
			this.TF0108Anrede = tF0108Anrede;
			this.TF0108Briefanrede = tF0108Briefanrede;
			this.TF0108Titel = tF0108Titel;
			this.TF0108BrancheID = tF0108BrancheID;
			this.TF0108ZoneID = tF0108ZoneID;
			this.TF0108VerkaufsgebietID = tF0108VerkaufsgebietID;
			this.TF0108km = tF0108km;
			this.TF0108Werbung = tF0108Werbung;
			this.TF0108PersonIDAlt = tF0108PersonIDAlt;
			this.rowguid = rowguid;
			this.TF0108KundenKategorie = tF0108KundenKategorie;
			this.TF0108Mobil = tF0108Mobil;
			this.TF0108Standardvertriebler2 = tF0108Standardvertriebler2;
			this.TF0108Standardvertriebler3 = tF0108Standardvertriebler3;
			this.TF0108Hide = tF0108Hide;
			this.TF0108VfM_Events = tF0108VfM_Events;
			this.TF0108VfM_Informationen = tF0108VfM_Informationen;
			this.TF0108VfM_IT = tF0108VfM_IT;
			this.TF0108VfM_Schulungen = tF0108VfM_Schulungen;
			this.TF0108VfM_VBM = tF0108VfM_VBM;
			this.TF0108VfM_Angebote = tF0108VfM_Angebote;
			this.TF0108VfM_Messen = tF0108VfM_Messen;
			this.TF0108VfM_Kundenzeitung = tF0108VfM_Kundenzeitung;
			this.TF0108VIPKunde = tF0108VIPKunde;
			this.TF0108VfM_BVB = tF0108VfM_BVB;
			this.TF0108VfM_SkiParty = tF0108VfM_SkiParty;
			this.ServiceEmail = serviceEmail;
			this.DatumExportDebitor = datumExportDebitor;
			this.DatumExportKreditor = datumExportKreditor;
			this.SachbearbeiterErfassung = sachbearbeiterErfassung;
			this.Region = region;
			this.KeineEdvWerbung = keineEdvWerbung;
			this.VorwahlMobil = vorwahlMobil;
			this.VorwahlFax = vorwahlFax;
			this.SuppressCanonUmfrage = suppressCanonUmfrage;
			this.AnfahrtArtikel = anfahrtArtikel;
			this.Status = status;
			this.SuppressServiceMailAp = suppressServiceMailAp;
			this.T0120Ansprechpartner_TF0120AnsprechpartnerID = t0120Ansprechpartner_TF0120AnsprechpartnerID;
			this.T0143Techniker_TF0143TechnikerID = t0143Techniker_TF0143TechnikerID;
			this.T0707Benutzer_TF0707BenutzerID = t0707Benutzer_TF0707BenutzerID;
        }
    }
}
