using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0802ProzesskopfDTO
    {
        [DataMember()]
        public Int32 TF0802ProzesskopfID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802VorgangsID { get; set; }

        [DataMember()]
        public Nullable<Int16> TF0802Prozessart { get; set; }

        [DataMember()]
        public String TF0802SBE { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0802Erfassung { get; set; }

        [DataMember()]
        public String TF0802SBA { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0802Aenderung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Prozesspartei1 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Prozesspartei2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Prozesspartei3 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802LagerID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Pakete { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Palette { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Gewicht { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Lieferart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802AuftragsNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Zahlungsbedingung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Zahlart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Verkaeufer { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Waehrung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Objektwert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Rate { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Serviceanteil { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Faktor { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802ServiceanteilProz { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Abrechnungsoption { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802APProzesspartei1 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802APProzesspartei2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802APProzesspartei3 { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Sammelrechnung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Gesamtsumme { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Teilrechnung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Teillieferung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Verkauf { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Miete { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Leasing { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Ratenkredit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Service { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0802Prozessdatum { get; set; }

        [DataMember()]
        public String TF0802BLZ { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802BankID { get; set; }

        [DataMember()]
        public String TF0802Kontonr { get; set; }

        [DataMember()]
        public String TF0802Kontoinhaber { get; set; }

        [DataMember()]
        public String TF0802ZAText1 { get; set; }

        [DataMember()]
        public String TF0802ZBText1 { get; set; }

        [DataMember()]
        public String TF0802ZBText2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802VorgaengerID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802FibuUebertragung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0802UebertragungAm { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertNetto { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertBrutto { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertMwSt { get; set; }

        [DataMember()]
        public String TF0802FreierText1 { get; set; }

        [DataMember()]
        public String TF0802FreierText2 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802AngebotID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802MaschineID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Prioritaet { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802FibuUebertrNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0802Sachbearbeiter { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0802Ausdruck { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802Auftragsfreigabe { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertbruttoStdW { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertNettoStdW { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0802GesamtwertMwStStdW { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0143TechnikerID { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Int32> Lieferstatus { get; set; }

        [DataMember()]
        public String ExterneTicketId { get; set; }

        [DataMember()]
        public Nullable<Boolean> DruckeLieferetikett { get; set; }

        public T0802ProzesskopfDTO()
        {
        }

        public T0802ProzesskopfDTO(Int32 tF0802ProzesskopfID, Nullable<Int32> tF0802VorgangsID, Nullable<Int16> tF0802Prozessart, String tF0802SBE, Nullable<DateTime> tF0802Erfassung, String tF0802SBA, Nullable<DateTime> tF0802Aenderung, Nullable<Int32> tF0802Prozesspartei1, Nullable<Int32> tF0802Prozesspartei2, Nullable<Int32> tF0802Prozesspartei3, Nullable<Int32> tF0802LagerID, Nullable<Decimal> tF0802Pakete, Nullable<Decimal> tF0802Palette, Nullable<Decimal> tF0802Gewicht, Nullable<Int32> tF0802Lieferart, Nullable<Int32> tF0802AuftragsNr, Nullable<Int32> tF0802Zahlungsbedingung, Nullable<Int32> tF0802Zahlart, Nullable<Int32> tF0802Verkaeufer, Nullable<Int32> tF0802Waehrung, Nullable<Decimal> tF0802Objektwert, Nullable<Decimal> tF0802Rate, Nullable<Decimal> tF0802Serviceanteil, Nullable<Decimal> tF0802Faktor, Nullable<Decimal> tF0802ServiceanteilProz, Nullable<Int32> tF0802Abrechnungsoption, Nullable<Int32> tF0802APProzesspartei1, Nullable<Int32> tF0802APProzesspartei2, Nullable<Int32> tF0802APProzesspartei3, Nullable<Decimal> tF0802Sammelrechnung, Nullable<Decimal> tF0802Gesamtsumme, Nullable<Decimal> tF0802Teilrechnung, Nullable<Decimal> tF0802Teillieferung, Nullable<Decimal> tF0802Verkauf, Nullable<Decimal> tF0802Miete, Nullable<Decimal> tF0802Leasing, Nullable<Decimal> tF0802Ratenkredit, Nullable<Decimal> tF0802Service, Nullable<DateTime> tF0802Prozessdatum, String tF0802BLZ, Nullable<Int32> tF0802BankID, String tF0802Kontonr, String tF0802Kontoinhaber, String tF0802ZAText1, String tF0802ZBText1, String tF0802ZBText2, Nullable<Int32> tF0802VorgaengerID, Nullable<Decimal> tF0802FibuUebertragung, Nullable<DateTime> tF0802UebertragungAm, Nullable<Decimal> tF0802GesamtwertNetto, Nullable<Decimal> tF0802GesamtwertBrutto, Nullable<Decimal> tF0802GesamtwertMwSt, String tF0802FreierText1, String tF0802FreierText2, Nullable<Int32> tF0802AngebotID, Nullable<Int32> tF0802MaschineID, Nullable<Int32> tF0802Prioritaet, Nullable<Int32> tF0802FibuUebertrNr, Nullable<Int32> tF0802Sachbearbeiter, Nullable<DateTime> tF0802Ausdruck, Nullable<Decimal> tF0802Auftragsfreigabe, Nullable<Decimal> tF0802GesamtwertbruttoStdW, Nullable<Decimal> tF0802GesamtwertNettoStdW, Nullable<Decimal> tF0802GesamtwertMwStStdW, Nullable<Int32> tF0143TechnikerID, Guid rowguid, Nullable<Int32> lieferstatus, String externeTicketId, Nullable<Boolean> druckeLieferetikett)
        {
			this.TF0802ProzesskopfID = tF0802ProzesskopfID;
			this.TF0802VorgangsID = tF0802VorgangsID;
			this.TF0802Prozessart = tF0802Prozessart;
			this.TF0802SBE = tF0802SBE;
			this.TF0802Erfassung = tF0802Erfassung;
			this.TF0802SBA = tF0802SBA;
			this.TF0802Aenderung = tF0802Aenderung;
			this.TF0802Prozesspartei1 = tF0802Prozesspartei1;
			this.TF0802Prozesspartei2 = tF0802Prozesspartei2;
			this.TF0802Prozesspartei3 = tF0802Prozesspartei3;
			this.TF0802LagerID = tF0802LagerID;
			this.TF0802Pakete = tF0802Pakete;
			this.TF0802Palette = tF0802Palette;
			this.TF0802Gewicht = tF0802Gewicht;
			this.TF0802Lieferart = tF0802Lieferart;
			this.TF0802AuftragsNr = tF0802AuftragsNr;
			this.TF0802Zahlungsbedingung = tF0802Zahlungsbedingung;
			this.TF0802Zahlart = tF0802Zahlart;
			this.TF0802Verkaeufer = tF0802Verkaeufer;
			this.TF0802Waehrung = tF0802Waehrung;
			this.TF0802Objektwert = tF0802Objektwert;
			this.TF0802Rate = tF0802Rate;
			this.TF0802Serviceanteil = tF0802Serviceanteil;
			this.TF0802Faktor = tF0802Faktor;
			this.TF0802ServiceanteilProz = tF0802ServiceanteilProz;
			this.TF0802Abrechnungsoption = tF0802Abrechnungsoption;
			this.TF0802APProzesspartei1 = tF0802APProzesspartei1;
			this.TF0802APProzesspartei2 = tF0802APProzesspartei2;
			this.TF0802APProzesspartei3 = tF0802APProzesspartei3;
			this.TF0802Sammelrechnung = tF0802Sammelrechnung;
			this.TF0802Gesamtsumme = tF0802Gesamtsumme;
			this.TF0802Teilrechnung = tF0802Teilrechnung;
			this.TF0802Teillieferung = tF0802Teillieferung;
			this.TF0802Verkauf = tF0802Verkauf;
			this.TF0802Miete = tF0802Miete;
			this.TF0802Leasing = tF0802Leasing;
			this.TF0802Ratenkredit = tF0802Ratenkredit;
			this.TF0802Service = tF0802Service;
			this.TF0802Prozessdatum = tF0802Prozessdatum;
			this.TF0802BLZ = tF0802BLZ;
			this.TF0802BankID = tF0802BankID;
			this.TF0802Kontonr = tF0802Kontonr;
			this.TF0802Kontoinhaber = tF0802Kontoinhaber;
			this.TF0802ZAText1 = tF0802ZAText1;
			this.TF0802ZBText1 = tF0802ZBText1;
			this.TF0802ZBText2 = tF0802ZBText2;
			this.TF0802VorgaengerID = tF0802VorgaengerID;
			this.TF0802FibuUebertragung = tF0802FibuUebertragung;
			this.TF0802UebertragungAm = tF0802UebertragungAm;
			this.TF0802GesamtwertNetto = tF0802GesamtwertNetto;
			this.TF0802GesamtwertBrutto = tF0802GesamtwertBrutto;
			this.TF0802GesamtwertMwSt = tF0802GesamtwertMwSt;
			this.TF0802FreierText1 = tF0802FreierText1;
			this.TF0802FreierText2 = tF0802FreierText2;
			this.TF0802AngebotID = tF0802AngebotID;
			this.TF0802MaschineID = tF0802MaschineID;
			this.TF0802Prioritaet = tF0802Prioritaet;
			this.TF0802FibuUebertrNr = tF0802FibuUebertrNr;
			this.TF0802Sachbearbeiter = tF0802Sachbearbeiter;
			this.TF0802Ausdruck = tF0802Ausdruck;
			this.TF0802Auftragsfreigabe = tF0802Auftragsfreigabe;
			this.TF0802GesamtwertbruttoStdW = tF0802GesamtwertbruttoStdW;
			this.TF0802GesamtwertNettoStdW = tF0802GesamtwertNettoStdW;
			this.TF0802GesamtwertMwStStdW = tF0802GesamtwertMwStStdW;
			this.TF0143TechnikerID = tF0143TechnikerID;
			this.rowguid = rowguid;
			this.Lieferstatus = lieferstatus;
			this.ExterneTicketId = externeTicketId;
			this.DruckeLieferetikett = druckeLieferetikett;
        }
    }
}
