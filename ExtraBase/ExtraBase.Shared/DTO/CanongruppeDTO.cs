using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class CanongruppeDTO
    {
        [DataMember()]
        public Int32 CanongruppeId { get; set; }

        [DataMember()]
        public String Bezeichnung { get; set; }

        [DataMember()]
        public List<Int32> T0401Angebot_TF0401AngebotID { get; set; }

        public CanongruppeDTO()
        {
        }

        public CanongruppeDTO(Int32 canongruppeId, String bezeichnung, List<Int32> t0401Angebot_TF0401AngebotID)
        {
			this.CanongruppeId = canongruppeId;
			this.Bezeichnung = bezeichnung;
			this.T0401Angebot_TF0401AngebotID = t0401Angebot_TF0401AngebotID;
        }
    }
}
