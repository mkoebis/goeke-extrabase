using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0602VertragDTO
    {
        [DataMember()]
        public Int32 TF0602VertragID { get; set; }

        [DataMember()]
        public String TF0602VertragsNrRefiBank { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602Vertragsbeginn { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602Vertragsende { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Laufzeit { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Verlaengerung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602Kuendigungsdatum { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Kuendigungsfrist { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602VertragsnehmerID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Rechnungsnehmer { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Vertragsart { get; set; }

        [DataMember()]
        public Decimal TF0602Abrechnungspool { get; set; }

        [DataMember()]
        public String TF0602AbrPauschaleText { get; set; }

        [DataMember()]
        public String TF0602AbrPauschaleTextMiete { get; set; }

        [DataMember()]
        public String TF0602AbrPauschaleTextService { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602Abrechnungsoption { get; set; }

        [DataMember()]
        public Decimal TF0602RefiVertrag { get; set; }

        [DataMember()]
        public String TF0602SBE { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602Erfassung { get; set; }

        [DataMember()]
        public String TF0602SBA { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602Aenderung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602Serviceanteil { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602ServiceanteilProz { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602Vertragspauschale { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602VertragsWaehrung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602SeZahlart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602SeZahlungsbedingungen { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602PaZahlart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602PaZahlungsbedingungen { get; set; }

        [DataMember()]
        public String TF0602alteVertragsNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602ProzessKopfID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602VertragsgeberID { get; set; }

        [DataMember()]
        public Decimal TF0602Endabgerechnet { get; set; }

        [DataMember()]
        public String TF0602BLZ { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602BankID { get; set; }

        [DataMember()]
        public String TF0602Kontonr { get; set; }

        [DataMember()]
        public String TF0602Kontoinhaber { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602PaKostenstelle { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602SeKostenstelle { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602KlickKostenstelle { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602RefiZahlzyklus { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602RefiZyklusstart { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602MwSt { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602KlickErloeskonto { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602SeErloeskonto { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0602PaErloeskonto { get; set; }

        [DataMember()]
        public String TF0602BesondereServicebedingung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0602Vertragserhoehung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0602VertragserhoehungDatum { get; set; }

        [DataMember()]
        public Nullable<Decimal> Restwert { get; set; }

        [DataMember()]
        public String RestwertVereinbarung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TonerWert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TonerGegenWert { get; set; }

        [DataMember()]
        public Nullable<DateTime> TonerWertDatum { get; set; }

        [DataMember()]
        public Int32 T0601Vertragstyp_TF0601VertragstypID { get; set; }

        [DataMember()]
        public List<Int32> T0602Vertrag1_TF0602VertragID { get; set; }

        [DataMember()]
        public Int32 T0602Vertrag2_TF0602VertragID { get; set; }

        public T0602VertragDTO()
        {
        }

        public T0602VertragDTO(Int32 tF0602VertragID, String tF0602VertragsNrRefiBank, Nullable<DateTime> tF0602Vertragsbeginn, Nullable<DateTime> tF0602Vertragsende, Nullable<Int32> tF0602Laufzeit, Nullable<Int32> tF0602Verlaengerung, Nullable<DateTime> tF0602Kuendigungsdatum, Nullable<Int32> tF0602Kuendigungsfrist, Nullable<Int32> tF0602VertragsnehmerID, Nullable<Int32> tF0602Rechnungsnehmer, Nullable<Int32> tF0602Vertragsart, Decimal tF0602Abrechnungspool, String tF0602AbrPauschaleText, String tF0602AbrPauschaleTextMiete, String tF0602AbrPauschaleTextService, Nullable<Int32> tF0602Abrechnungsoption, Decimal tF0602RefiVertrag, String tF0602SBE, Nullable<DateTime> tF0602Erfassung, String tF0602SBA, Nullable<DateTime> tF0602Aenderung, Nullable<Decimal> tF0602Serviceanteil, Nullable<Decimal> tF0602ServiceanteilProz, Nullable<Decimal> tF0602Vertragspauschale, Nullable<Int32> tF0602VertragsWaehrung, Nullable<Int32> tF0602SeZahlart, Nullable<Int32> tF0602SeZahlungsbedingungen, Nullable<Int32> tF0602PaZahlart, Nullable<Int32> tF0602PaZahlungsbedingungen, String tF0602alteVertragsNr, Nullable<Int32> tF0602ProzessKopfID, Nullable<Int32> tF0602VertragsgeberID, Decimal tF0602Endabgerechnet, String tF0602BLZ, Nullable<Int32> tF0602BankID, String tF0602Kontonr, String tF0602Kontoinhaber, Nullable<Int32> tF0602PaKostenstelle, Nullable<Int32> tF0602SeKostenstelle, Nullable<Int32> tF0602KlickKostenstelle, Nullable<Decimal> tF0602RefiZahlzyklus, Nullable<DateTime> tF0602RefiZyklusstart, Nullable<Decimal> tF0602MwSt, Nullable<Int32> tF0602KlickErloeskonto, Nullable<Int32> tF0602SeErloeskonto, Nullable<Int32> tF0602PaErloeskonto, String tF0602BesondereServicebedingung, Nullable<Decimal> tF0602Vertragserhoehung, Nullable<DateTime> tF0602VertragserhoehungDatum, Nullable<Decimal> restwert, String restwertVereinbarung, Nullable<Decimal> tonerWert, Nullable<Decimal> tonerGegenWert, Nullable<DateTime> tonerWertDatum, Int32 t0601Vertragstyp_TF0601VertragstypID, List<Int32> t0602Vertrag1_TF0602VertragID, Int32 t0602Vertrag2_TF0602VertragID)
        {
			this.TF0602VertragID = tF0602VertragID;
			this.TF0602VertragsNrRefiBank = tF0602VertragsNrRefiBank;
			this.TF0602Vertragsbeginn = tF0602Vertragsbeginn;
			this.TF0602Vertragsende = tF0602Vertragsende;
			this.TF0602Laufzeit = tF0602Laufzeit;
			this.TF0602Verlaengerung = tF0602Verlaengerung;
			this.TF0602Kuendigungsdatum = tF0602Kuendigungsdatum;
			this.TF0602Kuendigungsfrist = tF0602Kuendigungsfrist;
			this.TF0602VertragsnehmerID = tF0602VertragsnehmerID;
			this.TF0602Rechnungsnehmer = tF0602Rechnungsnehmer;
			this.TF0602Vertragsart = tF0602Vertragsart;
			this.TF0602Abrechnungspool = tF0602Abrechnungspool;
			this.TF0602AbrPauschaleText = tF0602AbrPauschaleText;
			this.TF0602AbrPauschaleTextMiete = tF0602AbrPauschaleTextMiete;
			this.TF0602AbrPauschaleTextService = tF0602AbrPauschaleTextService;
			this.TF0602Abrechnungsoption = tF0602Abrechnungsoption;
			this.TF0602RefiVertrag = tF0602RefiVertrag;
			this.TF0602SBE = tF0602SBE;
			this.TF0602Erfassung = tF0602Erfassung;
			this.TF0602SBA = tF0602SBA;
			this.TF0602Aenderung = tF0602Aenderung;
			this.TF0602Serviceanteil = tF0602Serviceanteil;
			this.TF0602ServiceanteilProz = tF0602ServiceanteilProz;
			this.TF0602Vertragspauschale = tF0602Vertragspauschale;
			this.TF0602VertragsWaehrung = tF0602VertragsWaehrung;
			this.TF0602SeZahlart = tF0602SeZahlart;
			this.TF0602SeZahlungsbedingungen = tF0602SeZahlungsbedingungen;
			this.TF0602PaZahlart = tF0602PaZahlart;
			this.TF0602PaZahlungsbedingungen = tF0602PaZahlungsbedingungen;
			this.TF0602alteVertragsNr = tF0602alteVertragsNr;
			this.TF0602ProzessKopfID = tF0602ProzessKopfID;
			this.TF0602VertragsgeberID = tF0602VertragsgeberID;
			this.TF0602Endabgerechnet = tF0602Endabgerechnet;
			this.TF0602BLZ = tF0602BLZ;
			this.TF0602BankID = tF0602BankID;
			this.TF0602Kontonr = tF0602Kontonr;
			this.TF0602Kontoinhaber = tF0602Kontoinhaber;
			this.TF0602PaKostenstelle = tF0602PaKostenstelle;
			this.TF0602SeKostenstelle = tF0602SeKostenstelle;
			this.TF0602KlickKostenstelle = tF0602KlickKostenstelle;
			this.TF0602RefiZahlzyklus = tF0602RefiZahlzyklus;
			this.TF0602RefiZyklusstart = tF0602RefiZyklusstart;
			this.TF0602MwSt = tF0602MwSt;
			this.TF0602KlickErloeskonto = tF0602KlickErloeskonto;
			this.TF0602SeErloeskonto = tF0602SeErloeskonto;
			this.TF0602PaErloeskonto = tF0602PaErloeskonto;
			this.TF0602BesondereServicebedingung = tF0602BesondereServicebedingung;
			this.TF0602Vertragserhoehung = tF0602Vertragserhoehung;
			this.TF0602VertragserhoehungDatum = tF0602VertragserhoehungDatum;
			this.Restwert = restwert;
			this.RestwertVereinbarung = restwertVereinbarung;
			this.TonerWert = tonerWert;
			this.TonerGegenWert = tonerGegenWert;
			this.TonerWertDatum = tonerWertDatum;
			this.T0601Vertragstyp_TF0601VertragstypID = t0601Vertragstyp_TF0601VertragstypID;
			this.T0602Vertrag1_TF0602VertragID = t0602Vertrag1_TF0602VertragID;
			this.T0602Vertrag2_TF0602VertragID = t0602Vertrag2_TF0602VertragID;
        }
    }
}
