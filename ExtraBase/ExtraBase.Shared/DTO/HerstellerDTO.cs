using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class HerstellerDTO
    {
        [DataMember()]
        public Int32 HerstellerId { get; set; }

        [DataMember()]
        public String Bezeichnung { get; set; }

        [DataMember()]
        public List<Int32> T0401Angebot_TF0401AngebotID { get; set; }

        public HerstellerDTO()
        {
        }

        public HerstellerDTO(Int32 herstellerId, String bezeichnung, List<Int32> t0401Angebot_TF0401AngebotID)
        {
			this.HerstellerId = herstellerId;
			this.Bezeichnung = bezeichnung;
			this.T0401Angebot_TF0401AngebotID = t0401Angebot_TF0401AngebotID;
        }
    }
}
