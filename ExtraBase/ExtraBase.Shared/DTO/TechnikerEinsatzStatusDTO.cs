using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class TechnikerEinsatzStatusDTO
    {
        [DataMember()]
        public Int32 TechnikerEinsatzStatusID { get; set; }

        [DataMember()]
        public String Bezeichnung { get; set; }

        [DataMember()]
        public List<Int32> T0810TechnikerEinsatz_TF0810TechnikerEinsatzID { get; set; }

        public TechnikerEinsatzStatusDTO()
        {
        }

        public TechnikerEinsatzStatusDTO(Int32 technikerEinsatzStatusID, String bezeichnung, List<Int32> t0810TechnikerEinsatz_TF0810TechnikerEinsatzID)
        {
			this.TechnikerEinsatzStatusID = technikerEinsatzStatusID;
			this.Bezeichnung = bezeichnung;
			this.T0810TechnikerEinsatz_TF0810TechnikerEinsatzID = t0810TechnikerEinsatz_TF0810TechnikerEinsatzID;
        }
    }
}
