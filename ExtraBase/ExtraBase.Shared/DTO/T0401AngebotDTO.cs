using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0401AngebotDTO
    {
        [DataMember()]
        public Int32 TF0401AngebotID { get; set; }

        [DataMember()]
        public String TF0401Angebotsbezeichnung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401Angebotsgruppe { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401Warengruppe { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401Warenkonto { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401Erloeskonto { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401Verrechnungseinheit { get; set; }

        [DataMember()]
        public String TF0401Mengenkennung { get; set; }

        [DataMember()]
        public Int32 TF0401MwSt { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401BasispreisDM { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401BasispreisEuro { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401AktionspreisDM { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401AktionspreisEuro { get; set; }

        [DataMember()]
        public String TF0401AktionVon { get; set; }

        [DataMember()]
        public String TF0401AktionBis { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401HaendlerpreisDM { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401HaendlerpreisEuro { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401HaendlerAktionspreisDM { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401HaendlerAktionspreisEuro { get; set; }

        [DataMember()]
        public String TF0401HaendlerAktionVon { get; set; }

        [DataMember()]
        public String TF0401HaendlerAktionBis { get; set; }

        [DataMember()]
        public String TF0401letzteAenderung { get; set; }

        [DataMember()]
        public String TF0401Aenderungsperson { get; set; }

        [DataMember()]
        public String TF0401Aenderungsgrund { get; set; }

        [DataMember()]
        public String TF0401Erfassungsdatum { get; set; }

        [DataMember()]
        public Boolean TF0401Lagerfuehrung { get; set; }

        [DataMember()]
        public Boolean TF0401SperreBestellwesen { get; set; }

        [DataMember()]
        public Boolean TF0401SperreAuftragswesen { get; set; }

        [DataMember()]
        public Boolean TF0401Teilbarkeit { get; set; }

        [DataMember()]
        public Boolean TF0401Incentive { get; set; }

        [DataMember()]
        public Boolean TF0401Seriennummer { get; set; }

        [DataMember()]
        public Boolean TF0401Durchschnittskalkulation { get; set; }

        [DataMember()]
        public Boolean TF0401Maschinentyp { get; set; }

        [DataMember()]
        public Boolean TF0401FiFo { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401Laufzeit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0431Wartungsintervall { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0431WartungstypID { get; set; }

        [DataMember()]
        public String TF0401aktLieferantenArtikelNr { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401Frachtartikel { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0401Frachtartikel1 { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0401LetzteAuftragsPos { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Boolean Hide { get; set; }

        [DataMember()]
        public String Merkmale { get; set; }

        [DataMember()]
        public Byte[] Thumbnail { get; set; }

        [DataMember()]
        public Nullable<Int32> Buchungsschluessel { get; set; }

        [DataMember()]
        public Nullable<Int32> Kostenkonto { get; set; }

        [DataMember()]
        public String PathServiceManual { get; set; }

        [DataMember()]
        public String PathPartsCatalog { get; set; }

        [DataMember()]
        public Nullable<Int32> ArbeitseinheitId { get; set; }

        [DataMember()]
        public Int32 Canongruppe_CanongruppeId { get; set; }

        [DataMember()]
        public Int32 Hersteller_HerstellerId { get; set; }

        [DataMember()]
        public List<Int32> T0430Maschinenstamm_TF0430MaschinenstammID { get; set; }

        [DataMember()]
        public List<Int32> T0431ZaehlwerkTyp_TF0431ZaehlwerktypID { get; set; }

        public T0401AngebotDTO()
        {
        }

        public T0401AngebotDTO(Int32 tF0401AngebotID, String tF0401Angebotsbezeichnung, Nullable<Int32> tF0401Angebotsgruppe, Nullable<Int32> tF0401Warengruppe, Nullable<Int32> tF0401Warenkonto, Nullable<Int32> tF0401Erloeskonto, Nullable<Decimal> tF0401Verrechnungseinheit, String tF0401Mengenkennung, Int32 tF0401MwSt, Nullable<Decimal> tF0401BasispreisDM, Nullable<Decimal> tF0401BasispreisEuro, Nullable<Decimal> tF0401AktionspreisDM, Nullable<Decimal> tF0401AktionspreisEuro, String tF0401AktionVon, String tF0401AktionBis, Nullable<Decimal> tF0401HaendlerpreisDM, Nullable<Decimal> tF0401HaendlerpreisEuro, Nullable<Decimal> tF0401HaendlerAktionspreisDM, Nullable<Decimal> tF0401HaendlerAktionspreisEuro, String tF0401HaendlerAktionVon, String tF0401HaendlerAktionBis, String tF0401letzteAenderung, String tF0401Aenderungsperson, String tF0401Aenderungsgrund, String tF0401Erfassungsdatum, Boolean tF0401Lagerfuehrung, Boolean tF0401SperreBestellwesen, Boolean tF0401SperreAuftragswesen, Boolean tF0401Teilbarkeit, Boolean tF0401Incentive, Boolean tF0401Seriennummer, Boolean tF0401Durchschnittskalkulation, Boolean tF0401Maschinentyp, Boolean tF0401FiFo, Nullable<Int32> tF0401Laufzeit, Nullable<Decimal> tF0431Wartungsintervall, Nullable<Int32> tF0431WartungstypID, String tF0401aktLieferantenArtikelNr, Nullable<Decimal> tF0401Frachtartikel, Nullable<Decimal> tF0401Frachtartikel1, Nullable<Int32> tF0401LetzteAuftragsPos, Guid rowguid, Boolean hide, String merkmale, Byte[] thumbnail, Nullable<Int32> buchungsschluessel, Nullable<Int32> kostenkonto, String pathServiceManual, String pathPartsCatalog, Nullable<Int32> arbeitseinheitId, Int32 canongruppe_CanongruppeId, Int32 hersteller_HerstellerId, List<Int32> t0430Maschinenstamm_TF0430MaschinenstammID, List<Int32> t0431ZaehlwerkTyp_TF0431ZaehlwerktypID)
        {
			this.TF0401AngebotID = tF0401AngebotID;
			this.TF0401Angebotsbezeichnung = tF0401Angebotsbezeichnung;
			this.TF0401Angebotsgruppe = tF0401Angebotsgruppe;
			this.TF0401Warengruppe = tF0401Warengruppe;
			this.TF0401Warenkonto = tF0401Warenkonto;
			this.TF0401Erloeskonto = tF0401Erloeskonto;
			this.TF0401Verrechnungseinheit = tF0401Verrechnungseinheit;
			this.TF0401Mengenkennung = tF0401Mengenkennung;
			this.TF0401MwSt = tF0401MwSt;
			this.TF0401BasispreisDM = tF0401BasispreisDM;
			this.TF0401BasispreisEuro = tF0401BasispreisEuro;
			this.TF0401AktionspreisDM = tF0401AktionspreisDM;
			this.TF0401AktionspreisEuro = tF0401AktionspreisEuro;
			this.TF0401AktionVon = tF0401AktionVon;
			this.TF0401AktionBis = tF0401AktionBis;
			this.TF0401HaendlerpreisDM = tF0401HaendlerpreisDM;
			this.TF0401HaendlerpreisEuro = tF0401HaendlerpreisEuro;
			this.TF0401HaendlerAktionspreisDM = tF0401HaendlerAktionspreisDM;
			this.TF0401HaendlerAktionspreisEuro = tF0401HaendlerAktionspreisEuro;
			this.TF0401HaendlerAktionVon = tF0401HaendlerAktionVon;
			this.TF0401HaendlerAktionBis = tF0401HaendlerAktionBis;
			this.TF0401letzteAenderung = tF0401letzteAenderung;
			this.TF0401Aenderungsperson = tF0401Aenderungsperson;
			this.TF0401Aenderungsgrund = tF0401Aenderungsgrund;
			this.TF0401Erfassungsdatum = tF0401Erfassungsdatum;
			this.TF0401Lagerfuehrung = tF0401Lagerfuehrung;
			this.TF0401SperreBestellwesen = tF0401SperreBestellwesen;
			this.TF0401SperreAuftragswesen = tF0401SperreAuftragswesen;
			this.TF0401Teilbarkeit = tF0401Teilbarkeit;
			this.TF0401Incentive = tF0401Incentive;
			this.TF0401Seriennummer = tF0401Seriennummer;
			this.TF0401Durchschnittskalkulation = tF0401Durchschnittskalkulation;
			this.TF0401Maschinentyp = tF0401Maschinentyp;
			this.TF0401FiFo = tF0401FiFo;
			this.TF0401Laufzeit = tF0401Laufzeit;
			this.TF0431Wartungsintervall = tF0431Wartungsintervall;
			this.TF0431WartungstypID = tF0431WartungstypID;
			this.TF0401aktLieferantenArtikelNr = tF0401aktLieferantenArtikelNr;
			this.TF0401Frachtartikel = tF0401Frachtartikel;
			this.TF0401Frachtartikel1 = tF0401Frachtartikel1;
			this.TF0401LetzteAuftragsPos = tF0401LetzteAuftragsPos;
			this.rowguid = rowguid;
			this.Hide = hide;
			this.Merkmale = merkmale;
			this.Thumbnail = thumbnail;
			this.Buchungsschluessel = buchungsschluessel;
			this.Kostenkonto = kostenkonto;
			this.PathServiceManual = pathServiceManual;
			this.PathPartsCatalog = pathPartsCatalog;
			this.ArbeitseinheitId = arbeitseinheitId;
			this.Canongruppe_CanongruppeId = canongruppe_CanongruppeId;
			this.Hersteller_HerstellerId = hersteller_HerstellerId;
			this.T0430Maschinenstamm_TF0430MaschinenstammID = t0430Maschinenstamm_TF0430MaschinenstammID;
			this.T0431ZaehlwerkTyp_TF0431ZaehlwerktypID = t0431ZaehlwerkTyp_TF0431ZaehlwerktypID;
        }
    }
}
