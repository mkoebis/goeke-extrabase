using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0504LagerortbestandDTO
    {
        [DataMember()]
        public Int32 TF0504LagerortbestandID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0504Bestandsmenge { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0504ArtikelID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0504Lagerbestandswert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0504Mindestbestand { get; set; }

        [DataMember()]
        public Int32 T0302Lagerort_TF0302LagerortID { get; set; }

        public T0504LagerortbestandDTO()
        {
        }

        public T0504LagerortbestandDTO(Int32 tF0504LagerortbestandID, Nullable<Decimal> tF0504Bestandsmenge, Nullable<Int32> tF0504ArtikelID, Nullable<Decimal> tF0504Lagerbestandswert, Nullable<Decimal> tF0504Mindestbestand, Int32 t0302Lagerort_TF0302LagerortID)
        {
			this.TF0504LagerortbestandID = tF0504LagerortbestandID;
			this.TF0504Bestandsmenge = tF0504Bestandsmenge;
			this.TF0504ArtikelID = tF0504ArtikelID;
			this.TF0504Lagerbestandswert = tF0504Lagerbestandswert;
			this.TF0504Mindestbestand = tF0504Mindestbestand;
			this.T0302Lagerort_TF0302LagerortID = t0302Lagerort_TF0302LagerortID;
        }
    }
}
