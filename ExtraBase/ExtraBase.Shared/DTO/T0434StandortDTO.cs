using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0434StandortDTO
    {
        [DataMember()]
        public Int32 TF0434StandortID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0434AnschriftID { get; set; }

        [DataMember()]
        public String TF0434Standortbezeichnung { get; set; }

        [DataMember()]
        public String TF0434StandortBeschreibung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0434AnsprechpartnerID { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0434StandortDTO()
        {
        }

        public T0434StandortDTO(Int32 tF0434StandortID, Nullable<Int32> tF0434AnschriftID, String tF0434Standortbezeichnung, String tF0434StandortBeschreibung, Nullable<Int32> tF0434AnsprechpartnerID, Guid rowguid)
        {
			this.TF0434StandortID = tF0434StandortID;
			this.TF0434AnschriftID = tF0434AnschriftID;
			this.TF0434Standortbezeichnung = tF0434Standortbezeichnung;
			this.TF0434StandortBeschreibung = tF0434StandortBeschreibung;
			this.TF0434AnsprechpartnerID = tF0434AnsprechpartnerID;
			this.rowguid = rowguid;
        }
    }
}
