using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0110PersonDTO
    {
        [DataMember()]
        public Int32 TF0110PersonID { get; set; }

        [DataMember()]
        public String TF0110Familienstand { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0110BerufID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0110Toechter { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0110Soehne { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0110PersonDTO()
        {
        }

        public T0110PersonDTO(Int32 tF0110PersonID, String tF0110Familienstand, Nullable<Int32> tF0110BerufID, Nullable<Int32> tF0110Toechter, Nullable<Int32> tF0110Soehne, Guid rowguid)
        {
			this.TF0110PersonID = tF0110PersonID;
			this.TF0110Familienstand = tF0110Familienstand;
			this.TF0110BerufID = tF0110BerufID;
			this.TF0110Toechter = tF0110Toechter;
			this.TF0110Soehne = tF0110Soehne;
			this.rowguid = rowguid;
        }
    }
}
