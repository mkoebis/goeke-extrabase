using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0803ProzesspositionDTO
    {
        [DataMember()]
        public Int32 TF0803ProzessPosID { get; set; }

        [DataMember()]
        public String TF0803ProzessPosNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803ArtikelLeistungID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803KostenstelleID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Menge { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803HauptprozessID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Einzelpreis { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Rabattprozent { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Bonus { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Gesamtpreis { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803MaschineID { get; set; }

        [DataMember()]
        public String TF0803FreierText1 { get; set; }

        [DataMember()]
        public String TF0803FreierText2 { get; set; }

        [DataMember()]
        public String TF0803FreierText3 { get; set; }

        [DataMember()]
        public String TF0803FreierText4 { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Bestellmenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Teillieferung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0803Lieferdatum { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Gewaehrleistung { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Gewaehrleistungseinheit { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803LagerID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803TechnikerID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803ExternerEinsatz { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0803Startzeit { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0803Endzeit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Festpreis { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Positionsart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803LagerortID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803AbgeschlosseneMenge { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803AuftragsPosID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803StornoMenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Leerzeit { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Objektwert { get; set; }

        [DataMember()]
        public String TF0803GB { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Lieferart { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803StandortID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803MwSt { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Garantie { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Garantieeinheit { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803WaehrungID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Erloeskonto { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Abholartikel { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803LieferartikelID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803offeneWEMenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Auftragsstorno { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803APLieferanschrift { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Lieferanschrift { get; set; }

        [DataMember()]
        public String TF0803LieferantenArtikelNr { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0803Vertrag { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803WEMenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803KommiMenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803WAMenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803WAWert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Rechnungsmenge { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Rechnungswert { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803Einzelkosten { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0803Garantiedatum { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803EinzelpreisStdW { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0803GesamtpreisStdW { get; set; }

        [DataMember()]
        public String TF0803Waehrungsbezeichnung { get; set; }

        [DataMember()]
        public Nullable<DateTime> DatumExportDatev { get; set; }

        public T0803ProzesspositionDTO()
        {
        }

        public T0803ProzesspositionDTO(Int32 tF0803ProzessPosID, String tF0803ProzessPosNr, Nullable<Int32> tF0803ArtikelLeistungID, Nullable<Int32> tF0803KostenstelleID, Nullable<Decimal> tF0803Menge, Nullable<Int32> tF0803HauptprozessID, Nullable<Decimal> tF0803Einzelpreis, Nullable<Decimal> tF0803Rabattprozent, Nullable<Decimal> tF0803Bonus, Nullable<Decimal> tF0803Gesamtpreis, Nullable<Int32> tF0803MaschineID, String tF0803FreierText1, String tF0803FreierText2, String tF0803FreierText3, String tF0803FreierText4, Nullable<Decimal> tF0803Bestellmenge, Nullable<Decimal> tF0803Teillieferung, Nullable<DateTime> tF0803Lieferdatum, Nullable<Decimal> tF0803Gewaehrleistung, Nullable<Int32> tF0803Gewaehrleistungseinheit, Nullable<Int32> tF0803LagerID, Nullable<Int32> tF0803TechnikerID, Nullable<Decimal> tF0803ExternerEinsatz, Nullable<DateTime> tF0803Startzeit, Nullable<DateTime> tF0803Endzeit, Nullable<Decimal> tF0803Festpreis, Nullable<Int32> tF0803Positionsart, Nullable<Int32> tF0803LagerortID, Nullable<Decimal> tF0803AbgeschlosseneMenge, Nullable<Int32> tF0803AuftragsPosID, Nullable<Decimal> tF0803StornoMenge, Nullable<Decimal> tF0803Leerzeit, Nullable<Decimal> tF0803Objektwert, String tF0803GB, Nullable<Int32> tF0803Lieferart, Nullable<Int32> tF0803StandortID, Nullable<Decimal> tF0803MwSt, Nullable<Decimal> tF0803Garantie, Nullable<Int32> tF0803Garantieeinheit, Nullable<Int32> tF0803WaehrungID, Nullable<Decimal> tF0803Erloeskonto, Nullable<Int32> tF0803Abholartikel, Nullable<Int32> tF0803LieferartikelID, Nullable<Decimal> tF0803offeneWEMenge, Nullable<Decimal> tF0803Auftragsstorno, Nullable<Int32> tF0803APLieferanschrift, Nullable<Int32> tF0803Lieferanschrift, String tF0803LieferantenArtikelNr, Nullable<Int32> tF0803Vertrag, Nullable<Decimal> tF0803WEMenge, Nullable<Decimal> tF0803KommiMenge, Nullable<Decimal> tF0803WAMenge, Nullable<Decimal> tF0803WAWert, Nullable<Decimal> tF0803Rechnungsmenge, Nullable<Decimal> tF0803Rechnungswert, Nullable<Decimal> tF0803Einzelkosten, Nullable<DateTime> tF0803Garantiedatum, Nullable<Decimal> tF0803EinzelpreisStdW, Nullable<Decimal> tF0803GesamtpreisStdW, String tF0803Waehrungsbezeichnung, Nullable<DateTime> datumExportDatev)
        {
			this.TF0803ProzessPosID = tF0803ProzessPosID;
			this.TF0803ProzessPosNr = tF0803ProzessPosNr;
			this.TF0803ArtikelLeistungID = tF0803ArtikelLeistungID;
			this.TF0803KostenstelleID = tF0803KostenstelleID;
			this.TF0803Menge = tF0803Menge;
			this.TF0803HauptprozessID = tF0803HauptprozessID;
			this.TF0803Einzelpreis = tF0803Einzelpreis;
			this.TF0803Rabattprozent = tF0803Rabattprozent;
			this.TF0803Bonus = tF0803Bonus;
			this.TF0803Gesamtpreis = tF0803Gesamtpreis;
			this.TF0803MaschineID = tF0803MaschineID;
			this.TF0803FreierText1 = tF0803FreierText1;
			this.TF0803FreierText2 = tF0803FreierText2;
			this.TF0803FreierText3 = tF0803FreierText3;
			this.TF0803FreierText4 = tF0803FreierText4;
			this.TF0803Bestellmenge = tF0803Bestellmenge;
			this.TF0803Teillieferung = tF0803Teillieferung;
			this.TF0803Lieferdatum = tF0803Lieferdatum;
			this.TF0803Gewaehrleistung = tF0803Gewaehrleistung;
			this.TF0803Gewaehrleistungseinheit = tF0803Gewaehrleistungseinheit;
			this.TF0803LagerID = tF0803LagerID;
			this.TF0803TechnikerID = tF0803TechnikerID;
			this.TF0803ExternerEinsatz = tF0803ExternerEinsatz;
			this.TF0803Startzeit = tF0803Startzeit;
			this.TF0803Endzeit = tF0803Endzeit;
			this.TF0803Festpreis = tF0803Festpreis;
			this.TF0803Positionsart = tF0803Positionsart;
			this.TF0803LagerortID = tF0803LagerortID;
			this.TF0803AbgeschlosseneMenge = tF0803AbgeschlosseneMenge;
			this.TF0803AuftragsPosID = tF0803AuftragsPosID;
			this.TF0803StornoMenge = tF0803StornoMenge;
			this.TF0803Leerzeit = tF0803Leerzeit;
			this.TF0803Objektwert = tF0803Objektwert;
			this.TF0803GB = tF0803GB;
			this.TF0803Lieferart = tF0803Lieferart;
			this.TF0803StandortID = tF0803StandortID;
			this.TF0803MwSt = tF0803MwSt;
			this.TF0803Garantie = tF0803Garantie;
			this.TF0803Garantieeinheit = tF0803Garantieeinheit;
			this.TF0803WaehrungID = tF0803WaehrungID;
			this.TF0803Erloeskonto = tF0803Erloeskonto;
			this.TF0803Abholartikel = tF0803Abholartikel;
			this.TF0803LieferartikelID = tF0803LieferartikelID;
			this.TF0803offeneWEMenge = tF0803offeneWEMenge;
			this.TF0803Auftragsstorno = tF0803Auftragsstorno;
			this.TF0803APLieferanschrift = tF0803APLieferanschrift;
			this.TF0803Lieferanschrift = tF0803Lieferanschrift;
			this.TF0803LieferantenArtikelNr = tF0803LieferantenArtikelNr;
			this.TF0803Vertrag = tF0803Vertrag;
			this.TF0803WEMenge = tF0803WEMenge;
			this.TF0803KommiMenge = tF0803KommiMenge;
			this.TF0803WAMenge = tF0803WAMenge;
			this.TF0803WAWert = tF0803WAWert;
			this.TF0803Rechnungsmenge = tF0803Rechnungsmenge;
			this.TF0803Rechnungswert = tF0803Rechnungswert;
			this.TF0803Einzelkosten = tF0803Einzelkosten;
			this.TF0803Garantiedatum = tF0803Garantiedatum;
			this.TF0803EinzelpreisStdW = tF0803EinzelpreisStdW;
			this.TF0803GesamtpreisStdW = tF0803GesamtpreisStdW;
			this.TF0803Waehrungsbezeichnung = tF0803Waehrungsbezeichnung;
			this.DatumExportDatev = datumExportDatev;
        }
    }
}
