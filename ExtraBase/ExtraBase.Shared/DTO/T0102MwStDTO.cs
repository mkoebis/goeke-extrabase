using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0102MwStDTO
    {
        [DataMember()]
        public Int32 TF0102MwStID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0102MwSt { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0102MwStKonto { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0102REFVorsteuerID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0102REFUmsatzsteuerID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0102ArtikelStandardMwSt { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Boolean> Hide { get; set; }

        public T0102MwStDTO()
        {
        }

        public T0102MwStDTO(Int32 tF0102MwStID, Nullable<Decimal> tF0102MwSt, Nullable<Int32> tF0102MwStKonto, Nullable<Int32> tF0102REFVorsteuerID, Nullable<Int32> tF0102REFUmsatzsteuerID, Nullable<Decimal> tF0102ArtikelStandardMwSt, Guid rowguid, Nullable<Boolean> hide)
        {
			this.TF0102MwStID = tF0102MwStID;
			this.TF0102MwSt = tF0102MwSt;
			this.TF0102MwStKonto = tF0102MwStKonto;
			this.TF0102REFVorsteuerID = tF0102REFVorsteuerID;
			this.TF0102REFUmsatzsteuerID = tF0102REFUmsatzsteuerID;
			this.TF0102ArtikelStandardMwSt = tF0102ArtikelStandardMwSt;
			this.rowguid = rowguid;
			this.Hide = hide;
        }
    }
}
