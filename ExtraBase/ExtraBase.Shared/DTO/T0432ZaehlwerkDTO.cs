using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0432ZaehlwerkDTO
    {
        [DataMember()]
        public Int32 TF0432ZaehlwerkID { get; set; }

        [DataMember()]
        public String TF0432Zaehlwerkbezeichnung { get; set; }

        [DataMember()]
        public Decimal TF0432Faktor { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0432Einbau { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0432Ausbau { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0432Endabgerechnet { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0432VerteilungFreikopien { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0432DSKopienAnzahl { get; set; }

        [DataMember()]
        public Nullable<Boolean> RealVorhanden { get; set; }

        [DataMember()]
        public Nullable<Int32> DIN { get; set; }

        [DataMember()]
        public Nullable<Int32> Funktion { get; set; }

        [DataMember()]
        public Int32 T0430Maschinenstamm_TF0430MaschinenstammID { get; set; }

        [DataMember()]
        public Int32 T0431ZaehlwerkTyp_TF0431ZaehlwerktypID { get; set; }

        public T0432ZaehlwerkDTO()
        {
        }

        public T0432ZaehlwerkDTO(Int32 tF0432ZaehlwerkID, String tF0432Zaehlwerkbezeichnung, Decimal tF0432Faktor, Nullable<DateTime> tF0432Einbau, Nullable<DateTime> tF0432Ausbau, Nullable<Decimal> tF0432Endabgerechnet, Nullable<Decimal> tF0432VerteilungFreikopien, Nullable<Decimal> tF0432DSKopienAnzahl, Nullable<Boolean> realVorhanden, Nullable<Int32> dIN, Nullable<Int32> funktion, Int32 t0430Maschinenstamm_TF0430MaschinenstammID, Int32 t0431ZaehlwerkTyp_TF0431ZaehlwerktypID)
        {
			this.TF0432ZaehlwerkID = tF0432ZaehlwerkID;
			this.TF0432Zaehlwerkbezeichnung = tF0432Zaehlwerkbezeichnung;
			this.TF0432Faktor = tF0432Faktor;
			this.TF0432Einbau = tF0432Einbau;
			this.TF0432Ausbau = tF0432Ausbau;
			this.TF0432Endabgerechnet = tF0432Endabgerechnet;
			this.TF0432VerteilungFreikopien = tF0432VerteilungFreikopien;
			this.TF0432DSKopienAnzahl = tF0432DSKopienAnzahl;
			this.RealVorhanden = realVorhanden;
			this.DIN = dIN;
			this.Funktion = funktion;
			this.T0430Maschinenstamm_TF0430MaschinenstammID = t0430Maschinenstamm_TF0430MaschinenstammID;
			this.T0431ZaehlwerkTyp_TF0431ZaehlwerktypID = t0431ZaehlwerkTyp_TF0431ZaehlwerktypID;
        }
    }
}
