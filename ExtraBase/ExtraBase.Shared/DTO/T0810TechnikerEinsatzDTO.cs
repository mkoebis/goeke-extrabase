using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0810TechnikerEinsatzDTO
    {
        [DataMember()]
        public Int32 TF0810TechnikerEinsatzID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0810ProzessKopfID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0810Materialabhollung { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0810Startdatum { get; set; }

        [DataMember()]
        public Nullable<DateTime> TF0810Enddatum { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0810Folgeeinsatz { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0810TechnikerID { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0810Prioritaet { get; set; }

        [DataMember()]
        public Nullable<Boolean> IsAbfrage { get; set; }

        [DataMember()]
        public Int32 TechnikerEinsatzStatus_TechnikerEinsatzStatusID { get; set; }

        public T0810TechnikerEinsatzDTO()
        {
        }

        public T0810TechnikerEinsatzDTO(Int32 tF0810TechnikerEinsatzID, Nullable<Int32> tF0810ProzessKopfID, Nullable<Decimal> tF0810Materialabhollung, Nullable<DateTime> tF0810Startdatum, Nullable<DateTime> tF0810Enddatum, Nullable<Decimal> tF0810Folgeeinsatz, Nullable<Int32> tF0810TechnikerID, Guid rowguid, Nullable<Int32> tF0810Prioritaet, Nullable<Boolean> isAbfrage, Int32 technikerEinsatzStatus_TechnikerEinsatzStatusID)
        {
			this.TF0810TechnikerEinsatzID = tF0810TechnikerEinsatzID;
			this.TF0810ProzessKopfID = tF0810ProzessKopfID;
			this.TF0810Materialabhollung = tF0810Materialabhollung;
			this.TF0810Startdatum = tF0810Startdatum;
			this.TF0810Enddatum = tF0810Enddatum;
			this.TF0810Folgeeinsatz = tF0810Folgeeinsatz;
			this.TF0810TechnikerID = tF0810TechnikerID;
			this.rowguid = rowguid;
			this.TF0810Prioritaet = tF0810Prioritaet;
			this.IsAbfrage = isAbfrage;
			this.TechnikerEinsatzStatus_TechnikerEinsatzStatusID = technikerEinsatzStatus_TechnikerEinsatzStatusID;
        }
    }
}
