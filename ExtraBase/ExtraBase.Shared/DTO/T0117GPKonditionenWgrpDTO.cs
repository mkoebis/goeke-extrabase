using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ExtraBase.Shared.DTO
{
    [DataContract()]
    public partial class T0117GPKonditionenWgrpDTO
    {
        [DataMember()]
        public Int32 TF0117GPKonditionenWgrpID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0117AnschriftID { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0117WarengruppeID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0117Rabatt { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0117Preis { get; set; }

        [DataMember()]
        public Nullable<Int32> TF0117WaehrungID { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0117RabattAktivierung { get; set; }

        [DataMember()]
        public Nullable<Decimal> TF0117PreisAktivierung { get; set; }

        [DataMember()]
        public Guid rowguid { get; set; }

        public T0117GPKonditionenWgrpDTO()
        {
        }

        public T0117GPKonditionenWgrpDTO(Int32 tF0117GPKonditionenWgrpID, Nullable<Int32> tF0117AnschriftID, Nullable<Int32> tF0117WarengruppeID, Nullable<Decimal> tF0117Rabatt, Nullable<Decimal> tF0117Preis, Nullable<Int32> tF0117WaehrungID, Nullable<Decimal> tF0117RabattAktivierung, Nullable<Decimal> tF0117PreisAktivierung, Guid rowguid)
        {
			this.TF0117GPKonditionenWgrpID = tF0117GPKonditionenWgrpID;
			this.TF0117AnschriftID = tF0117AnschriftID;
			this.TF0117WarengruppeID = tF0117WarengruppeID;
			this.TF0117Rabatt = tF0117Rabatt;
			this.TF0117Preis = tF0117Preis;
			this.TF0117WaehrungID = tF0117WaehrungID;
			this.TF0117RabattAktivierung = tF0117RabattAktivierung;
			this.TF0117PreisAktivierung = tF0117PreisAktivierung;
			this.rowguid = rowguid;
        }
    }
}
