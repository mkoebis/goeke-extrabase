﻿using System;
using System.Collections.Generic;
using ExtraBase.Shared.DTO;
using ExtraBase.Shared.Interface;
using ExtraBase.Business;
using System.ServiceModel;
using System.Security.Permissions;

namespace ExtraBase
{  
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single,ConcurrencyMode=ConcurrencyMode.Single)]
    public class ExtraBaseService:IExtraBaseService
    {
        #region Canongruppe      
        public bool AddCanongruppe(Shared.DTO.CanongruppeDTO canongruppe)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddCanongruppe(canongruppe);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.CanongruppeDTO GetCanongruppeById(int canongruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetCanongruppeById(canongruppeId);
            }
            catch
            {
                throw;
            }

        }

        public List<Shared.DTO.CanongruppeDTO> GetCanongruppen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetCanongruppen();
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateCanongruppe(Shared.DTO.CanongruppeDTO canongruppe, int canongruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateCanongruppeById(canongruppe, canongruppeId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteCanongruppe(int canongruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteCanongruppeById(canongruppeId);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Hersteller       
        public bool AddHersteller(Shared.DTO.HerstellerDTO hersteller)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddHersteller(hersteller);
            }
            catch
            {
                throw;
            }

        }
 
        public Shared.DTO.HerstellerDTO GetHerstellerById(int herstellerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetHerstellerById(herstellerId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateHerstellerById(Shared.DTO.HerstellerDTO hersteller, int herstellerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateHerstellerById(hersteller, herstellerId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteHerstellerById(int herstellerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteHerstellerById(herstellerId);
            }
            catch
            {
                throw;
            }
        }

        public List<HerstellerDTO> GetHersteller()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetHersteller();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Serviceart       
        public bool AddServiceArt(Shared.DTO.ServiceArtDTO serviceart)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddServiceArt(serviceart);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.ServiceArtDTO GetServiceArtById(int serviceartId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceArtById(serviceartId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceArtById(Shared.DTO.ServiceArtDTO serviceart, int serviceartId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateServiceArtById(serviceart, serviceartId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteServiceArtById(int serviceartId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteServiceArtById(serviceartId);
            }
            catch
            {
                throw;
            }

        }
        
        public List<ServiceArtDTO> GetServiceArten()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceArten();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Maschinenstamm       
        public bool AddT0430Maschinenstamm(Shared.DTO.T0430MaschinenstammDTO Maschinenstamm)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0430Maschinenstamm(Maschinenstamm);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0430MaschinenstammDTO GetT0430MaschinenstammById(int MaschinenstammId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0430MaschinenstammById(MaschinenstammId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0430MaschinenstammById(Shared.DTO.T0430MaschinenstammDTO Maschinenstamm, int MaschinenstammId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateMaschinenstammById(Maschinenstamm, MaschinenstammId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0430MaschinenstammById(int MaschinenstammId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0430MaschinenstammById(MaschinenstammId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0430MaschinenstammDTO> GetT0430Maschinenstamm()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0430Maschinenstamm();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Servicebericht       
        public bool AddServicebericht(Shared.DTO.ServiceberichtDTO serviceBericht)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddServicebericht(serviceBericht);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.ServiceberichtDTO GetServiceberichtById(int serviceberichtId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceberichtById(serviceberichtId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceberichtById(Shared.DTO.ServiceberichtDTO serviceBericht, int serviceberichtId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateServiceberichtById(serviceBericht, serviceberichtId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteServiceberichtById(int serviceberichtId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteServiceberichtById(serviceberichtId);
            }
            catch
            {
                throw;
            }
        }

        public List<ServiceberichtDTO> GetServiceberichte()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceberichte();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Techniker      
        public bool AddT0143Techniker(Shared.DTO.T0143TechnikerDTO T0143Techniker)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0143Techniker(T0143Techniker);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0143TechnikerDTO GetT0143TechnikerById(int T0143TechnikerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0143TechnikerById(T0143TechnikerId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0143TechnikerById(Shared.DTO.T0143TechnikerDTO T0143Techniker, int T0143TechnikerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0143TechnikerById(T0143Techniker, T0143TechnikerId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0143TechnikerById(int T0143TechnikerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0143TechnikerById(T0143TechnikerId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0143TechnikerDTO> GetT0143Techniker()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0143Techniker();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region MwSt        
        public bool AddT0102MwSt(Shared.DTO.T0102MwStDTO T0102MwSt)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0102Mwst(T0102MwSt);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0102MwStDTO GetT0102MwStById(int T0102MwStId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0102MwStById(T0102MwStId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0102MwStById(Shared.DTO.T0102MwStDTO T0102MwSt, int T0102MwStId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0102MwStById(T0102MwSt, T0102MwStId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0102MwStById(int T0102MwStId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0102MwStById(T0102MwStId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0102MwStDTO> GetT0102MwSt()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0102MwSt();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertrag      
        public bool AddT0602Vertrag(Shared.DTO.T0602VertragDTO T0602Vertrag)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0602Vertrag(T0602Vertrag);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0602VertragDTO GetT0602VertragById(int T0602VertragId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0602VertragById(T0602VertragId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0602VertragById(Shared.DTO.T0602VertragDTO T0602Vertrag, int T0602VertragId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0602VertragById(T0602Vertrag, T0602VertragId);

            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0602VertragById(int T0602VertragId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0602VertragById(T0602VertragId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0602VertragDTO> GetT0602Vertraege()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0602Vertraege();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Bank        
        public bool AddT0103Bank(Shared.DTO.T0103BankDTO T0103Bank)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0103Bank(T0103Bank);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0103BankDTO GetT0103BankById(int T0103BankId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0103BankById(T0103BankId);
            }

            catch
            {
                throw;
            }

        }

        public bool UpdateT0103BankById(Shared.DTO.T0103BankDTO T0103Bank, int T0103BankId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0103BankById(T0103Bank, T0103BankId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0103BankById(int T0103BankId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0103BankById(T0103BankId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0103BankDTO> GetT0103Banken()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0103Banken();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Anschrift       
        public bool AddT0108Anschrift(Shared.DTO.T0108AnschriftDTO T0108Anschrift)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0108Anschrift(T0108Anschrift);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0108AnschriftDTO GetT0108AnschriftById(int T0108AnschriftId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0108AnschriftById(T0108AnschriftId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0108AnschriftById(Shared.DTO.T0108AnschriftDTO T0108Anschrift, int T0108AnschriftId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0108AnschriftById(T0108Anschrift, T0108AnschriftId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0108AnschriftById(int T0108AnschriftId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0108AnschriftById(T0108AnschriftId);
            }
            catch
            {
                throw;
            }

        }
        public List<T0108AnschriftDTO> GetT0108Anschriften()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0108Anschriften();
            }
            catch
            {
                throw;
            }
        }


        #endregion

        #region Lagerort        
        public bool AddT0302Lagerort(Shared.DTO.T0302LagerortDTO T0302Lagerort)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0302Lagerort(T0302Lagerort);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0302LagerortDTO GetT0302LagerortById(int T0302LagerortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0302LagerortById(T0302LagerortId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0302LagerortById(Shared.DTO.T0302LagerortDTO T0302Lagerort, int T0302LagerortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0302LagerortById(T0302Lagerort, T0302LagerortId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0302LagerortById(int T0302LagerortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0302LagerortById(T0302LagerortId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0302LagerortDTO> GetT0302Lagerorte()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0302Lagerorte();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Kostenstelle       
        public bool AddT0141Kostenstelle(Shared.DTO.T0141KostenstelleDTO T0141Kostenstelle)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0141Kostenstelle(T0141Kostenstelle);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0141KostenstelleDTO GetT0141KostenstelleById(int T0141KostenstelleId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0141KostenstelleById(T0141KostenstelleId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0141KostenstelleById(Shared.DTO.T0141KostenstelleDTO T0141Kostenstelle, int T0141KostenstelleId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0141KostenstelleById(T0141Kostenstelle, T0141KostenstelleId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0141KostenstelleById(int T0141KostenstelleId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0141KostenstelleById(T0141KostenstelleId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0141KostenstelleDTO> GetT0141Kostenstellen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0141Kostenstellen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Lager       
        public bool AddT0301Lager(Shared.DTO.T0301LagerDTO T0301Lager)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0301Lager(T0301Lager);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0301LagerDTO GetT0301LagerById(int T0301LagerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0301LagerById(T0301LagerId);
            }
            catch
            {
                throw;
            }


        }

        public bool UpdateT0301LagerById(Shared.DTO.T0301LagerDTO T0301Lager, int T0301LagerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0301LagerById(T0301Lager, T0301LagerId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0301LagerById(int T0301LagerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0301LagerById(T0301LagerId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0301LagerDTO> GetT0301Lager()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0301Lager();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Angebot       
        public bool AddT0401Angebot(Shared.DTO.T0401AngebotDTO T0401Angebot)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0401Angebot(T0401Angebot);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0401AngebotDTO GetT0401AngebotById(int T0401AngebotId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0401AngebotById(T0401AngebotId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0401AngebotById(Shared.DTO.T0401AngebotDTO T0401Angebot, int T0401AngebotId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0401AngebotById(T0401Angebot, T0401AngebotId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0401AngebotById(int T0401AngebotId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0401AngebotById(T0401AngebotId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0401AngebotDTO> GetT0401Angebote()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0401Angebote();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertragstyp        
        public bool AddT0601Vertragstyp(Shared.DTO.T0601VertragstypDTO T0601Vertragstyp)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0601Vertragstyp(T0601Vertragstyp);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0601VertragstypDTO GetT0601VertragstypById(int T0601VertragstypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0601VertragstypById(T0601VertragstypId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0601VertragstypById(Shared.DTO.T0601VertragstypDTO T0601Vertragstyp, int T0601VertragstypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0601VertragstypById(T0601Vertragstyp, T0601VertragstypId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0601VertragstypById(int T0601VertragstypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0601VertragstypById(T0601VertragstypId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0601VertragstypDTO> GetT0601Vertragstypen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0601Vertragstypen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Abrechnungskopf        
        public bool AddT0620Abrechnunskopf(Shared.DTO.T0620AbrechnungskopfDTO T0620Abrechnungskopf)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0620Abrechnunskopf(T0620Abrechnungskopf);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0620AbrechnungskopfDTO GetT0620AbrechnungskopfById(int T0620AbrechnungskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0620AbrechnungskopfById(T0620AbrechnungskopfId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0620AbrechnungskopfById(Shared.DTO.T0620AbrechnungskopfDTO T0620Abrechnungskopf, int T0620AbrechnungskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0620AbrechnungskopfById(T0620Abrechnungskopf, T0620AbrechnungskopfId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0620AbrechnungskopfById(int T0620AbrechnungskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0620AbrechnungskopfById(T0620AbrechnungskopfId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0620AbrechnungskopfDTO> GetT0620Abrechnungskoepfe()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0620Abrechnungskoepfe();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Gruppe        
        public bool AddT0706Gruppe(Shared.DTO.T0706GruppeDTO T0706Gruppe)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0706Gruppe(T0706Gruppe);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0706GruppeDTO GetT0706GruppeById(int T0706GruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0706GruppeById(T0706GruppeId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0706GruppeById(Shared.DTO.T0706GruppeDTO T0706Gruppe, int T0706GruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0706GruppeById(T0706Gruppe, T0706GruppeId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0706GruppeById(int T0706GruppeId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0706GruppeById(T0706GruppeId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0706GruppeDTO> GetT0706Gruppen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0706Gruppen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Benutzer       
        public bool AddT0707Benutzer(Shared.DTO.T0707BenutzerDTO T0707Benutzer)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0707Benutzer(T0707Benutzer);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0707BenutzerDTO GetT0707BenutzerById(int T0707BenutzerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0707BenutzerById(T0707BenutzerId);
            }
            catch
            {
                throw;
            }


        }

        public bool UpdateT0707BenutzerById(Shared.DTO.T0707BenutzerDTO T0707Benutzer, int T0707BenutzerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0707BenutzerById(T0707Benutzer, T0707BenutzerId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0707BenutzerById(int T0707BenutzerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0707BenutzerById(T0707BenutzerId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0707BenutzerDTO> GetT0707Benutzer()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0707Benutzer();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Prozesskopf       
        public bool AddT0802Prozesskopf(Shared.DTO.T0802ProzesskopfDTO T0802Prozesskopf)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0802Prozesskopf(T0802Prozesskopf);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0802ProzesskopfDTO GetT0802ProzesskopfById(int T0802ProzesskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0802ProzesskopfById(T0802ProzesskopfId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0802ProzesskopfById(Shared.DTO.T0802ProzesskopfDTO T0802Prozesskopf, int T0802ProzesskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0802ProzesskopfById(T0802Prozesskopf, T0802ProzesskopfId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0802ProzesskopfById(int T0802ProzesskopfId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0802ProzesskopfById(T0802ProzesskopfId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0802ProzesskopfDTO> GetT0802Prozesskoepfe()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0802Prozesskoepfe();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Technikereinsatz     
        public bool AddT0810TechnikerEinsatz(Shared.DTO.T0810TechnikerEinsatzDTO T0810TechnikerEinsatz)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0810TechnikerEinsatz(T0810TechnikerEinsatz);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0810TechnikerEinsatzDTO GetT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0810TechnikerEinsatzById(T0810TechnikerEinsatzId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0810TechnikerEinsatzById(Shared.DTO.T0810TechnikerEinsatzDTO T0810TechnikerEinsatz, int T0810TechnikerEinsatzId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0810TechnikerEinsatzById(T0810TechnikerEinsatz, T0810TechnikerEinsatzId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0810TechnikerEinsatzById(T0810TechnikerEinsatzId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0810TechnikerEinsatzDTO> GetT0810TechnikerEinsaetze()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0810TechnikerEinsaetze();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region TechnikerStatus     
        public bool AddT0813TechnikerStatus(Shared.DTO.T0813TechnikerStatusDTO T0813TechnikerStatus)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0813TechnikerStatus(T0813TechnikerStatus);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T0813TechnikerStatusDTO GetT0813TechnikerStatusById(int T0813TechnikerStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0813TechnikerStatusById(T0813TechnikerStatusId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0813TechnikerStatusById(Shared.DTO.T0813TechnikerStatusDTO T0813TechnikerStatus, int T0813TechnikerStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0813TechnikerStatusById(T0813TechnikerStatus, T0813TechnikerStatusId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT0813TechnikerStatusById(int T0813TechnikerStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0813TechnikerStatusById(T0813TechnikerStatusId);
            }
            catch
            {
                throw;
            }

        }

        public List<T0813TechnikerStatusDTO> GetT0813TechnikerStatus()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0813TechnikerStatus();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Fehlercodes        
        public bool AddT1601Fehlercodes(Shared.DTO.T1601FehlercodesDTO T1601Fehlercodes)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT1601Fehlercodes(T1601Fehlercodes);
            }
            catch
            {
                throw;
            }

        }

        public Shared.DTO.T1601FehlercodesDTO GetT1601FehlercodesById(int T1601FehlercodesId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT1601FehlercodesById(T1601FehlercodesId);
            }
            catch
            {
                throw;
            }

        }

        public List<Shared.DTO.T1601FehlercodesDTO> GetT1601Fehlercodes()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT1601Fehlercodes();
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT1601FehlercodesById(Shared.DTO.T1601FehlercodesDTO T1601Fehlercodes, int T1601FehlercodesId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT1601FehlercodesById(T1601Fehlercodes, T1601FehlercodesId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteT1601FehlercodesById(int T1601FehlercodesId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT1601FehlercodesById(T1601FehlercodesId);
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region TechnikerEinsatzStatus        
        public bool AddTechnikerEinsatzStatus(Shared.DTO.TechnikerEinsatzStatusDTO TechnikerEinsatzStatus)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddTechnikerEinsatzStatus(TechnikerEinsatzStatus);
            }
            catch
            {
                throw;
            }


        }

        public Shared.DTO.TechnikerEinsatzStatusDTO GetTechnikerEinsatzStatusById(int TechnikerEinsatzStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetTechnikerEinsatzStatusById(TechnikerEinsatzStatusId);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateTechnikerEinsatzStatusById(Shared.DTO.TechnikerEinsatzStatusDTO TechnikerEinsatzStatus, int TechnikerEinsatzStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateTechnikerEinsatzStatusById(TechnikerEinsatzStatus, TechnikerEinsatzStatusId);
            }
            catch
            {
                throw;
            }

        }

        public bool DeleteTechnikerEinsatzStatusById(int TechnikerEinsatzStatusId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteTechnikerEinsatzStatusById(TechnikerEinsatzStatusId);
            }
            catch
            {
                throw;
            }

        }

        public List<TechnikerEinsatzStatusDTO> GetTechnikerEinsatzStatus()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetTechnikerEinsatzStatus();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Lagerortbestand       
        public bool AddT0504Lagerortbestand(Shared.DTO.T0504LagerortbestandDTO T0504Lagerortbestand)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0504Lagerortbestand(T0504Lagerortbestand);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0504LagerortbestandDTO GetT0504LagerortbestandById(int T0504LagerortbestandId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0504LagerortbestandById(T0504LagerortbestandId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0504LagerortbestandById(Shared.DTO.T0504LagerortbestandDTO T0504Lagerortbestand, int T0504LagerortbestandId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0504LagerortbestandById(T0504Lagerortbestand, T0504LagerortbestandId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0504LagerortbestandById(int T0504LagerortbestandId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0504LagerortbestandById(T0504LagerortbestandId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0504LagerortbestandDTO> GetT0504Lagerortbestaende()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0504Lagerortbestaende();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Zaehlwerk        
        public bool AddT0432Zaehlwerk(Shared.DTO.T0432ZaehlwerkDTO T0432Zaehlwerk)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0432Zaehlwerk(T0432Zaehlwerk);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0432ZaehlwerkDTO GetT0432ZaehlwerkById(int T0432ZaehlwerkId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0432ZaehlwerkById(T0432ZaehlwerkId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0432ZaehlwerkById(Shared.DTO.T0432ZaehlwerkDTO T0432Zaehlwerk, int T0432ZaehlwerkId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0432ZaehlwerkById(T0432Zaehlwerk, T0432ZaehlwerkId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0432ZaehlwerkById(int T0432ZaehlwerkId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0432ZaehlwerkById(T0432ZaehlwerkId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0432ZaehlwerkDTO> GetT0432Zaehlwerke()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0432Zaehlwerke();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Zaehlwerktyp        
        public bool AddT0431ZaehlwerkTyp(Shared.DTO.T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0431ZaehlwerkTyp(T0431ZaehlwerkTyp);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0431ZaehlwerkTypDTO GetT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0431ZaehlwerkTypById(T0431ZaehlwerkTypId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0431ZaehlwerkTypById(Shared.DTO.T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp, int T0431ZaehlwerkTypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0431ZaehlwerkTypById(T0431ZaehlwerkTyp, T0431ZaehlwerkTypId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0431ZaehlwerkTypById(T0431ZaehlwerkTypId);
            }
            catch
            {
                throw;
            }
        }


        public List<T0431ZaehlwerkTypDTO> GetT0431Zaehlwerktypen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0431Zaehlwerktypen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Standort       
        public bool AddT0434Standort(Shared.DTO.T0434StandortDTO T0434Standort)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0434Standort(T0434Standort);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0434StandortDTO GetT0434StandortById(int T0434StandortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0434StandortById(T0434StandortId);
            }
            catch
            {
                throw;
            }
        }

        public List<Shared.DTO.T0434StandortDTO> GetT0434Standorte()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0434Standorte();
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0434StandortById(Shared.DTO.T0434StandortDTO T0434Standort, int T0434StandortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0434StandortById(T0434Standort, T0434StandortId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0434StandortById(int T0434StandortId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0434StandortById(T0434StandortId);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Ansprechpartner       
        public bool AddT0120Ansprechpartner(Shared.DTO.T0120AnsprechpartnerDTO T0120Ansprechpartner)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0120Ansprechpartner(T0120Ansprechpartner);
            }
            catch
            {
                throw;
            }
        }

        public Shared.DTO.T0120AnsprechpartnerDTO GetT0120AnsprechpartnerById(int T0120AnsprechpartnerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0120AnsprechpartnerById(T0120AnsprechpartnerId);
            }
            catch
            {
                throw;
            }
        }

        public List<Shared.DTO.T0120AnsprechpartnerDTO> GetT0120Ansprechpartner()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0120Ansprechpartner();
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0120AnsprechpartnerById(Shared.DTO.T0120AnsprechpartnerDTO T0120Ansprechpartner, int T0120AnsprechpartnerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0120AnsprechpartnerById(T0120Ansprechpartner, T0120AnsprechpartnerId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0120AnsprechpartnerById(int T0120AnsprechpartnerId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0120AnsprechpartnerById(T0120AnsprechpartnerId);
            }
            catch
            {
                throw;
            }
        }
        #endregion
              
        #region Prozessposition       
        public bool AddT0803Prozessposition(T0803ProzesspositionDTO T0803Prozessposition)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0803Prozessposition(T0803Prozessposition);
            }
            catch
            {
                throw;
            }
        }

        public T0803ProzesspositionDTO GetT0803ProzesspositionById(int T0803ProzesspositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0803ProzesspositionById(T0803ProzesspositionId);
            }
            catch
            {
                throw;
            }
                
        }

        public bool UpdateT0803ProzesspositionById(T0803ProzesspositionDTO T0803Prozessposition, int T0803ProzesspositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0803ProzesspositionById(T0803Prozessposition, T0803ProzesspositionId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0803ProzesspositionById(int T0803ProzesspositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0803ProzesspositionById(T0803ProzesspositionId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0803ProzesspositionDTO> GetT0803Prozesspositionen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0803Prozesspositionen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Person
        public bool AddT0110Person(T0110PersonDTO T0110Person)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0110Person(T0110Person);
            }
            catch
            {
                throw;
            }
        }

        public T0110PersonDTO GetT0110PersonById(int T0110PersonId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0110PersonById(T0110PersonId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0110PersonById(T0110PersonDTO T0110Person, int T0110PersonId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0110PersonById(T0110Person, T0110PersonId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0110PersonById(int T0110PersonId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0110PersonById(T0110PersonId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0110PersonDTO> GetT0110Personen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0110Personen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ServiceberichtPosition        
        public bool AddServiceberichtPosition(ServiceberichtPositionDTO ServiceberichtPosition)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddServiceberichtPosition(ServiceberichtPosition);
            }
            catch
            {
                throw;
            }
        }

        public ServiceberichtPositionDTO GetServiceberichtPositionById(int ServiceberichtPositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceberichtPositionById(ServiceberichtPositionId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceberichtPositionById(ServiceberichtPositionDTO ServiceberichtPosition, int ServiceberichtPositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateServiceberichtPositionById(ServiceberichtPosition, ServiceberichtPositionId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteServiceberichtPositionById(int ServiceberichtPositionId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteServiceberichtPositionById(ServiceberichtPositionId);
            }
            catch
            {
                throw;
            }
        }

        public List<ServiceberichtPositionDTO> GetServiceberichtPositionen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetServiceberichtPositionen();
            }
            catch
            {
                throw;
            }
        }
        #endregion               

        #region Bankverbindung        
        public bool AddT0123Bankverbindung(T0123BankverbindungDTO T0123Bankverbindung)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0123Bankverbindung(T0123Bankverbindung);
            }
            catch
            {
                throw;
            }
        }

        public T0123BankverbindungDTO GetT0123BankverbindungById(int T0123BankverbindungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0123BankverbindungById(T0123BankverbindungId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0123BankverbindungById(T0123BankverbindungDTO T0123Bankverbindung, int T0123BankverbindungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0123BankverbindungById(T0123Bankverbindung, T0123BankverbindungId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0123BankverbindungById(int T0123BankverbindungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0123BankverbindungById(T0123BankverbindungId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0123BankverbindungDTO> GetT0123Bankverbindungen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0123Bankverbindungen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region VDE
        
        public bool AddVDE(VDEDTO VDE)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddVDE(VDE);
            }
            catch
            {
                throw;
            }
        }

        public VDEDTO GetVDEById(int VDEId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetVDEById(VDEId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateVDEById(VDEDTO VDE, int VDEId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateVDEById(VDE, VDEId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteVDEById(int VDEId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteVDEById(VDEId);
            }
            catch
            {
                throw;
            }
        }

        public List<VDEDTO> GetVDEs()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetVDEs();
            }
            catch
            {
                throw;
            }
        }
        #endregion            

        #region SNRBewegung        
        public bool AddT0511SNRBewegung(T0511SNRBewegungDTO T0511SNRBewegung)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0511SNRBewegung(T0511SNRBewegung);
            }
            catch
            {
                throw;
            }
        }

        public T0511SNRBewegungDTO GetT0511SNRBewegungById(int T0511SNRBewegungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0511SNRBewegungById(T0511SNRBewegungId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0511SNRBewegungById(T0511SNRBewegungDTO T0511SNRBewegung, int T0511SNRBewegungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0511SNRBewegungById(T0511SNRBewegung, T0511SNRBewegungId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0511SNRBewegungById(int T0511SNRBewegungId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0511SNRBewegungById(T0511SNRBewegungId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0511SNRBewegungDTO> GetT0511SNRBewegungen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0511SNRBewegungen();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertragsmaschine        
        public bool AddT0606Vertragsmaschine(T0606VertragsmaschineDTO T0606Vertragsmaschine)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0606Vertragsmaschine(T0606Vertragsmaschine);
            }
            catch
            {
                throw;
            }
        }

        public T0606VertragsmaschineDTO GetT0606VertragsmaschineById(int T0606VertragsmaschineId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0606VertragsmaschineById(T0606VertragsmaschineId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0606VertragsmaschineById(T0606VertragsmaschineDTO T0606Vertragsmaschine, int T0606VertragsmaschineId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0606VertragsmaschineById(T0606Vertragsmaschine, T0606VertragsmaschineId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0606VertragsmaschineById(int T0606VertragsmaschineId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0606VertragsmaschineById(T0606VertragsmaschineId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0606VertragsmaschineDTO> GetT0606Vertragsmaschinen()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0606Vertragsmaschinen();
            }
            catch
            {
                throw;
            }
        }
        #endregion
    
        #region GPKonditionenWgrp       
        public bool AddT0117GPKonditionenWgrp(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0117GPKonditionenWgrp(T0117GPKonditionenWgrp);
            }
            catch
            {
                throw;
            }
        }

        public T0117GPKonditionenWgrpDTO GetT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp, int T0117GPKonditionenWgrpId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0117GPKonditionenWgrpById(T0117GPKonditionenWgrp, T0117GPKonditionenWgrpId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0117GPKonditionenWgrpDTO> GetT0117GPKonditionenWgrp()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0117GPKonditionenWgrp();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GPKonditionenArtikel
        public bool AddT0118GPKonditionenArtikel(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel)
        {
            try
            {
                return ExtraBaseBusiness.Instance().AddT0118GPKonditionenArtikel(T0118GPKonditionenArtikel);
            }
            catch
            {
                throw;
            }
        }

        public T0118GPKonditionenArtikelDTO GetT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel, int T0118GPKonditionenArtikelId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().UpdateT0118GPKonditionenArtikelById(T0118GPKonditionenArtikel, T0118GPKonditionenArtikelId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId)
        {
            try
            {
                return ExtraBaseBusiness.Instance().DeleteT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelId);
            }
            catch
            {
                throw;
            }
        }

        public List<T0118GPKonditionenArtikelDTO> GetT0118GPKonditionenArtikel()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetT0118GPKonditionenArtikel();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        public DateTime GetCurrentDateTime()
        {
            try
            {
                return ExtraBaseBusiness.Instance().GetCurrentDateTime();
            }
            catch
            {
                throw;
            }
        }

        public bool TestConnection()
        {
            try
            {
                return ExtraBaseBusiness.Instance().TestConnection();
            }
            catch
            {
                throw;
            }
        }

    }
}
