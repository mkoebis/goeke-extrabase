﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ExtraBase.Shared.Interface;
using ExtraBase.Shared.DTO;
using ExtraBase.Utility;

namespace ExtraBase.DAL.DALObjects
{
    public class ExtraBaseDAL : IExtraBaseDAL
    {
        private static ExtraBaseDAL instance = null;

        //Konstruktor aktiviert Mapping Methode
        public ExtraBaseDAL()
        {
            AutoMapperConfig.Configure();
        }                

        //Singleton
        public static ExtraBaseDAL Instance()
        {
            if (instance == null)
            {
                instance = new ExtraBaseDAL();
            }
            return instance;
        }

        #region Canongruppe
        public bool AddCanongruppe(CanongruppeDTO canongruppe)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Canongruppe c = Mapper.Map<CanongruppeDTO, Canongruppe>(canongruppe);
                    context.Canongruppe.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public CanongruppeDTO GetCanongruppeById(int canongruppeId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.Canongruppe.Where(i => i.CanongruppeId == canongruppeId).FirstOrDefault<Canongruppe>();
                    return Mapper.Map<Canongruppe, CanongruppeDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateCanongruppeById(CanongruppeDTO canongruppe, int canongruppeId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Canongruppe c = context.Canongruppe.Where(i => i.CanongruppeId == canongruppeId).FirstOrDefault<Canongruppe>();
                    Mapper.Map<CanongruppeDTO, Canongruppe>(canongruppe);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteCanongruppeById(int canongruppeId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Canongruppe c = context.Canongruppe.Where(i => i.CanongruppeId == canongruppeId).FirstOrDefault<Canongruppe>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<CanongruppeDTO> GetCanongruppen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var canongruppe = from t in context.Canongruppe
                                      select t;
                    return Mapper.Map<IEnumerable<Canongruppe>, List<CanongruppeDTO>>(canongruppe);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Hersteller
        public bool AddHersteller(HerstellerDTO hersteller)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Hersteller c = Mapper.Map<HerstellerDTO, Hersteller>(hersteller);
                    context.Hersteller.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public HerstellerDTO GetHerstellerById(int herstellerId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.Hersteller.Where(i => i.HerstellerId == herstellerId).FirstOrDefault<Hersteller>();
                    return Mapper.Map<Hersteller, HerstellerDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHerstelleById(HerstellerDTO hersteller, int herstellerId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Hersteller c = context.Hersteller.Where(i => i.HerstellerId == herstellerId).FirstOrDefault<Hersteller>();
                    Mapper.Map<HerstellerDTO, Hersteller>(hersteller);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteHerstellerById(int herstellerId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Hersteller c = context.Hersteller.Where(i => i.HerstellerId == herstellerId).FirstOrDefault<Hersteller>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<HerstellerDTO> GetHersteller()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var hersteller = from t in context.Hersteller
                                     select t;
                    return Mapper.Map<IEnumerable<Hersteller>, List<HerstellerDTO>>(hersteller);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Serviceart
        public bool AddServiceArt(ServiceArtDTO serviceart)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceArt c = Mapper.Map<ServiceArtDTO, ServiceArt>(serviceart);
                    context.ServiceArt.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public ServiceArtDTO GetServiceArtById(int serviceartId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.ServiceArt.Where(i => i.ServiceArtID == serviceartId).FirstOrDefault<ServiceArt>();
                    return Mapper.Map<ServiceArt, ServiceArtDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceArtById(ServiceArtDTO serviceart, int serviceartId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceArt c = context.ServiceArt.Where(i => i.ServiceArtID == serviceartId).FirstOrDefault<ServiceArt>();
                    Mapper.Map<ServiceArtDTO, ServiceArt>(serviceart);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteServiceArtById(int serviceartId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceArt c = context.ServiceArt.Where(i => i.ServiceArtID == serviceartId).FirstOrDefault<ServiceArt>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<ServiceArtDTO> GetServiceArten()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var serviceArt = from t in context.ServiceArt
                                     select t;
                    return Mapper.Map<IEnumerable<ServiceArt>, List<ServiceArtDTO>>(serviceArt);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Maschinenstamm
        public bool AddT0430Maschinenstamm(T0430MaschinenstammDTO maschinenstamm)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0430Maschinenstamm c = Mapper.Map<T0430MaschinenstammDTO, T0430Maschinenstamm>(maschinenstamm);
                    context.T0430Maschinenstamm.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0430MaschinenstammDTO GetT0430MaschinenstammById(int maschinenstammId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0430Maschinenstamm.Where(i => i.TF0430MaschinenstammID == maschinenstammId).FirstOrDefault<T0430Maschinenstamm>();
                    return Mapper.Map<T0430Maschinenstamm, T0430MaschinenstammDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0430MaschinenstammById(T0430MaschinenstammDTO Maschinenstamm, int maschinenstammId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0430Maschinenstamm c = context.T0430Maschinenstamm.Where(i => i.TF0430MaschinenstammID == maschinenstammId).FirstOrDefault<T0430Maschinenstamm>();
                    Mapper.Map<T0430MaschinenstammDTO, T0430Maschinenstamm>(Maschinenstamm);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0430MaschinenstammById(int maschinenstammId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0430Maschinenstamm c = context.T0430Maschinenstamm.Where(i => i.TF0430MaschinenstammID == maschinenstammId).FirstOrDefault<T0430Maschinenstamm>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0430MaschinenstammDTO> GetT0430Maschinenstamm()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var maschinenstamm = from t in context.T0430Maschinenstamm
                                         select t;
                    return Mapper.Map<IEnumerable<T0430Maschinenstamm>, List<T0430MaschinenstammDTO>>(maschinenstamm);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Servicebericht
        public bool AddServicebericht(ServiceberichtDTO serviceBericht)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Servicebericht c = Mapper.Map<ServiceberichtDTO, Servicebericht>(serviceBericht);
                    context.Servicebericht.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public ServiceberichtDTO GetServiceberichtById(int serviceberichtId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.Servicebericht.Where(i => i.ServiceberichtID == serviceberichtId).FirstOrDefault<Servicebericht>();
                    return Mapper.Map<Servicebericht, ServiceberichtDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceberichtById(ServiceberichtDTO serviceBericht, int serviceberichtId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Servicebericht c = context.Servicebericht.Where(i => i.ServiceberichtID == serviceberichtId).FirstOrDefault<Servicebericht>();
                    Mapper.Map<ServiceberichtDTO, Servicebericht>(serviceBericht);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteServiceberichtById(int serviceberichtId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    Servicebericht c = context.Servicebericht.Where(i => i.ServiceberichtID == serviceberichtId).FirstOrDefault<Servicebericht>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<ServiceberichtDTO> GetServiceberichte()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var servicebericht = from t in context.Servicebericht
                                         select t;
                    return Mapper.Map<IEnumerable<Servicebericht>, List<ServiceberichtDTO>>(servicebericht);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Techniker
        public bool AddT0143Techniker(T0143TechnikerDTO T0143Techniker)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0143Techniker c = Mapper.Map<T0143TechnikerDTO, T0143Techniker>(T0143Techniker);
                    context.T0143Techniker.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0143TechnikerDTO GetT0143TechnikerById(int T0143TechnikerId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0143Techniker.Where(i => i.TF0143TechnikerID == T0143TechnikerId).FirstOrDefault<T0143Techniker>();
                    return Mapper.Map<T0143Techniker, T0143TechnikerDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0143TechnikerById(T0143TechnikerDTO T0143Techniker, int T0143TechnikerId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0143Techniker c = context.T0143Techniker.Where(i => i.TF0143TechnikerID == T0143TechnikerId).FirstOrDefault<T0143Techniker>();
                    Mapper.Map<T0143TechnikerDTO, T0143Techniker>(T0143Techniker);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0143TechnikerById(int T0143TechnikerId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0143Techniker c = context.T0143Techniker.Where(i => i.TF0143TechnikerID == T0143TechnikerId).FirstOrDefault<T0143Techniker>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0143TechnikerDTO> GetT0143Techniker()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var techniker = from t in context.T0143Techniker
                                    select t;
                    return Mapper.Map<IEnumerable<T0143Techniker>, List<T0143TechnikerDTO>>(techniker);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region MwSt
        public bool AddT0102Mwst(T0102MwStDTO T0102MwSt)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0102MwSt c = Mapper.Map<T0102MwStDTO, T0102MwSt>(T0102MwSt);
                    context.T0102MwSt.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0102MwStDTO GetT0102MwStById(int T0102MwStId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0102MwSt.Where(i => i.TF0102MwStID == T0102MwStId).FirstOrDefault<T0102MwSt>();
                    return Mapper.Map<T0102MwSt, T0102MwStDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0102MwStById(T0102MwStDTO T0102MwSt, int T0102MwStId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0102MwSt c = context.T0102MwSt.Where(i => i.TF0102MwStID == T0102MwStId).FirstOrDefault<T0102MwSt>();
                    Mapper.Map<T0102MwStDTO, T0102MwSt>(T0102MwSt);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0102MwStById(int T0102MwStId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0102MwSt c = context.T0102MwSt.Where(i => i.TF0102MwStID == T0102MwStId).FirstOrDefault<T0102MwSt>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0102MwStDTO> GetT0102MwSt()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var mwst = from t in context.T0102MwSt
                               select t;
                    return Mapper.Map<IEnumerable<T0102MwSt>, List<T0102MwStDTO>>(mwst);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertrag
        public bool AddT0602Vertrag(T0602VertragDTO T0602Vertrag)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0602Vertrag c = Mapper.Map<T0602VertragDTO, T0602Vertrag>(T0602Vertrag);
                    context.T0602Vertrag.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0602VertragDTO GetT0602VertragById(int T0602VertragId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0602Vertrag.Where(i => i.TF0602VertragID == T0602VertragId).FirstOrDefault<T0602Vertrag>();
                    return Mapper.Map<T0602Vertrag, T0602VertragDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0602VertragById(T0602VertragDTO T0602Vertrag, int T0602VertragId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0602Vertrag c = context.T0602Vertrag.Where(i => i.TF0602VertragID == T0602VertragId).FirstOrDefault<T0602Vertrag>();
                    Mapper.Map<T0602VertragDTO, T0602Vertrag>(T0602Vertrag);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0602VertragById(int T0602VertragId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0602Vertrag c = context.T0602Vertrag.Where(i => i.TF0602VertragID == T0602VertragId).FirstOrDefault<T0602Vertrag>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0602VertragDTO> GetT0602Vertraege()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var vertraege = from t in context.T0602Vertrag
                                    select t;
                    return Mapper.Map<IEnumerable<T0602Vertrag>, List<T0602VertragDTO>>(vertraege);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Bank
        public bool AddT0103Bank(T0103BankDTO T0103Bank)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0103Bank c = Mapper.Map<T0103BankDTO, T0103Bank>(T0103Bank);
                    context.T0103Bank.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0103BankDTO GetT0103BankById(int T0103BankId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0103Bank.Where(i => i.TF0103BankID == T0103BankId).FirstOrDefault<T0103Bank>();
                    return Mapper.Map<T0103Bank, T0103BankDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0103BankbyId(T0103BankDTO T0103Bank, int T0103BankId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0103Bank c = context.T0103Bank.Where(i => i.TF0103BankID == T0103BankId).FirstOrDefault<T0103Bank>();
                    Mapper.Map<T0103BankDTO, T0103Bank>(T0103Bank);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0103BankById(int T0103BankId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0103Bank c = context.T0103Bank.Where(i => i.TF0103BankID == T0103BankId).FirstOrDefault<T0103Bank>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0103BankDTO> GetT0103Banken()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var banken = from t in context.T0103Bank
                                 select t;
                    return Mapper.Map<IEnumerable<T0103Bank>, List<T0103BankDTO>>(banken);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Anschrift
        public bool AddT0108Anschrift(T0108AnschriftDTO T0108Anschrift)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0108Anschrift c = Mapper.Map<T0108AnschriftDTO, T0108Anschrift>(T0108Anschrift);
                    context.T0108Anschrift.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0108AnschriftDTO GetT0108AnschriftById(int T0108AnschriftId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0108Anschrift.Where(i => i.TF0108AnschriftID == T0108AnschriftId).FirstOrDefault<T0108Anschrift>();
                    return Mapper.Map<T0108Anschrift, T0108AnschriftDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0108AnschriftById(T0108AnschriftDTO T0108Anschrift, int T0108AnschriftId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0108Anschrift c = context.T0108Anschrift.Where(i => i.TF0108AnschriftID == T0108AnschriftId).FirstOrDefault<T0108Anschrift>();
                    Mapper.Map<T0108AnschriftDTO, T0108Anschrift>(T0108Anschrift);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0108AnschriftById(int T0108AnschriftId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0108Anschrift c = context.T0108Anschrift.Where(i => i.TF0108AnschriftID == T0108AnschriftId).FirstOrDefault<T0108Anschrift>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0108AnschriftDTO> GetT0108Anschriften()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var anschriften = from t in context.T0108Anschrift
                                      select t;
                    return Mapper.Map<IEnumerable<T0108Anschrift>, List<T0108AnschriftDTO>>(anschriften);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Lagerort
        public bool AddT0302Lagerort(T0302LagerortDTO T0302Lagerort)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0302Lagerort c = Mapper.Map<T0302LagerortDTO, T0302Lagerort>(T0302Lagerort);
                    context.T0302Lagerort.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0302LagerortDTO GetT0302LagerortById(int T0302LagerortId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0302Lagerort.Where(i => i.TF0302LagerortID == T0302LagerortId).FirstOrDefault<T0302Lagerort>();
                    return Mapper.Map<T0302Lagerort, T0302LagerortDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0302LagerortById(T0302LagerortDTO T0302Lagerort, int T0302LagerortId)
        {
            bool updated = false;

                try
                {
                    using (ExtraBaseEntities context = new ExtraBaseEntities())
                    {
                        T0302Lagerort c = context.T0302Lagerort.Where(i => i.TF0302LagerortID== T0302LagerortId).FirstOrDefault<T0302Lagerort>();
                        Mapper.Map<T0302LagerortDTO, T0302Lagerort>(T0302Lagerort);
                        int n = context.SaveChanges();
                        if (n >= 1)
                        {
                            updated = true;
                        }
                    }
                    return updated;
                }
                catch
                {
                    return updated;
                }            
        }

        public bool DeleteT0302LagerortById(int T0302LagerortId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0302Lagerort c = context.T0302Lagerort.Where(i => i.TF0302LagerortID == T0302LagerortId).FirstOrDefault<T0302Lagerort>();
                    context.DeleteObject(c);
                }
                return deleted;
            }  
            catch
            {
                return deleted;
            }
        }

        public List<T0302LagerortDTO> GetT0302Lagerorte()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var lagerorte = from t in context.T0302Lagerort
                                    select t;
                    return Mapper.Map<IEnumerable<T0302Lagerort>, List<T0302LagerortDTO>>(lagerorte);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Kostenstelle
        public T0141KostenstelleDTO GetT0141KostenstelleById(int T0141KostenstelleId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0141Kostenstelle.Where(i => i.TF0141KostenstelleID == T0141KostenstelleId).FirstOrDefault<T0141Kostenstelle>();
                    return Mapper.Map<T0141Kostenstelle, T0141KostenstelleDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0141KostenstelleById(T0141KostenstelleDTO T0141Kostenstelle, int T0141KostenstelleId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0141Kostenstelle c = context.T0141Kostenstelle.Where(i => i.TF0141KostenstelleID == T0141KostenstelleId).FirstOrDefault<T0141Kostenstelle>();
                    Mapper.Map<T0141KostenstelleDTO, T0141Kostenstelle>(T0141Kostenstelle);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0141KostenstelleById(int T0141KostenstelleId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0141Kostenstelle c = context.T0141Kostenstelle.Where(i => i.TF0141KostenstelleID == T0141KostenstelleId).FirstOrDefault<T0141Kostenstelle>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public bool AddT0141Kostenstelle(T0141KostenstelleDTO T0141Kostenstelle)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0141Kostenstelle c = Mapper.Map<T0141KostenstelleDTO, T0141Kostenstelle>(T0141Kostenstelle);
                    context.T0141Kostenstelle.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public List<T0141KostenstelleDTO> GetT0141Kostenstellen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var kostenstellen = from t in context.T0141Kostenstelle
                                        select t;
                    return Mapper.Map<IEnumerable<T0141Kostenstelle>, List<T0141KostenstelleDTO>>(kostenstellen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Lager
        public bool AddT0301Lager(T0301LagerDTO T0301Lager)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0301Lager c = Mapper.Map<T0301LagerDTO, T0301Lager>(T0301Lager);
                    context.T0301Lager.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0301LagerDTO GetT0301LagerById(int T0301LagerId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0301Lager.Where(i => i.TF0301LagerID == T0301LagerId).FirstOrDefault<T0301Lager>();
                    return Mapper.Map<T0301Lager, T0301LagerDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0301LagerById(T0301LagerDTO T0301Lager, int T0301LagerId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0301Lager c = context.T0301Lager.Where(i => i.TF0301LagerID == T0301LagerId).FirstOrDefault<T0301Lager>();
                    Mapper.Map<T0301LagerDTO, T0301Lager>(T0301Lager);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0301LagerById(int T0301LagerId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0301Lager c = context.T0301Lager.Where(i => i.TF0301LagerID == T0301LagerId).FirstOrDefault<T0301Lager>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0301LagerDTO> GetT0301Lager()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var lager = from t in context.T0301Lager
                                select t;
                    return Mapper.Map<IEnumerable<T0301Lager>, List<T0301LagerDTO>>(lager);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Angebot
        public bool AddT0401Angebot(T0401AngebotDTO T0401Angebot)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0401Angebot c = Mapper.Map<T0401AngebotDTO, T0401Angebot>(T0401Angebot);
                    context.T0401Angebot.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0401AngebotDTO GetT0401AngebotById(int T0401AngebotId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0401Angebot.Where(i => i.TF0401AngebotID == T0401AngebotId).FirstOrDefault<T0401Angebot>();
                    return Mapper.Map<T0401Angebot, T0401AngebotDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0401AngebotById(T0401AngebotDTO T0401Angebot, int T0401AngebotId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0401Angebot c = context.T0401Angebot.Where(i => i.TF0401AngebotID == T0401AngebotId).FirstOrDefault<T0401Angebot>();
                    Mapper.Map<T0401AngebotDTO, T0401Angebot>(T0401Angebot);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0401AngebotById(int T0401AngebotId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0401Angebot c = context.T0401Angebot.Where(i => i.TF0401AngebotID == T0401AngebotId).FirstOrDefault<T0401Angebot>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0401AngebotDTO> GetT0401Angebote()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var angebote = from t in context.T0401Angebot
                                   select t;
                    return Mapper.Map<IEnumerable<T0401Angebot>, List<T0401AngebotDTO>>(angebote);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertragstyp
        public bool AddT0601Vertragstyp(T0601VertragstypDTO T0601Vertragstyp)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0601Vertragstyp c = Mapper.Map<T0601VertragstypDTO, T0601Vertragstyp>(T0601Vertragstyp);
                    context.T0601Vertragstyp.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0601VertragstypDTO GetT0601VertragstypById(int T0601VertragstypId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0601Vertragstyp.Where(i => i.TF0601VertragstypID == T0601VertragstypId).FirstOrDefault<T0601Vertragstyp>();
                    return Mapper.Map<T0601Vertragstyp, T0601VertragstypDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0601VertragstypById(T0601VertragstypDTO T0601Vertragstyp, int T0601VertragstypId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0601Vertragstyp c = context.T0601Vertragstyp.Where(i => i.TF0601VertragstypID == T0601VertragstypId).FirstOrDefault<T0601Vertragstyp>();
                    Mapper.Map<T0601VertragstypDTO, T0601Vertragstyp>(T0601Vertragstyp);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0601VertragstypById(int T0601VertragstypId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0601Vertragstyp c = context.T0601Vertragstyp.Where(i => i.TF0601VertragstypID == T0601VertragstypId).FirstOrDefault<T0601Vertragstyp>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0601VertragstypDTO> GetT0601Vertragstypen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var vertragstypen = from t in context.T0601Vertragstyp
                                        select t;
                    return Mapper.Map<IEnumerable<T0601Vertragstyp>, List<T0601VertragstypDTO>>(vertragstypen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Abrechnungskopf

        public bool AddT0620Abrechnunskopf(T0620AbrechnungskopfDTO T0620Abrechnungskopf)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0620Abrechnungskopf c = Mapper.Map<T0620AbrechnungskopfDTO, T0620Abrechnungskopf>(T0620Abrechnungskopf);
                    context.T0620Abrechnungskopf.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0620AbrechnungskopfDTO GetT0620AbrechnungskopfById(int T0620AbrechnungskopfId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0620Abrechnungskopf.Where(i => i.TF0620ProzesskopfID == T0620AbrechnungskopfId).FirstOrDefault<T0620Abrechnungskopf>();
                    return Mapper.Map<T0620Abrechnungskopf, T0620AbrechnungskopfDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0620AbrechnungskopfById(T0620AbrechnungskopfDTO T0620Abrechnungskopf, int T0620AbrechnungskopfId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0620Abrechnungskopf c = context.T0620Abrechnungskopf.Where(i => i.TF0620ProzesskopfID == T0620AbrechnungskopfId).FirstOrDefault<T0620Abrechnungskopf>();
                    Mapper.Map<T0620AbrechnungskopfDTO, T0620Abrechnungskopf>(T0620Abrechnungskopf);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0620AbrechnungskopfById(int T0620AbrechnungskopfId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0620Abrechnungskopf c = context.T0620Abrechnungskopf.Where(i => i.TF0620ProzesskopfID == T0620AbrechnungskopfId).FirstOrDefault<T0620Abrechnungskopf>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0620AbrechnungskopfDTO> GetT0620Abrechnungskoepfe()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var abrechnungskoepfe = from t in context.T0620Abrechnungskopf
                                            select t;
                    return Mapper.Map<IEnumerable<T0620Abrechnungskopf>, List<T0620AbrechnungskopfDTO>>(abrechnungskoepfe);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Gruppe
        public bool AddT0706Gruppe(T0706GruppeDTO T0706Gruppe)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0706Gruppe c = Mapper.Map<T0706GruppeDTO, T0706Gruppe>(T0706Gruppe);
                    context.T0706Gruppe.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0706GruppeDTO GetT0706GruppeById(int T0706GruppeId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0706Gruppe.Where(i => i.TF0706GruppeID == T0706GruppeId).FirstOrDefault<T0706Gruppe>();
                    return Mapper.Map<T0706Gruppe, T0706GruppeDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0706GruppeById(T0706GruppeDTO T0706Gruppe, int T0706GruppeId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0706Gruppe c = context.T0706Gruppe.Where(i => i.TF0706GruppeID == T0706GruppeId).FirstOrDefault<T0706Gruppe>();
                    Mapper.Map<T0706GruppeDTO, T0706Gruppe>(T0706Gruppe);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0706GruppeById(int T0706GruppeId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0706Gruppe c = context.T0706Gruppe.Where(i => i.TF0706GruppeID == T0706GruppeId).FirstOrDefault<T0706Gruppe>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0706GruppeDTO> GetT0706Gruppen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var gruppen = from t in context.T0706Gruppe
                                  select t;
                    return Mapper.Map<IEnumerable<T0706Gruppe>, List<T0706GruppeDTO>>(gruppen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Benutzer
        public bool AddT0707Benutzer(T0707BenutzerDTO T0707Benutzer)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0707Benutzer c = Mapper.Map<T0707BenutzerDTO, T0707Benutzer>(T0707Benutzer);
                    context.T0707Benutzer.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0707BenutzerDTO GetT0707BenutzerById(int T0707BenutzerId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0707Benutzer.Where(i => i.TF0707BenutzerID == T0707BenutzerId).FirstOrDefault<T0707Benutzer>();
                    return Mapper.Map<T0707Benutzer, T0707BenutzerDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0707BenutzerById(T0707BenutzerDTO T0707Benutzer, int T0707BenutzerId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0707Benutzer c = context.T0707Benutzer.Where(i => i.TF0707BenutzerID == T0707BenutzerId).FirstOrDefault<T0707Benutzer>();
                    Mapper.Map<T0707BenutzerDTO, T0707Benutzer>(T0707Benutzer);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0707BenutzerById(int T0707BenutzerId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0707Benutzer c = context.T0707Benutzer.Where(i => i.TF0707BenutzerID == T0707BenutzerId).FirstOrDefault<T0707Benutzer>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }
        public List<T0707BenutzerDTO> GetT0707Benutzer()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var benutzer = from t in context.T0707Benutzer
                                   select t;
                    return Mapper.Map<IEnumerable<T0707Benutzer>, List<T0707BenutzerDTO>>(benutzer);
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Prozesskopf
        public bool AddT0802Prozesskopf(T0802ProzesskopfDTO T0802Prozesskopf)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0802Prozesskopf c = Mapper.Map<T0802ProzesskopfDTO, T0802Prozesskopf>(T0802Prozesskopf);
                    context.T0802Prozesskopf.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0802ProzesskopfDTO GetT0802ProzesskopfById(int T0802ProzesskopfId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0802Prozesskopf.Where(i => i.TF0802ProzesskopfID == T0802ProzesskopfId).FirstOrDefault<T0802Prozesskopf>();
                    return Mapper.Map<T0802Prozesskopf, T0802ProzesskopfDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0802ProzesskopfById(T0802ProzesskopfDTO T0802Prozesskopf, int T0802ProzesskopfId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0802Prozesskopf c = context.T0802Prozesskopf.Where(i => i.TF0802ProzesskopfID == T0802ProzesskopfId).FirstOrDefault<T0802Prozesskopf>();
                    Mapper.Map<T0802ProzesskopfDTO, T0802Prozesskopf>(T0802Prozesskopf);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0802ProzesskopfById(int T0802ProzesskopfId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0802Prozesskopf c = context.T0802Prozesskopf.Where(i => i.TF0802ProzesskopfID == T0802ProzesskopfId).FirstOrDefault<T0802Prozesskopf>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0802ProzesskopfDTO> GetT0802Prozesskoepfe()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var prozesskoepfe = from t in context.T0802Prozesskopf
                                        select t;
                    return Mapper.Map<IEnumerable<T0802Prozesskopf>, List<T0802ProzesskopfDTO>>(prozesskoepfe);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region TechnikerEinsatz
        public bool AddT0810TechnikerEinsatz(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0810TechnikerEinsatz c = Mapper.Map<T0810TechnikerEinsatzDTO, T0810TechnikerEinsatz>(T0810TechnikerEinsatz);
                    context.T0810TechnikerEinsatz.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0810TechnikerEinsatzDTO GetT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0810TechnikerEinsatz.Where(i => i.TF0810TechnikerEinsatzID == T0810TechnikerEinsatzId).FirstOrDefault<T0810TechnikerEinsatz>();
                    return Mapper.Map<T0810TechnikerEinsatz, T0810TechnikerEinsatzDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0810TechnikerEinsatzById(T0810TechnikerEinsatzDTO T0810TechnikerEinsatz, int T0810TechnikerEinsatzId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0810TechnikerEinsatz c = context.T0810TechnikerEinsatz.Where(i => i.TF0810TechnikerEinsatzID == T0810TechnikerEinsatzId).FirstOrDefault<T0810TechnikerEinsatz>();
                    Mapper.Map<T0810TechnikerEinsatzDTO, T0810TechnikerEinsatz>(T0810TechnikerEinsatz);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0810TechnikerEinsatzById(int T0810TechnikerEinsatzId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0810TechnikerEinsatz c = context.T0810TechnikerEinsatz.Where(i => i.TF0810TechnikerEinsatzID == T0810TechnikerEinsatzId).FirstOrDefault<T0810TechnikerEinsatz>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0810TechnikerEinsatzDTO> GetT0810TechnikerEinsaetze()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var technikerEinsätze = from t in context.T0810TechnikerEinsatz
                                            select t;
                    return Mapper.Map<IEnumerable<T0810TechnikerEinsatz>, List<T0810TechnikerEinsatzDTO>>(technikerEinsätze);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Fehlercodes
        public bool AddT1601Fehlercodes(T1601FehlercodesDTO T1601Fehlercodes)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T1601Fehlercodes c = Mapper.Map<T1601FehlercodesDTO, T1601Fehlercodes>(T1601Fehlercodes);
                    context.T1601Fehlercodes.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T1601FehlercodesDTO GetT1601FehlercodesById(int T1601FehlercodesId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T1601Fehlercodes.Where(i => i.TF1601FehlercodesID == T1601FehlercodesId).FirstOrDefault<T1601Fehlercodes>();
                    return Mapper.Map<T1601Fehlercodes, T1601FehlercodesDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public List<T1601FehlercodesDTO> GetT1601Fehlercodes()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var fehlercodes = (from c in context.T1601Fehlercodes
                                       select c).ToList();
                    return Mapper.Map<IEnumerable<T1601Fehlercodes>, List<T1601FehlercodesDTO>>(fehlercodes);
                }
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT1601FehlercodesById(T1601FehlercodesDTO T1601Fehlercodes, int T1601FehlercodesId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T1601Fehlercodes c = context.T1601Fehlercodes.Where(i => i.TF1601FehlercodesID == T1601FehlercodesId).FirstOrDefault<T1601Fehlercodes>();
                    Mapper.Map<T1601FehlercodesDTO, T1601Fehlercodes>(T1601Fehlercodes);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT1601FehlercodesById(int T1601FehlercodesId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T1601Fehlercodes c = context.T1601Fehlercodes.Where(i => i.TF1601FehlercodesID == T1601FehlercodesId).FirstOrDefault<T1601Fehlercodes>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }
        #endregion

        #region TechnikerStatus
        public T0813TechnikerStatusDTO GetT0813TechnikerStatusById(int T0813TechnikerStatusId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0813TechnikerStatus.Where(i => i.TF0813TechnikerStatusID == T0813TechnikerStatusId).FirstOrDefault<T0813TechnikerStatus>();
                    return Mapper.Map<T0813TechnikerStatus, T0813TechnikerStatusDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool AddT0813TechnikerStatus(T0813TechnikerStatusDTO T0813TechnikerStatus)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0813TechnikerStatus c = Mapper.Map<T0813TechnikerStatusDTO, T0813TechnikerStatus>(T0813TechnikerStatus);
                    context.T0813TechnikerStatus.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public bool UpdateT0813TechnikerStatusById(T0813TechnikerStatusDTO T0813TechnikerStatus, int T0813TechnikerStatusId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0813TechnikerStatus c = context.T0813TechnikerStatus.Where(i => i.TF0813TechnikerStatusID == T0813TechnikerStatusId).FirstOrDefault<T0813TechnikerStatus>();
                    Mapper.Map<T0813TechnikerStatusDTO, T0813TechnikerStatus>(T0813TechnikerStatus);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0813TechnikerStatusById(int T0813TechnikerStatusId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0813TechnikerStatus c = context.T0813TechnikerStatus.Where(i => i.TF0813TechnikerStatusID == T0813TechnikerStatusId).FirstOrDefault<T0813TechnikerStatus>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0813TechnikerStatusDTO> GetT0813TechnikerStatus()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var technikerStatus = from t in context.T0813TechnikerStatus
                                          select t;
                    return Mapper.Map<IEnumerable<T0813TechnikerStatus>, List<T0813TechnikerStatusDTO>>(technikerStatus);
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region TechnikerEinsatzStatus
        public bool AddTechnikerEinsatzStatus(TechnikerEinsatzStatusDTO TechnikerEinsatzStatus)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    TechnikerEinsatzStatus c = Mapper.Map<TechnikerEinsatzStatusDTO, TechnikerEinsatzStatus>(TechnikerEinsatzStatus);
                    context.TechnikerEinsatzStatus.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public TechnikerEinsatzStatusDTO GetTechnikerEinsatzStatusById(int TechnikerEinsatzStatusId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.TechnikerEinsatzStatus.Where(i => i.TechnikerEinsatzStatusID == TechnikerEinsatzStatusId).FirstOrDefault<TechnikerEinsatzStatus>();
                    return Mapper.Map<TechnikerEinsatzStatus, TechnikerEinsatzStatusDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateTechnikerEinsatzStatusById(TechnikerEinsatzStatusDTO TechnikerEinsatzStatus, int TechnikerEinsatzStatusId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    TechnikerEinsatzStatus c = context.TechnikerEinsatzStatus.Where(i => i.TechnikerEinsatzStatusID == TechnikerEinsatzStatusId).FirstOrDefault<TechnikerEinsatzStatus>();
                    Mapper.Map<TechnikerEinsatzStatusDTO, TechnikerEinsatzStatus>(TechnikerEinsatzStatus);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }
        }

        public bool DeleteTechnikerEinsatzStatusById(int TechnikerEinsatzStatusId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    TechnikerEinsatzStatus c = context.TechnikerEinsatzStatus.Where(i => i.TechnikerEinsatzStatusID == TechnikerEinsatzStatusId).FirstOrDefault<TechnikerEinsatzStatus>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<TechnikerEinsatzStatusDTO> GetTechnikerEinsatzStatus()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var technikerEinsatzStatus = from t in context.TechnikerEinsatzStatus
                                                 select t;
                    return Mapper.Map<IEnumerable<TechnikerEinsatzStatus>, List<TechnikerEinsatzStatusDTO>>(technikerEinsatzStatus);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Ansprechpartner
        public bool AddT0120Ansprechpartner(T0120AnsprechpartnerDTO T0120Ansprechpartner)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0120Ansprechpartner c = Mapper.Map<T0120AnsprechpartnerDTO, T0120Ansprechpartner>(T0120Ansprechpartner);
                    context.T0120Ansprechpartner.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public List<T0120AnsprechpartnerDTO> GetT0120Ansprechpartner()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var ansprechpartner = from t in context.T0120Ansprechpartner
                                          select t;
                    return Mapper.Map<IEnumerable<T0120Ansprechpartner>, List<T0120AnsprechpartnerDTO>>(ansprechpartner);
                }
            }
            catch
            {
                throw;
            }
        }

        public T0120AnsprechpartnerDTO GetT0120AnsprechpartnerById(int T0120AnsprechpartnerId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0120Ansprechpartner.Where(i => i.TF0120AnsprechpartnerID == T0120AnsprechpartnerId).FirstOrDefault<T0120Ansprechpartner>();
                    return Mapper.Map<T0120Ansprechpartner, T0120AnsprechpartnerDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0120AnsprechpartnerById(T0120AnsprechpartnerDTO T0120Ansprechpartner, int T0120AnsprechpartnerId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0120Ansprechpartner c = context.T0120Ansprechpartner.Where(i => i.TF0120AnsprechpartnerID == T0120AnsprechpartnerId).FirstOrDefault<T0120Ansprechpartner>();
                    Mapper.Map<T0120AnsprechpartnerDTO, T0120Ansprechpartner>(T0120Ansprechpartner);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }
        }

        public bool DeleteT0120AnsprechpartnerById(int T0120AnsprechpartnerId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0120Ansprechpartner c = context.T0120Ansprechpartner.Where(i => i.TF0120AnsprechpartnerID == T0120AnsprechpartnerId).FirstOrDefault<T0120Ansprechpartner>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }
        #endregion

        #region Lagerortbestand
        public bool AddT0504Lagerortbestand(T0504LagerortbestandDTO T0504Lagerortbestand)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0504Lagerortbestand c = Mapper.Map<T0504LagerortbestandDTO, T0504Lagerortbestand>(T0504Lagerortbestand);
                    context.T0504Lagerortbestand.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0504LagerortbestandDTO GetT0504LagerortbestandById(int T0504LagerortbestandId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0504Lagerortbestand.Where(i => i.TF0504LagerortbestandID == T0504LagerortbestandId).FirstOrDefault<T0504Lagerortbestand>();
                    return Mapper.Map<T0504Lagerortbestand, T0504LagerortbestandDTO>(c);
                }
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateT0504LagerortbestandById(T0504LagerortbestandDTO T0504Lagerortbestand, int T0504LagerortbestandId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0504Lagerortbestand c = context.T0504Lagerortbestand.Where(i => i.TF0504LagerortbestandID == T0504LagerortbestandId).FirstOrDefault<T0504Lagerortbestand>();
                    Mapper.Map<T0504LagerortbestandDTO, T0504Lagerortbestand>(T0504Lagerortbestand);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }
        }

        public bool DeleteT0504LagerortbestandById(int T0504LagerortbestandId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0504Lagerortbestand c = context.T0504Lagerortbestand.Where(i => i.TF0504LagerortbestandID == T0504LagerortbestandId).FirstOrDefault<T0504Lagerortbestand>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0504LagerortbestandDTO> GetT0504Lagerortbestaende()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var lagerortbestaende = from t in context.T0504Lagerortbestand
                                            select t;
                    return Mapper.Map<IEnumerable<T0504Lagerortbestand>, List<T0504LagerortbestandDTO>>(lagerortbestaende);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Zaehlwerk
        public bool AddT0432Zaehlwerk(T0432ZaehlwerkDTO T0432Zaehlwerk)
        {

            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0432Zaehlwerk c = Mapper.Map<T0432ZaehlwerkDTO, T0432Zaehlwerk>(T0432Zaehlwerk);
                    context.T0432Zaehlwerk.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0432ZaehlwerkDTO GetT0432ZaehlwerkById(int T0432ZaehlwerkId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0432Zaehlwerk.Where(i => i.TF0432ZaehlwerkID == T0432ZaehlwerkId).FirstOrDefault<T0432Zaehlwerk>();
                    return Mapper.Map<T0432Zaehlwerk, T0432ZaehlwerkDTO>(c);
                }
            }
            catch
            {
                throw;
            }


        }

        public bool UpdateT0432ZaehlwerkById(T0432ZaehlwerkDTO T0432Zaehlwerk, int T0432ZaehlwerkId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0432Zaehlwerk c = context.T0432Zaehlwerk.Where(i => i.TF0432ZaehlwerkID == T0432ZaehlwerkId).FirstOrDefault<T0432Zaehlwerk>();
                    Mapper.Map<T0432ZaehlwerkDTO, T0432Zaehlwerk>(T0432Zaehlwerk);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }

        }

        public bool DeleteT0432ZaehlwerkById(int T0432ZaehlwerkId)
        {

            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0432Zaehlwerk c = context.T0432Zaehlwerk.Where(i => i.TF0432ZaehlwerkID == T0432ZaehlwerkId).FirstOrDefault<T0432Zaehlwerk>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }

        }

        public List<T0432ZaehlwerkDTO> GetT0432Zaehlwerke()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var zaehlwerke = from t in context.T0432Zaehlwerk
                                     select t;
                    return Mapper.Map<IEnumerable<T0432Zaehlwerk>, List<T0432ZaehlwerkDTO>>(zaehlwerke);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ZaehlwerkTyp
        public bool AddT0431ZaehlwerkTyp(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0431ZaehlwerkTyp c = Mapper.Map<T0431ZaehlwerkTypDTO, T0431ZaehlwerkTyp>(T0431ZaehlwerkTyp);
                    context.T0431ZaehlwerkTyp.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }

        }

        public T0431ZaehlwerkTypDTO GetT0431ZaehlwerkTypById(int T0431ZaehlwerkTypId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0431ZaehlwerkTyp.Where(i => i.TF0431ZaehlwerktypID == T0431ZaehlwerkTypId).FirstOrDefault<T0431ZaehlwerkTyp>();
                    return Mapper.Map<T0431ZaehlwerkTyp, T0431ZaehlwerkTypDTO>(c);
                }
            }
            catch
            {
                throw;
            }


        }

        public bool UpdateT0431ZaehlwerkTypById(T0431ZaehlwerkTypDTO T0431ZaehlwerkTyp, int T0431ZaehlwerkTypId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0431ZaehlwerkTyp c = context.T0431ZaehlwerkTyp.Where(i => i.TF0431ZaehlwerktypID == T0431ZaehlwerkTypId).FirstOrDefault<T0431ZaehlwerkTyp>();
                    Mapper.Map<T0431ZaehlwerkTypDTO, T0431ZaehlwerkTyp>(T0431ZaehlwerkTyp);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }

        }

        public bool DeleteT0431ZaehlwerkTyp(int T0431ZaehlwerkTypId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0431ZaehlwerkTyp c = context.T0431ZaehlwerkTyp.Where(i => i.TF0431ZaehlwerktypID == T0431ZaehlwerkTypId).FirstOrDefault<T0431ZaehlwerkTyp>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }


        }

        public List<T0431ZaehlwerkTypDTO> GetT0431Zaehlwerktypen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var zaehlwerktypen = from t in context.T0431ZaehlwerkTyp
                                         select t;
                    return Mapper.Map<IEnumerable<T0431ZaehlwerkTyp>, List<T0431ZaehlwerkTypDTO>>(zaehlwerktypen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Standort
        public bool AddT0434Standort(T0434StandortDTO T0434Standort)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0434Standort c = Mapper.Map<T0434StandortDTO, T0434Standort>(T0434Standort);
                    context.T0434Standort.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }

        }

        public T0434StandortDTO GetT0434StandortById(int T0434StandortId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0434Standort.Where(i => i.TF0434StandortID == T0434StandortId).FirstOrDefault<T0434Standort>();
                    return Mapper.Map<T0434Standort, T0434StandortDTO>(c);
                }
            }
            catch
            {
                throw;
            }



        }

        public List<T0434StandortDTO> GetT0434Standorte()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var standorte = from t in context.T0434Standort
                                    select t;
                    return Mapper.Map<IEnumerable<T0434Standort>, List<T0434StandortDTO>>(standorte);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0434StandortById(T0434StandortDTO T0434Standort, int T0434StandortId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0434Standort c = context.T0434Standort.Where(i => i.TF0434StandortID == T0434StandortId).FirstOrDefault<T0434Standort>();
                    Mapper.Map<T0434StandortDTO, T0434Standort>(T0434Standort);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }

        }

        public bool DeleteT0434StandortById(int T0434StandortId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0434Standort c = context.T0434Standort.Where(i => i.TF0434StandortID == T0434StandortId).FirstOrDefault<T0434Standort>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }
        #endregion          
    
        #region Prozessposition      
        public bool AddT0803Prozessposition(T0803ProzesspositionDTO T0803Prozessposition)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0803Prozessposition c = Mapper.Map<T0803ProzesspositionDTO, T0803Prozessposition>(T0803Prozessposition);
                    context.T0803Prozessposition.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0803ProzesspositionDTO GetT0803ProzesspositionById(int T0803ProzesspositionId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0803Prozessposition.Where(i => i.TF0803ProzessPosID == T0803ProzesspositionId).FirstOrDefault<T0803Prozessposition>();
                    return Mapper.Map<T0803Prozessposition, T0803ProzesspositionDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0803ProzesspositionById(T0803ProzesspositionDTO T0803Prozessposition, int T0803ProzesspositionId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0803Prozessposition c = context.T0803Prozessposition.Where(i => i.TF0803ProzessPosID == T0803ProzesspositionId).FirstOrDefault<T0803Prozessposition>();
                    Mapper.Map<T0803ProzesspositionDTO, T0803Prozessposition>(T0803Prozessposition);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0803ProzesspositionById(int T0803ProzesspositionId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0803Prozessposition c = context.T0803Prozessposition.Where(i => i.TF0803ProzessPosID == T0803ProzesspositionId).FirstOrDefault<T0803Prozessposition>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }


        public List<T0803ProzesspositionDTO> GetT0803Prozesspositionen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var prozesspositionen = from t in context.T0803Prozessposition
                                            select t;
                    return Mapper.Map<IEnumerable<T0803Prozessposition>, List<T0803ProzesspositionDTO>>(prozesspositionen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
        
        #region Person
        public bool AddT0110Person(T0110PersonDTO T0110Person)
        {

                bool added = false;
                try
                {
                    using (ExtraBaseEntities context = new ExtraBaseEntities())
                    {
                        T0110Person c = Mapper.Map<T0110PersonDTO, T0110Person>(T0110Person);
                        context.T0110Person.AddObject(c);

                        if (context.SaveChanges() >= 1)
                        {
                            added = true;
                        }
                        return added;
                    }
                }
                catch
                {
                    return added;
                }

           
        }

        public T0110PersonDTO GetT0110PersonById(int T0110PersonId)
        {
                      
                try
                {
                    using (ExtraBaseEntities context = new ExtraBaseEntities())
                    {
                        var c = context.T0110Person.Where(i => i.TF0110PersonID == T0110PersonId).FirstOrDefault<T0110Person>();
                        return Mapper.Map<T0110Person, T0110PersonDTO>(c);
                    }
                }
                catch
                {
                    throw;
                }
        }

        public bool UpdateT0110PersonById(T0110PersonDTO T0110Person, int T0110PersonId)
        {

                bool updated = false;

                try
                {
                    using (ExtraBaseEntities context = new ExtraBaseEntities())
                    {
                        T0110Person c = context.T0110Person.Where(i => i.TF0110PersonID ==T0110PersonId).FirstOrDefault<T0110Person>();
                        Mapper.Map<T0110PersonDTO, T0110Person>(T0110Person);
                        int n = context.SaveChanges();
                        if (n >= 1)
                        {
                            updated = true;
                        }
                    }
                    return updated;
                }

                catch
                {
                    throw;
                }            
        }

        public bool DeleteT0110PersonById(int T0110PersonId)
        {

                bool deleted = false;

                try
                {
                    using (ExtraBaseEntities context = new ExtraBaseEntities())
                    {
                        T0110Person c = context.T0110Person.Where(i => i.TF0110PersonID == T0110PersonId).FirstOrDefault<T0110Person>();
                        context.DeleteObject(c);
                        if (context.SaveChanges() >= 1)
                        {
                            deleted = true;
                        }
                        return deleted;
                    }
                }
                catch
                {
                    return deleted;
                }
        }

        public List<T0110PersonDTO> GetT0110Personen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var personen = from t in context.T0110Person
                                            select t;
                    return Mapper.Map<IEnumerable<T0110Person>, List<T0110PersonDTO>>(personen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ServiceberichtPosition       
        public bool AddServiceberichtPosition(ServiceberichtPositionDTO ServiceberichtPosition)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceberichtPosition c = Mapper.Map<ServiceberichtPositionDTO, ServiceberichtPosition>(ServiceberichtPosition);
                    context.ServiceberichtPosition.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public ServiceberichtPositionDTO GetServiceberichtPositionById(int ServiceberichtPositionId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.ServiceberichtPosition.Where(i => i.ServiceberichtPositionID == ServiceberichtPositionId).FirstOrDefault<ServiceberichtPosition>();
                    return Mapper.Map<ServiceberichtPosition, ServiceberichtPositionDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateServiceberichtPositionById(ServiceberichtPositionDTO ServiceberichtPosition, int ServiceberichtPositionId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceberichtPosition c = context.ServiceberichtPosition.Where(i => i.ServiceberichtPositionID == ServiceberichtPositionId).FirstOrDefault<ServiceberichtPosition>();
                    Mapper.Map<ServiceberichtPositionDTO, ServiceberichtPosition>(ServiceberichtPosition);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteServiceberichtPositionById(int ServiceberichtPositionId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    ServiceberichtPosition c = context.ServiceberichtPosition.Where(i => i.ServiceberichtPositionID == ServiceberichtPositionId).FirstOrDefault<ServiceberichtPosition>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<ServiceberichtPositionDTO> GetServiceberichtPositionen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var serviceberichtPosition = from t in context.ServiceberichtPosition
                                     select t;
                    return Mapper.Map<IEnumerable<ServiceberichtPosition>, List<ServiceberichtPositionDTO>>(serviceberichtPosition);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Bankverbindung
    
        public bool AddT0123Bankverbindung(T0123BankverbindungDTO T0123Bankverbindung)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0123Bankverbindung c = Mapper.Map<T0123BankverbindungDTO, T0123Bankverbindung>(T0123Bankverbindung);
                    context.T0123Bankverbindung.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0123BankverbindungDTO GetT0123BankverbindungById(int T0123BankverbindungId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0123Bankverbindung.Where(i => i.TF0123BankverbindungID == T0123BankverbindungId).FirstOrDefault<T0123Bankverbindung>();
                    return Mapper.Map<T0123Bankverbindung, T0123BankverbindungDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0123BankverbindungById(T0123BankverbindungDTO T0123Bankverbindung, int T0123BankverbindungId)
        {
            bool updated = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0123Bankverbindung c = context.T0123Bankverbindung.Where(i => i.TF0123BankverbindungID == T0123BankverbindungId).FirstOrDefault<T0123Bankverbindung>();
                    Mapper.Map<T0123BankverbindungDTO, T0123Bankverbindung>(T0123Bankverbindung);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0123BankverbindungById(int T0123BankverbindungId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0123Bankverbindung c = context.T0123Bankverbindung.Where(i => i.TF0123BankverbindungID == T0123BankverbindungId).FirstOrDefault<T0123Bankverbindung>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }       
       
        public List<T0123BankverbindungDTO> GetT0123Bankverbindungen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var bankverbindungen = from t in context.T0123Bankverbindung
                                                 select t;
                    return Mapper.Map<IEnumerable<T0123Bankverbindung>, List<T0123BankverbindungDTO>>(bankverbindungen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region VDE
        public bool AddVDE(VDEDTO VDE)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    VDE c = Mapper.Map<VDEDTO, VDE>(VDE);
                    context.VDE.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public VDEDTO GetVDEById(int VDEId)
        {

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.VDE.Where(i => i.VdeId == VDEId).FirstOrDefault<VDE>();
                    return Mapper.Map<VDE, VDEDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateVDEById(VDEDTO VDE, int VDEId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    VDE c = context.VDE.Where(i => i.VdeId == VDEId).FirstOrDefault<VDE>();
                    Mapper.Map<VDEDTO, VDE>(VDE);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteVDEById(int VDEId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    VDE c = context.VDE.Where(i => i.VdeId == VDEId).FirstOrDefault<VDE>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<VDEDTO> GetVDEs()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var vdes = from t in context.VDE
                                                 select t;
                    return Mapper.Map<IEnumerable<VDE>, List<VDEDTO>>(vdes);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region SNRBewegung       
        public bool AddT0511SNRBewegung(T0511SNRBewegungDTO T0511SNRBewegung)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0511SNRBewegung c = Mapper.Map<T0511SNRBewegungDTO, T0511SNRBewegung>(T0511SNRBewegung);
                    context.T0511SNRBewegung.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0511SNRBewegungDTO GetT0511SNRBewegungById(int T0511SNRBewegungId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var c = context.T0511SNRBewegung.Where(i => i.TF0511SNRBewegungID== T0511SNRBewegungId).FirstOrDefault<T0511SNRBewegung>();
                    return Mapper.Map<T0511SNRBewegung, T0511SNRBewegungDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0511SNRBewegungById(T0511SNRBewegungDTO T0511SNRBewegung, int T0511SNRBewegungId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0511SNRBewegung c = context.T0511SNRBewegung.Where(i => i.TF0511SNRBewegungID == T0511SNRBewegungId).FirstOrDefault<T0511SNRBewegung>();
                    Mapper.Map<T0511SNRBewegungDTO, T0511SNRBewegung>(T0511SNRBewegung);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }

            catch
            {
                throw;
            }         
        }

        public bool DeleteT0511SNRBewegungById(int T0511SNRBewegungId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0511SNRBewegung c = context.T0511SNRBewegung.Where(i => i.TF0511SNRBewegungID == T0511SNRBewegungId).FirstOrDefault<T0511SNRBewegung>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0511SNRBewegungDTO> GetT0511SNRBewegungen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var snrbewegungen = from t in context.T0511SNRBewegung
                                   select t;
                    return Mapper.Map<IEnumerable<T0511SNRBewegung>, List<T0511SNRBewegungDTO>>(snrbewegungen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Vertragsmaschinen        
        public bool AddT0606Vertragsmaschine(T0606VertragsmaschineDTO T0606Vertragsmaschine)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0606Vertragsmaschine c = Mapper.Map<T0606VertragsmaschineDTO, T0606Vertragsmaschine>(T0606Vertragsmaschine);
                    context.T0606Vertragsmaschine.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0606VertragsmaschineDTO GetT0606VertragsmaschineById(int T0606VertragsmaschineId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {

                    var c = context.T0606Vertragsmaschine.Where(i => i.TF0606VertragsmaschineID == T0606VertragsmaschineId).FirstOrDefault<T0606Vertragsmaschine>();
                    return Mapper.Map<T0606Vertragsmaschine, T0606VertragsmaschineDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0606VertragsmaschineById(T0606VertragsmaschineDTO T0606Vertragsmaschine, int T0606VertragsmaschineId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0606Vertragsmaschine c = context.T0606Vertragsmaschine.Where(i => i.TF0606VertragsmaschineID == T0606VertragsmaschineId).FirstOrDefault<T0606Vertragsmaschine>();
                    Mapper.Map<T0606VertragsmaschineDTO, T0606Vertragsmaschine>(T0606Vertragsmaschine);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0606VertragsmaschineById(int T0606VertragsmaschineId)
        {
            bool deleted = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0606Vertragsmaschine c = context.T0606Vertragsmaschine.Where(i => i.TF0606VertragsmaschineID == T0606VertragsmaschineId).FirstOrDefault<T0606Vertragsmaschine>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                return deleted;
            }
        }

        public List<T0606VertragsmaschineDTO> GetT0606Vertragsmaschinen()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var vertragsmaschinen = from t in context.T0606Vertragsmaschine
                                    select t;
                    return Mapper.Map<IEnumerable<T0606Vertragsmaschine>,List<T0606VertragsmaschineDTO>>(vertragsmaschinen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GPKonditionenWgrp        
        public bool AddT0117GPKonditionenWgrp(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0117GPKonditionenWgrp c = Mapper.Map<T0117GPKonditionenWgrpDTO, T0117GPKonditionenWgrp>(T0117GPKonditionenWgrp);
                    context.T0117GPKonditionenWgrp.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0117GPKonditionenWgrpDTO GetT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {

                    var c = context.T0117GPKonditionenWgrp.Where(i => i.TF0117GPKonditionenWgrpID == T0117GPKonditionenWgrpId).FirstOrDefault<T0117GPKonditionenWgrp>();
                    return Mapper.Map<T0117GPKonditionenWgrp, T0117GPKonditionenWgrpDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0117GPKonditionenWgrpById(T0117GPKonditionenWgrpDTO T0117GPKonditionenWgrp, int T0117GPKonditionenWgrpId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0117GPKonditionenWgrp c = context.T0117GPKonditionenWgrp.Where(i => i.TF0117GPKonditionenWgrpID == T0117GPKonditionenWgrpId).FirstOrDefault<T0117GPKonditionenWgrp>();
                    Mapper.Map<T0117GPKonditionenWgrpDTO, T0117GPKonditionenWgrp>(T0117GPKonditionenWgrp);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0117GPKonditionenWgrpById(int T0117GPKonditionenWgrpId)
        {
            bool deleted = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0117GPKonditionenWgrp c = context.T0117GPKonditionenWgrp.Where(i => i.TF0117GPKonditionenWgrpID == T0117GPKonditionenWgrpId).FirstOrDefault<T0117GPKonditionenWgrp>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                throw;
            }
        }

        public List<T0117GPKonditionenWgrpDTO> GetT0117GPKonditionenWgrp()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var gpKonditionen = from t in context.T0117GPKonditionenWgrp
                                            select t;
                    return Mapper.Map<IEnumerable<T0117GPKonditionenWgrp>, List<T0117GPKonditionenWgrpDTO>>(gpKonditionen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GPKonditionenArtikel      
        public bool AddT0118GPKonditionenArtikel(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel)
        {
            bool added = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0118GPKonditionenArtikel c = Mapper.Map<T0118GPKonditionenArtikelDTO, T0118GPKonditionenArtikel>(T0118GPKonditionenArtikel);
                    context.T0118GPKonditionenArtikel.AddObject(c);

                    if (context.SaveChanges() >= 1)
                    {
                        added = true;
                    }
                    return added;
                }
            }
            catch
            {
                return added;
            }
        }

        public T0118GPKonditionenArtikelDTO GetT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId)
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {

                    var c = context.T0118GPKonditionenArtikel.Where(i => i.TF0118GPKonditionenArtikeliD == T0118GPKonditionenArtikelId).FirstOrDefault<T0118GPKonditionenArtikel>();
                    return Mapper.Map<T0118GPKonditionenArtikel, T0118GPKonditionenArtikelDTO>(c);
                }
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateT0118GPKonditionenArtikelById(T0118GPKonditionenArtikelDTO T0118GPKonditionenArtikel, int T0118GPKonditionenArtikelId)
        {
            bool updated = false;

            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0118GPKonditionenArtikel c = context.T0118GPKonditionenArtikel.Where(i => i.TF0118GPKonditionenArtikeliD == T0118GPKonditionenArtikelId).FirstOrDefault<T0118GPKonditionenArtikel>();
                    Mapper.Map<T0118GPKonditionenArtikelDTO, T0118GPKonditionenArtikel>(T0118GPKonditionenArtikel);
                    int n = context.SaveChanges();
                    if (n >= 1)
                    {
                        updated = true;
                    }
                }
                return updated;
            }
            catch
            {
                return updated;
            }
        }

        public bool DeleteT0118GPKonditionenArtikelById(int T0118GPKonditionenArtikelId)
        {
            bool deleted = false;
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    T0118GPKonditionenArtikel c = context.T0118GPKonditionenArtikel.Where(i => i.TF0118GPKonditionenArtikeliD == T0118GPKonditionenArtikelId).FirstOrDefault<T0118GPKonditionenArtikel>();
                    context.DeleteObject(c);
                    if (context.SaveChanges() >= 1)
                    {
                        deleted = true;
                    }
                    return deleted;
                }
            }
            catch
            {
                throw;
            }
        }

        public List<T0118GPKonditionenArtikelDTO> GetT0118GPKonditionenArtikel()
        {
            try
            {
                using (ExtraBaseEntities context = new ExtraBaseEntities())
                {
                    var gpKonditionen = from t in context.T0118GPKonditionenArtikel
                                        select t;
                    return Mapper.Map<IEnumerable<T0118GPKonditionenArtikel>, List<T0118GPKonditionenArtikelDTO>>(gpKonditionen);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}