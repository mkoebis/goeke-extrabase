﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ExtraBase.Shared.DTO;
using ExtraBase.DAL;

namespace ExtraBase.Utility
{
    public class ExtraBaseMappingProfile:Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ExtraBaseMappingProfile";
            }
        }
        
        protected override void Configure()
        {
            Mapper.CreateMap<Canongruppe, CanongruppeDTO>();
            Mapper.CreateMap<CanongruppeDTO, Canongruppe>();

            Mapper.CreateMap<HerstellerDTO, Hersteller>();
            Mapper.CreateMap<Hersteller, HerstellerDTO>();

            Mapper.CreateMap<ServiceArtDTO, ServiceArtDTO>();
            Mapper.CreateMap<ServiceArt, ServiceArtDTO>();

            Mapper.CreateMap<T0102MwStDTO, T0102MwSt>();
            Mapper.CreateMap<T0102MwSt, T0102MwStDTO>();

            Mapper.CreateMap<T0103BankDTO, T0103Bank>();
            Mapper.CreateMap<T0103Bank, T0103BankDTO>();

            Mapper.CreateMap<T0108AnschriftDTO, T0108Anschrift>();
            Mapper.CreateMap<T0108Anschrift, T0108AnschriftDTO>();

            Mapper.CreateMap<T0120AnsprechpartnerDTO, T0120Ansprechpartner>();
            Mapper.CreateMap<T0120Ansprechpartner, T0120AnsprechpartnerDTO>();

            Mapper.CreateMap<T0141KostenstelleDTO, T0141Kostenstelle>();
            Mapper.CreateMap<T0141Kostenstelle, T0141KostenstelleDTO>();

            Mapper.CreateMap<T0143TechnikerDTO, T0143Techniker>();
            Mapper.CreateMap<T0143Techniker, T0143TechnikerDTO>();

            Mapper.CreateMap<T0301LagerDTO, T0301Lager>();
            Mapper.CreateMap<T0301Lager, T0301LagerDTO>();

            Mapper.CreateMap<T0302LagerortDTO, T0302Lagerort>();
            Mapper.CreateMap<T0302Lagerort, T0302LagerortDTO>();

            Mapper.CreateMap<T0401AngebotDTO, T0401Angebot>();
            Mapper.CreateMap<T0401Angebot, T0401AngebotDTO>();

            Mapper.CreateMap<T0430MaschinenstammDTO, T0430Maschinenstamm>();
            Mapper.CreateMap<T0430Maschinenstamm, T0430MaschinenstammDTO>();

            Mapper.CreateMap<T0431ZaehlwerkTypDTO, T0431ZaehlwerkTyp>();
            Mapper.CreateMap<T0431ZaehlwerkTyp, T0431ZaehlwerkTypDTO>();

            Mapper.CreateMap<T0432ZaehlwerkDTO, T0432Zaehlwerk>();
            Mapper.CreateMap<T0432Zaehlwerk, T0432ZaehlwerkDTO>();

            Mapper.CreateMap<T0601VertragstypDTO, T0601Vertragstyp>();
            Mapper.CreateMap<T0601Vertragstyp, T0601VertragstypDTO>();

            Mapper.CreateMap<T0602VertragDTO, T0602Vertrag>();
            Mapper.CreateMap<T0602Vertrag, T0602VertragDTO>();

            Mapper.CreateMap<T0620AbrechnungskopfDTO, T0620Abrechnungskopf>();
            Mapper.CreateMap<T0620Abrechnungskopf, T0620AbrechnungskopfDTO>();

            Mapper.CreateMap<T0706GruppeDTO, T0706Gruppe>();
            Mapper.CreateMap<T0706Gruppe, T0706GruppeDTO>();

            Mapper.CreateMap<T0707BenutzerDTO, T0707Benutzer>();
            Mapper.CreateMap<T0707Benutzer, T0707BenutzerDTO>();

            Mapper.CreateMap<T0802ProzesskopfDTO, T0802Prozesskopf>();
            Mapper.CreateMap<T0802Prozesskopf, T0802ProzesskopfDTO>();

            Mapper.CreateMap<T0810TechnikerEinsatzDTO, T0810TechnikerEinsatz>();
            Mapper.CreateMap<T0810TechnikerEinsatz, T0810TechnikerEinsatzDTO>();

            Mapper.CreateMap<T0813TechnikerStatusDTO, T0813TechnikerStatus>();
            Mapper.CreateMap<T0813TechnikerStatus, T0813TechnikerStatusDTO>();

            Mapper.CreateMap<T1601FehlercodesDTO, T1601Fehlercodes>();
            Mapper.CreateMap<T1601Fehlercodes, T1601FehlercodesDTO>();

            Mapper.CreateMap<TechnikerEinsatzStatusDTO, TechnikerEinsatzStatus>();
            Mapper.CreateMap<TechnikerEinsatzStatus, TechnikerEinsatzStatusDTO>();

            Mapper.CreateMap<T0434StandortDTO, T0434Standort>();
            Mapper.CreateMap<T0434Standort, T0434StandortDTO>();

            Mapper.CreateMap<T0803ProzesspositionDTO, T0803Prozessposition>();
            Mapper.CreateMap<T0803Prozessposition, T0803ProzesspositionDTO>();

            Mapper.CreateMap<ServiceberichtDTO, Servicebericht>();
            Mapper.CreateMap<Servicebericht, ServiceberichtDTO>();

            Mapper.CreateMap<T0504LagerortbestandDTO, T0504Lagerortbestand>();
            Mapper.CreateMap<T0504Lagerortbestand, T0504LagerortbestandDTO>();

            Mapper.CreateMap<T0431ZaehlwerkTypDTO, T0431ZaehlwerkTyp>();
            Mapper.CreateMap<T0431ZaehlwerkTyp, T0431ZaehlwerkTypDTO>();
  
            Mapper.CreateMap<T0432Zaehlwerk, T0432ZaehlwerkDTO>();
            Mapper.CreateMap<T0432ZaehlwerkDTO, T0432Zaehlwerk>();

            Mapper.CreateMap<VDEDTO, VDE>();
            Mapper.CreateMap<VDE, VDEDTO>();

            Mapper.CreateMap<T0511SNRBewegungDTO, T0511SNRBewegung>();
            Mapper.CreateMap<T0511SNRBewegung,T0511SNRBewegungDTO>();

            Mapper.CreateMap<T0123BankverbindungDTO, T0123Bankverbindung>();
            Mapper.CreateMap<T0123Bankverbindung, T0123BankverbindungDTO>();

            Mapper.CreateMap<T0606VertragsmaschineDTO, T0606Vertragsmaschine>();
            Mapper.CreateMap<T0606Vertragsmaschine, T0606VertragsmaschineDTO>();

            Mapper.CreateMap<T0511SNRBewegung, T0511SNRBewegungDTO>();
            Mapper.CreateMap<T0511SNRBewegungDTO,T0511SNRBewegung>();

            Mapper.CreateMap<T0117GPKonditionenWgrp, T0117GPKonditionenWgrpDTO>();
            Mapper.CreateMap<T0117GPKonditionenWgrpDTO, T0117GPKonditionenWgrp>();

            Mapper.CreateMap<T0118GPKonditionenArtikel, T0118GPKonditionenArtikelDTO>();
            Mapper.CreateMap<T0118GPKonditionenArtikelDTO, T0118GPKonditionenArtikel>();

        }
    }
}