﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace ExtraBase.Utility
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(mapper => mapper.AddProfile<ExtraBaseMappingProfile>());
        }
    }
}